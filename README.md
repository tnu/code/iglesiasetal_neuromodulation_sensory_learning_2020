This repository contains code to perform the analyses presented in:

Iglesias S, Kasper L, Harrison SJ, Manka R, Mathys C, & Stephan KE (2020). 
Cholinergic and dopaminergic effects on prediction error and uncertainty 
responses during sensory associative learning. *NeuroImage*, 117590
===========================================================================
|                               |                                         |
| ----------------------------- | ----------------------------------------|
|Author:                        |Sandra Iglesias                          |
|Created:                       |December 2020                            |
|License:                       |GNU General Public License               |

This software is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details: http://www.gnu.org/licenses/

***************************************************************************
Details about the analysis can be found in the README.md files of the two substudies: Study 1 (antagonists) and Study 2 (agonists).

# Contributors / Roles
|                               |                                             |
| ----------------------------- | ------------------------------------------- |
| Project lead / analysis:      | Sandra Iglesias (PhD)                       |
| Supervising Prof.:            | Prof. Klaas Enno Stephan (MD Dr. med., PhD) |
| Abbreviation:                 | ADPRSI (study acronym)                      |
| Date:                         | December, 2020                              |

The project was conducted at the Translational Neuromodeling Unit (TNU).
