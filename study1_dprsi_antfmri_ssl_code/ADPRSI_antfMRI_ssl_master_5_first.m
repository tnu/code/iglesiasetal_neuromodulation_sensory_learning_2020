function ADPRSI_antfMRI_ssl_master_5_first( id )
% ADPRSI_antfMRI_ssl_master_5_first function to run the following analysis steps: 
% 1. write out regressors for the first level GLM
% 2. 1stlevel analyses: epsilons (GLM1)
% 3. 1stlevel analyses: da and precision-weights (GLM2)
% 4. plot physiological noise and movement contrasts for GLM1
% 5. plot physiological noise and movement contrasts for GLM2
% 6. checkreg mask.nii of both GLMs and overlay anatomical mask

%   IN:     id - the subject index

%   OUT:    -

% 07-01-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

%% Set paths
ADPRSI_antfMRI_ssl_initialise_environment();

%% GLM SSL 
ADPRSI_antfMRI_ssl_glmInput( id )

%% 1stlevel SSL analyses
ADPRSI_antfMRI_ssl_firstlevel_epsilon ( id )
ADPRSI_antfMRI_ssl_firstlevel_da ( id )

%% plot nuisance regressors
ADPRSI_antfMRI_ssl_nuisance_epsilon ( id )
ADPRSI_antfMRI_ssl_nuisance_da ( id )

%% plot anatomical mask over mask.nii files
% ADPRSI_antfMRI_ssl_checkreg_mask ( id )

end
