function ADPRSI_antfMRI_ssl_checkreg_contour( id )
% ADPRSI_antfMRI_ssl_checkreg_contour: checkreg warped functional and structural data
% - overlay contour of structural image on functional images.
% - overlay anatomical mask on structural and functional images.

%   IN:     id                  - the subject index

%   OUT:    -

% 08-01-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

% Set paths
workdir = DPRSI_antfMRI_ssl_set_workdir;
options = DPRSI_antfMRI_ssl_set_analysis_options;
[ details, paths ] = DPRSI_antfMRI_ssl_subjects( id );

% Start diary
diary(paths.logfile_checkreg);

% Initialize
disp(['running checkreg for subj: ', details.subjname,' (',num2str(id),')']);

matlabbatch{1}.spm.util.checkreg.data = {
    [fullfile([paths.res_struct,'\wmstruct.nii,1'])]
    [fullfile([paths.res_fMRIssl,'\wfmri_ssl_00145.nii,1'])]
    [fullfile([paths.res_fMRIssl,'\wfmri_ssl_00305.nii,1'])]
    [fullfile([paths.res_fMRIssl,'\wfmri_ssl_00400.nii,1'])]
    };
spm_jobman('run',matlabbatch);

% display contour of first image (structural image) on the other images
contour_display(1,NaN);
cd(paths.res_checkreg);

matlabbatch2{1}.spm.util.print.fname = ([details.subjname,'_checkreg_ssl']);
matlabbatch2{1}.spm.util.print.fig.fighandle = NaN;
matlabbatch2{1}.spm.util.print.opts = 'pdf';
spm_jobman('run',matlabbatch2);

copyfile ([details.subjname,'_checkreg_ssl_001.pdf'], [paths.quality_ssl_prep,details.subjname,'_checkreg_ssl.pdf']);

%% overlay anatomical ROI mask to check for droupouts
% Initialize
disp(['running anatomical mask overlay: ', details.subjname,' (',num2str(id),')']);

matlabbatch{1}.spm.util.checkreg.data = {
    [fullfile([paths.res_struct,'\wmstruct.nii,1'])]
    [fullfile([paths.res_fMRIssl,'\wfmri_ssl_00315.nii,1'])]
    };
spm_jobman('run',matlabbatch);

% display anatomical image on the other images
spm_orthviews('addcolouredimage', [1:2], [workdir.code,'\steps\mask\DAch_anat_red.nii'], [0 0 1]); % anatomical image (not included in this pipeline!)
spm_orthviews('Redraw');
spm_orthviews('Reposition',[5 -20 -21]);
spm_orthviews('Xhairs','on');
spm_orthviews('Zoom',60);

matlabbatch2{1}.spm.util.print.fname = ([details.subjname,'_mask_overlay_ssl']);
matlabbatch2{1}.spm.util.print.fig.fighandle = NaN;
matlabbatch2{1}.spm.util.print.opts = 'pdf';
spm_jobman('run',matlabbatch2);

copyfile ([details.subjname,'_mask_overlay_ssl_001.pdf'], [paths.quality_ssl_prep,details.subjname,'_mask_overlay_ssl.pdf']);


