function contour_display(i,o)
% subfunction from SPM12: spm_ov_contour(varargin)
% Contour tool - plugin for spm_orthviews
%
% This routine is a plugin to spm_orthviews. For general help about
% spm_orthviews and plugins type
%             help spm_orthviews
% at the MATLAB prompt.
%__________________________________________________________________________
% Copyright (C) 2014-2016 Wellcome Trust Centre for Neuroimaging

% Guillaume Flandin
% $Id: spm_ov_contour.m 6774 2016-04-20 12:25:08Z guillaume $

global st

if nargin < 2
    o = spm_input('Select image(s)', '!+1', 'e', ...
        num2str(spm_orthviews('valid_handles')));
    o = intersect(spm_orthviews('valid_handles'),o);
elseif isinf(o)
    o = spm_orthviews('valid_handles');
elseif isnan(o)
    o = setxor(spm_orthviews('valid_handles'),i);
end

try
    hM = findobj(st.vols{i}.ax{1}.cm,'Label','Contour');
    UD = get(hM,'UserData');
    nblines = UD.nblines;
    linestyle = UD.style;
catch
    nblines = 3; % default = 3
    linestyle = 'r-';
end
linewidth = 1;
nblines = 2; % added
contour_delete(i);

lh = {};
sw = warning('off','MATLAB:contour:ConstantData');
for d = 1:3
    CData = sqrt(sum(get(st.vols{i}.ax{d}.d,'CData').^2, 3));
    CData(isinf(CData)) = NaN;
    CData(isnan(CData)) = 0;
    for h = o(:)'
        set(st.vols{h}.ax{d}.ax,'NextPlot','add');
        [C,lh{end+1}] = ...
            contour(st.vols{h}.ax{d}.ax,CData,...
            nblines,linestyle,'LineWidth',linewidth);
    end
end
warning(sw);
set(cat(1,lh{:}),'HitTest','off');

st.vols{i}.contour.images = o;
st.vols{i}.contour.handles = lh;

%==========================================================================
function contour_delete(i)

global st

try, delete(cat(1,st.vols{i}.contour.handles{:})); end