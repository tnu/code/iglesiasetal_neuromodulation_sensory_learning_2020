function ADPRSI_antfMRI_ssl_corr_eps_da
% ADPRSI_antfMRI_ssl_corr_eps_da: computes for every subject the
% correlations between the precision-weighted PEs (epsilon) and the PEs
% (da)

% - GLM1 - epsilons:
%     -epsilon2
%     -epsilon3
%     -epsilon_ch

% - GLM2 - deltas:
%     -da1
%     -da2
%     -da_ch

%   IN:     -
%   OUT:    -

% 19-02-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================
% Set paths
workdir = ADPRSI_antfMRI_ssl_set_workdir;
options = ADPRSI_antfMRI_ssl_set_analysis_options;
[ details, paths ] = ADPRSI_antfMRI_ssl_subjects( 1 );

% Start diary
diary(paths.logfile_hgf_ssl);

%% find the winning model
WinningModel_BMS = load((fullfile(paths.res_hgf_ssl_bms,'/antfMRI_BMS_hgfv1_0.mat')));
[WinningModel_BMS.M,WinningModel_BMS.I] = max(WinningModel_BMS.exp_r);

maskModel = {'/HGF_fixom_v1',  '/HGF_2l_v1', '/RW_v1'};
WinningModel = maskModel{WinningModel_BMS.I};
disp('****************');
disp(WinningModel);
disp('****************');

% load structure with final list of excluded participants:
subj_out = load([paths.res_group_ssl_glm,'/sslbin/fixom_bin3_spm12_fact/antfMRI_ssl_group.mat']);
disp(['subjects out: ', num2str(subj_out.out_final')]);

% load descriptives:
ant_ssl = load([paths.res_group_ssl_descr,'/ADPRSI_antfMRI_ssl.mat']);

% determine number of subjects in final analysis:
Nr_subjects = length(ant_ssl.subj)-length(subj_out.out_final);
indices_subjects = [1:1:length(ant_ssl.subj)];
indices_subjects(subj_out.out_final)=[];

% Go through scans
for id = indices_subjects
    [ details, paths ] = ADPRSI_antfMRI_ssl_subjects( id );
    clear SSL_PE_str;
    clear SSL_PE_corr;
    
    %% load input regressors split for the conditions face and house:
    SSL_PE_corr(id)= load([paths.res_first_ssl,'/GLM_HGF_fixom_all.mat']);
    % columns in SSL_PE_corr.SSL_input.data correspond to:
    % GLM_HGF_fixom_all.SSL_input.data = [VSssl.TargetInput GLM_HGF_fixom_all.SSL_input.TimeCue VSssl.abs_epsilon2 VSssl.abs_da1 VSssl.sa1hat VSssl.sa1 ... %6
    %     VSssl.sa2hat VSssl.sa2 VSssl.sa3hat VSssl.sa3 VSssl.epsilon3 VSssl.da2 VSssl.mu1hat VSssl.mu1 VSssl.mu2hat VSssl.mu2 VSssl.mu3hat VSssl.mu3 ... %18
    %     VSssl.da_ch VSssl.epsilon_ch VSssl.pw3 GLM_HGF_fixom_all.SSL_input.TimeTarget]; %22
    
    % where to store subject-specific correlations:
    cd (paths.res_first_ssl);
    mkdir corr_PEs_SSL
    cd ([paths.res_first_ssl,'/corr_PEs_SSL/']);
    
    %% select epsilons and das
    SSL_PE_str(id).all_eps_da=[SSL_PE_corr(id).SSL_input.data(:,3), SSL_PE_corr(id).SSL_input.data(:,11), SSL_PE_corr(id).SSL_input.data(:,20), ...
        SSL_PE_corr(id).SSL_input.data(:,4), SSL_PE_corr(id).SSL_input.data(:,12), SSL_PE_corr(id).SSL_input.data(:,19)];
    
    %% compute correlation between epsilons and das
    [SSL_PE_str(id).corr_eps_da.r p] = corrcoef(SSL_PE_str(id).all_eps_da);
    figure('Name', 'Correlations eps/da','Color', [1 1 1]);
    imagesc(SSL_PE_str(id).corr_eps_da.r,[-1 1]);colorbar;
    set(gca, 'XTick', [1:6], 'XTickLabel', {'epsilon2' 'epsilon3 ' 'epsilon ch' 'da1' 'da2' 'da ch'});
    set(gca, 'YTick', [1:6], 'YTickLabel', {'epsilon2' 'epsilon3 ' 'epsilon ch' 'da1' 'da2' 'da ch'});
    ax=gca;
    ax.XTickLabelRotation = 45;
    title('Correlations between epsilon & da');
    tString = num2str(SSL_PE_str(id).corr_eps_da.r(:), '%0.2f');    % Create strings
    tString = strtrim(cellstr(tString));                            % Remove any space
    [x,y] = meshgrid(1:6);                                          % Create x and y coordinates for the strings
    hStrings = text(x(:),y(:),tString(:),...                        % Plot the strings
        'HorizontalAlignment','center');
    textColors = repmat(SSL_PE_str(id).corr_eps_da.r(:) > 0.5,1,3)|repmat(SSL_PE_str(id).corr_eps_da.r(:) < -0.5,1,3);
    set(hStrings,{'Color'},num2cell(~textColors,2));
    
    % save figures and results:
    print -djpeg Corr_epsilon_da;
    saveas(gcf,'Corr_epsilon_da','fig');
    copyfile ('Corr_epsilon_da.jpg', [paths.quality_ssl_first,'/Corr_epsilon_da',details.subjname,'.jpg']);

    save('SSL_PE_str.mat','SSL_PE_str');

    %% transform to z-values:
    SSL_PE_str(id).corr_eps_da.z = fisherz(SSL_PE_str(id).corr_eps_da.r);

    %% load single subject correlation matrix, compute mean over all subjects
    %% and plot mean correlations over all regressors and for each condition
    clear tString
    clear x
    clear y
    clear hStrings
    clear textColors
    
    SSL_PE_corr_all.corr_eps_da.r{id} = cat(3,SSL_PE_str(id).corr_eps_da.r);
    SSL_PE_corr_all.corr_eps_da.dr = cat(3,SSL_PE_corr_all.corr_eps_da.r{:});
    SSL_PE_corr_all.corr_eps_da.z{id} = cat(3,SSL_PE_str(id).corr_eps_da.z);
    SSL_PE_corr_all.corr_eps_da.dz = cat(3,SSL_PE_corr_all.corr_eps_da.z{:});
    
    cd([paths.quality_ssl_first]);
    save('SSL_PE_corr_all', '-struct','SSL_PE_corr_all');
    
    %% compute group mean over z-transformed coefficients and transform back
    SSL_PE_corr_all.mean_z = mean(SSL_PE_corr_all.corr_eps_da.dz,3);
    SSL_PE_corr_all.mean_r = ifisherz(SSL_PE_corr_all.mean_z);
    
    cd([paths.quality_ssl_first]);
    save('SSL_PE_corr_all', '-struct','SSL_PE_corr_all');
    
    % plot mean correlations:
    figure('Name', 'Mean correlations eps/da', 'Color', [1 1 1]);
    
    corr_epsilon_da = SSL_PE_corr_all.mean_r(:,:);
    imagesc(corr_epsilon_da,[-1 1]);colorbar;
    set(gca, 'XTick', [1:6], 'XTickLabel', {'epsilon2' 'epsilon3 ' 'epsilon ch' 'da1' 'da2' 'da ch'});
    set(gca, 'YTick', [1:6], 'YTickLabel', {'epsilon2' 'epsilon3 ' 'epsilon ch' 'da1' 'da2' 'da ch'});ax=gca;
    ax.XTickLabelRotation = 45;
    title('Mean correlations between epsilon & da');
    tString = num2str(corr_epsilon_da(:), '%0.2f');     % Create strings
    tString = strtrim(cellstr(tString));                % Remove any space
    [x,y] = meshgrid(1:6);                              % Create x and y coordinates for the strings
    hStrings = text(x(:),y(:),tString(:),...            % Plot the strings
        'HorizontalAlignment','center');
    textColors = repmat(corr_epsilon_da(:) > 0.5,1,3)|repmat(corr_epsilon_da(:) < -0.5,1,3);
    set(hStrings,{'Color'},num2cell(~textColors,2));
    print -djpeg Mean_SSL_PE_corr;
    saveas(gcf,'Mean_SSL_PE_corr','fig');
    
    close all;
end


