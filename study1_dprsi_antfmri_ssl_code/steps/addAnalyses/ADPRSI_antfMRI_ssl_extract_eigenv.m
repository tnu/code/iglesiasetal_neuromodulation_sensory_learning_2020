function ADPRSI_antfMRI_ssl_extract_eigenv(m,r,c)
% ADPRSI_antfMRI_ssl_extract_eigenv:
% - extracts first eigenvariate for each computational quantity separately:
%   * across SN/VTA mask
%   * across PPT/LDT mask
%   * across Basal Forebrain (BF) mask

% output will be entered in a Bayesian One-Sample T-Test (average effects) and
% Bayesian ANCOVA (drug-effects)

%   IN:     m                   - first level model
%                                   1 = epsilon (GLM1)
%                                   2 = delta   (GLM2)
%           r                   - regressor, i.e. computational state
%                                 if m == 1
%                                   1 = epsilon2
%                                   2 = epsilon3
%                                   3 = epsilon_ch

%                                 if m == 2
%                                   1 = da1
%                                   2 = da2
%                                   3 = da_ch
%                                   4 = pw2
%                                   5 = pw3
%           c                    - contrast,
%                                   1 = select average effect
%                                   2 = go through differential contrasts

% Steps - average effects:
% 1. select 2nd-level whole-brain SPM.mat file for one of the
% compuatational states (e.g. epsilon2)
% 2. select contrast for average effect of epsilon2 across all pharmacological conditions
% 3. extract first eigenvariate within one of the three anatomical masks (SN/VTA, PPT/LDT, or BF)
% 4. save results in structure

% 28-07-2020; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

% Set paths
workdir = ADPRSI_antfMRI_ssl_set_workdir;
options = ADPRSI_antfMRI_ssl_set_analysis_options;
[ details, paths ] = ADPRSI_antfMRI_ssl_subjects( 1 );

% Start diary
diary(paths.logfile_group_ssl);

% Initialize
disp(['running "extract eigenvariate; model: ', num2str(m),' (regressor: ',num2str(r),')']);
spm('defaults', 'fmri');

sep1 = '===============================================================================================';
sep2 = '-----------------------------------------------------------------------------------------------';
sep3 = '***********************************************************************************************';
antfMRI_ssl_results.region = 'mask';

% specify where 2nd-level results have been stored
antfMRI_ssl_results.Bin_folder = {'fixom_bin3_spm12_fact', 'fixom_bin5_spm12_fact'};

if m == 1 % GLM1
    antfMRI_ssl_results.PM = {'epsilon2','epsilon3', 'epsilon_ch', 'SSL'};
    antfMRI_ssl_results.firstlevel_contrasts = {'con_0009','con_0011','con_0015'};
    antfMRI_ssl_results.firstlevel_con = antfMRI_ssl_results.firstlevel_contrasts{r};
    antfMRI_ssl_results.PM_n = {'1','2','3'};
    antfMRI_ssl_results.PM_name = {'\epsilon_2', '\epsilon_3', '\epsilon_c','_h'};
elseif m == 2 % GLM2
    antfMRI_ssl_results.PM = {'da1','da2', 'wCPE', 'pw2', 'pw3'};
    antfMRI_ssl_results.firstlevel_contrasts = {'con_0013','con_0015','con_0017','con_0019','con_0023'};
    antfMRI_ssl_results.firstlevel_con = antfMRI_ssl_results.firstlevel_contrasts{r};
    antfMRI_ssl_results.PM_n = {'1','2','3','4','5'};
    antfMRI_ssl_results.PM_name = {'\delta_1', '\delta_2', '\delta_c','_h', 'psi_2', 'psi_3'};
end
disp(sep3);
%% specify contrasts for differential effecst
antfMRI_ssl_results.con_drugeff = [7:12, 15:20]; % differential (i.e. drug) effects
antfMRI_ssl_results.con_aveff = [13]; % average effects
%% load data:
% load structural image:
antfMRI_ssl_results.paths.fileStruct = fullfile([paths.res_group_ssl_glm,'/mean_struct_antfMRI_ssl.img']);
% load anatomical masks:
antfMRI_ssl_results.paths.SNVTA = fullfile([workdir.code,'/steps/mask/SNVTA_ROI.img']);
antfMRI_ssl_results.paths.PPTLDT = fullfile([workdir.code,'/steps/mask/LDT_PPT_012013.img']);
antfMRI_ssl_results.paths.BF = fullfile([workdir.code,'/steps/mask/ROI_BF_AnatomyToolbox_MNI.img']);
antfMRI_ssl_results.masks = {'SNVTA', 'PPTLDT', 'BF'};
% load vector of outliers
SubjOut = load([paths.res_group_ssl_glm,'/sslbin/', antfMRI_ssl_results.Bin_folder{m},'/antfMRI_ssl_group.mat']);
% load descriptives:
ant_ssl = load([paths.res_group_ssl_descr,'/ADPRSI_antfMRI_ssl.mat']);
% count number of subjects per condition:
drug = ant_ssl.group;
drug(SubjOut.out_final) = [];
antfMRI_ssl_results.pla_nr_subj = numel(drug(drug == 0));
antfMRI_ssl_results.ami_nr_subj = numel(drug(drug == 1));
antfMRI_ssl_results.bip_nr_subj = numel(drug(drug == 2));

%% specify folders
antfMRI_ssl_results.paths.dirResults = ([paths.res_group_ssl_glm,'/sslbin/', antfMRI_ssl_results.Bin_folder{m},'/mask/',antfMRI_ssl_results.PM{r},'_basic_DACh']);

antfMRI_ssl_results.paths.dirGlm = ([paths.res_group_ssl_glm,'/sslbin/', antfMRI_ssl_results.Bin_folder{m},'/mask/',antfMRI_ssl_results.PM{r},'_basic_DACh/SPM.mat']);

%% select contrast and extract eigenvariate
if c == 1
    %% define effects of interest contrasts
    if ~exist([paths.res_group_ssl_glm,'/sslbin/', antfMRI_ssl_results.Bin_folder{m},'/mask/',antfMRI_ssl_results.PM{r},'_basic_DACh/ess_0022.nii'])
        matlabbatch{1}.spm.stats.con.spmmat = {antfMRI_ssl_results.paths.dirGlm};
        matlabbatch{1}.spm.stats.con.consess{1}.fcon.name = 'effects of interest'; %effects of interest contrast factor group
        matlabbatch{1}.spm.stats.con.consess{1}.fcon.weights = eye(3);
        matlabbatch{1}.spm.stats.con.consess{1}.fcon.sessrep = 'none';
        matlabbatch{1}.spm.stats.con.consess{2}.fcon.name = 'effects of interest - weight'; %effects of interest contrast covariate weight
        matlabbatch{1}.spm.stats.con.consess{2}.fcon.weights = [zeros(3,3),eye(3)];
        matlabbatch{1}.spm.stats.con.consess{2}.fcon.sessrep = 'none';
        matlabbatch{1}.spm.stats.con.delete = 0;
        spm_jobman('run',{matlabbatch}); %run_nogui
        clear matlabbatch
    end
    
    %loop through contrasts
    for i = 1:numel(antfMRI_ssl_results.con_aveff)
        
        %loop through anatomical masks
        for v = 1:numel(antfMRI_ssl_results.masks)
            %%define contast index:
            antfMRI_ssl_results.contrast_index = antfMRI_ssl_results.con_aveff(i);
            %-----------------------------------------------------------------------
            matlabbatch{1}.spm.stats.results.spmmat = {antfMRI_ssl_results.paths.dirGlm};
            matlabbatch{1}.spm.stats.results.conspec.titlestr = '';
            matlabbatch{1}.spm.stats.results.conspec.contrasts = antfMRI_ssl_results.contrast_index;
            matlabbatch{1}.spm.stats.results.conspec.threshdesc = 'none';
            matlabbatch{1}.spm.stats.results.conspec.thresh = 1;
            matlabbatch{1}.spm.stats.results.conspec.extent = 0;
            matlabbatch{1}.spm.stats.results.conspec.conjunction = 1;
            matlabbatch{1}.spm.stats.results.conspec.mask.none = 1;
            matlabbatch{1}.spm.stats.results.units = 1;
            matlabbatch{1}.spm.stats.results.export = cell(1, 0);
            
            spm_jobman('run',{matlabbatch});
            
            SPM = evalin('base','SPM');
            xSPM = evalin('base','xSPM');
            hReg = evalin('base','hReg');
            TabDat = evalin('base','TabDat');
            
            xY.name = [num2str(antfMRI_ssl_results.contrast_index),'_',sprintf('sphere_%04d',c),'_', ...
                num2str(m),'_',antfMRI_ssl_results.PM{r}, '_', antfMRI_ssl_results.masks{v}];
            xY.def = 'mask';
            if v == 1
                xY.spec = spm_vol(antfMRI_ssl_results.paths.SNVTA);
            elseif v == 2
                xY.spec = spm_vol(antfMRI_ssl_results.paths.PPTLDT);
            elseif v == 3
                xY.spec = spm_vol(antfMRI_ssl_results.paths.BF);
            end
            xY.Ic = 22; % effects of interest contrast over factor group
            [Y,xY] = spm_regions(xSPM,SPM,hReg,xY);
            clear xY;
        end
    end
elseif c == 2
    %% define effects of interest contrasts
    if ~exist([paths.res_group_ssl_glm,'/sslbin/', antfMRI_ssl_results.Bin_folder{m},'/',antfMRI_ssl_results.PM{r},'_basic/ess_0022.nii'])
        matlabbatch{1}.spm.stats.con.spmmat = {antfMRI_ssl_results.paths.dirGlm};
        matlabbatch{1}.spm.stats.con.consess{1}.fcon.name = 'effects of interest'; %effects of interest contrast factor group
        matlabbatch{1}.spm.stats.con.consess{1}.fcon.weights = eye(3);
        matlabbatch{1}.spm.stats.con.consess{1}.fcon.sessrep = 'none';
        matlabbatch{1}.spm.stats.con.consess{2}.fcon.name = 'effects of interest - weight'; %effects of interest contrast covariate weight
        matlabbatch{1}.spm.stats.con.consess{2}.fcon.weights = [zeros(3,3),eye(3)];
        matlabbatch{1}.spm.stats.con.consess{2}.fcon.sessrep = 'none';
        matlabbatch{1}.spm.stats.con.delete = 0;
        spm_jobman('run',{matlabbatch}); %run_nogui
        clear matlabbatch
    end
    for i = 1:numel(antfMRI_ssl_results.con_drugeff)
        %loop through anatomical masks
        for v = 1:numel(antfMRI_ssl_results.masks)
            %%define contast index:
            antfMRI_ssl_results.contrast_index = antfMRI_ssl_results.con_drugeff(i);
            %-----------------------------------------------------------------------
            matlabbatch{1}.spm.stats.results.spmmat = {antfMRI_ssl_results.paths.dirGlm};
            matlabbatch{1}.spm.stats.results.conspec.titlestr = '';
            matlabbatch{1}.spm.stats.results.conspec.contrasts = antfMRI_ssl_results.contrast_index;
            matlabbatch{1}.spm.stats.results.conspec.threshdesc = 'none';
            matlabbatch{1}.spm.stats.results.conspec.thresh = 1;
            matlabbatch{1}.spm.stats.results.conspec.extent = 0;
            matlabbatch{1}.spm.stats.results.conspec.conjunction = 1;
            matlabbatch{1}.spm.stats.results.conspec.mask.none = 1;
            matlabbatch{1}.spm.stats.results.units = 1;
            matlabbatch{1}.spm.stats.results.export = cell(1, 0);
            spm_jobman('run',{matlabbatch});
            
            SPM = evalin('base','SPM');
            xSPM = evalin('base','xSPM');
            hReg = evalin('base','hReg');
            TabDat = evalin('base','TabDat');
            
            xY.name = [num2str(antfMRI_ssl_results.contrast_index),'_',sprintf('sphere_%04d',c),'_', ...
                num2str(m),'_',antfMRI_ssl_results.PM{r}, '_', antfMRI_ssl_results.masks{v}];
            xY.def = 'mask';
            if v == 1
                xY.spec = spm_vol(antfMRI_ssl_results.paths.SNVTA);
            elseif v == 2
                xY.spec = spm_vol(antfMRI_ssl_results.paths.PPTLDT);
            elseif v == 3
                xY.spec = spm_vol(antfMRI_ssl_results.paths.BF);
            end
            if ismember(antfMRI_ssl_results.contrast_index,7:12)
                xY.Ic = 22; % effects of interest contrast over factor group
            elseif ismember(antfMRI_ssl_results.contrast_index,15:20)
                xY.Ic = 23; % effects of interest contrast over covariate weight
            end
            [Y,xY] = spm_regions(xSPM,SPM,hReg,xY);
            clear xY;
        end
    end
end

