function ADPRSI_antfMRI_ssl_sum_eigenv(m,r)
% ADPRSI_antfMRI_ssl_sum_eigenv:
% - summarise in a structure the extracted first eigenvariates for each computational quantity separately:
%   * across SN/VTA mask
%   * across PPT/LDT mask
%   * across Basal Forebrain (BF) mask

% output will be entered in a Bayesian One-Sample T-Test (average effects) and
% Bayesian ANCOVA (drug-effects)

%   IN:     m                   - first level model
%                                   1 = epsilon (GLM1)
%                                   2 = delta   (GLM2)
%           r                   - regressor, i.e. computational state
%                                 if m == 1
%                                   1 = epsilon2
%                                   2 = epsilon3
%                                   3 = epsilon_ch

%                                 if m == 2
%                                   1 = da1
%                                   2 = da2
%                                   3 = da_ch
%                                   4 = pw2
%                                   5 = pw3

% 28-07-2020; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

% Set paths
workdir = ADPRSI_antfMRI_ssl_set_workdir;
options = ADPRSI_antfMRI_ssl_set_analysis_options;
[ details, paths ] = ADPRSI_antfMRI_ssl_subjects( 1 );

% Start diary
diary(paths.logfile_group_ssl);

% Initialize
disp(['running "extract eigenvariate; model: ', num2str(m),' (regressor: ',num2str(r),')']);
spm('defaults', 'fmri');

sep1 = '===============================================================================================';
sep2 = '-----------------------------------------------------------------------------------------------';
sep3 = '***********************************************************************************************';
antfMRI_ssl_results.region = 'mask';

% specify where 2nd-level results have been stored
antfMRI_ssl_results.Bin_folder = {'fixom_bin3_spm12_fact', 'fixom_bin5_spm12_fact'};

if m == 1 % GLM1
    antfMRI_ssl_results.PM = {'epsilon2','epsilon3', 'epsilon_ch', 'SSL'};
    antfMRI_ssl_results.firstlevel_contrasts = {'con_0009','con_0011','con_0015'};
    antfMRI_ssl_results.firstlevel_con = antfMRI_ssl_results.firstlevel_contrasts{r};
    antfMRI_ssl_results.PM_n = {'1','2','3'};
    antfMRI_ssl_results.PM_name = {'\epsilon_2', '\epsilon_3', '\epsilon_c','_h'};
elseif m == 2 % GLM2
    antfMRI_ssl_results.PM = {'da1','da2', 'wCPE', 'pw2', 'pw3'};
    antfMRI_ssl_results.firstlevel_contrasts = {'con_0013','con_0015','con_0017','con_0019','con_0023'};
    antfMRI_ssl_results.firstlevel_con = antfMRI_ssl_results.firstlevel_contrasts{r};
    antfMRI_ssl_results.PM_n = {'1','2','3','4','5'};
    antfMRI_ssl_results.PM_name = {'\delta_1', '\delta_2', '\delta_c','_h', 'psi_2', 'psi_3'};
end
disp(sep3);
%% specify contrasts:
% first eigenvariates for contrasts 7:13 are identical, but different from contrasts 15:20
% therefore, only two VOIs need to be selected
antfMRI_ssl_results.con_drug_aveff = [13]; % VOI for the average effect and differential (i.e. drug) effects
antfMRI_ssl_results.con_drug_weighteff = [15]; % VOI for the body-weight dependent drug effects
%% specify masks:
antfMRI_ssl_results.masks = {'SNVTA', 'PPTLDT', 'BF'};
%% load data:
% load vector of outliers
SubjOut = load([paths.res_group_ssl_glm,'/sslbin/', antfMRI_ssl_results.Bin_folder{m},'/antfMRI_ssl_group.mat']);
% load descriptives:
ant_ssl = load([paths.res_group_ssl_descr,'/ADPRSI_antfMRI_ssl.mat']);

%% specify folders
antfMRI_ssl_results.paths.dirResults = ([paths.res_group_ssl_glm,'/sslbin/', antfMRI_ssl_results.Bin_folder{m},'/mask/',antfMRI_ssl_results.PM{r},'_basic_DACh']);
antfMRI_ssl_results.paths.dirResults_VOI = ([paths.res_group_ssl_glm,'/sslbin/JASP']);
[~,~] = mkdir(antfMRI_ssl_results.paths.dirResults_VOI);
%% load VOIs
for v = 1:numel(antfMRI_ssl_results.masks)
    c=1; %VOI extracted adjusting for F-contrast over factor 1
    V = load([antfMRI_ssl_results.paths.dirResults, '/VOI_',num2str(antfMRI_ssl_results.con_drug_aveff),'_',sprintf('sphere_%04d',c),'_', ...
        num2str(m),'_',antfMRI_ssl_results.PM{r}, '_', antfMRI_ssl_results.masks{v},'.mat']);
    
    ADPRSI_antfMRI_VOI_F1(:,v) = V.Y;
    clear V;
end

for v = 1:numel(antfMRI_ssl_results.masks)
    c=2; %VOI extracted adjusting for F-contrast over covariate body weight
    V = load([antfMRI_ssl_results.paths.dirResults, '/VOI_',num2str(antfMRI_ssl_results.con_drug_weighteff),'_',sprintf('sphere_%04d',c),'_', ...
        num2str(m),'_',antfMRI_ssl_results.PM{r}, '_', antfMRI_ssl_results.masks{v},'.mat']);
    
    ADPRSI_antfMRI_VOI_weight(:,v) = V.Y;
    clear V;
end


%% reorder covariates and remove entries of excluded subjects:
cov_kss = ant_ssl.behav.KSS;
cov_kss(SubjOut.out_final) = [];
cov_weight = ant_ssl.weight;
cov_weight(SubjOut.out_final) = [];
drug = ant_ssl.group;
drug(SubjOut.out_final) = [];
subj_name = ant_ssl.subj;
subj_name(SubjOut.out_final) = [];
cov_kss_pla = cov_kss(drug == 0);
cov_kss_ami = cov_kss(drug == 1);
cov_kss_bip = cov_kss(drug == 2);
cov_weight_pla = cov_weight(drug == 0);
cov_weight_ami = cov_weight(drug == 1);
cov_weight_bip = cov_weight(drug == 2);
cov_kss = [cov_kss_ami; cov_kss_bip; cov_kss_pla];
cov_weight = [cov_weight_ami; cov_weight_bip; cov_weight_pla];

% substance
drug_new = [ones(numel(cov_kss_ami),1); 2*ones(numel(cov_kss_bip),1);  0*ones(numel(cov_kss_pla),1)];

%% summarise VOIs:
ADPRSI_antfMRI_ssl_VOI.data = [drug_new, cov_weight, cov_kss, ...
    ADPRSI_antfMRI_VOI_F1, ADPRSI_antfMRI_VOI_weight];
ADPRSI_antfMRI_ssl_VOI.data_string = [{'Substance'}, {'Weight'}, {'KSS'}, ...
    {['F1_',antfMRI_ssl_results.PM{r},'_',antfMRI_ssl_results.masks{1}]}, ...
    {['F1_',antfMRI_ssl_results.PM{r},'_',antfMRI_ssl_results.masks{2}]}, ...
    {['F1_',antfMRI_ssl_results.PM{r},'_',antfMRI_ssl_results.masks{3}]}, ...
    {['W_',antfMRI_ssl_results.PM{r},'_',antfMRI_ssl_results.masks{1}]}, ...
    {['W_',antfMRI_ssl_results.PM{r},'_',antfMRI_ssl_results.masks{2}]}, ...
    {['W_',antfMRI_ssl_results.PM{r},'_',antfMRI_ssl_results.masks{3}]}];

cd(antfMRI_ssl_results.paths.dirResults_VOI);
save(['ADPRSI_antfMRI_ssl_VOI_',antfMRI_ssl_results.PM{r},'.mat'], '-struct','ADPRSI_antfMRI_ssl_VOI');
writetable(array2table(ADPRSI_antfMRI_ssl_VOI.data, 'VariableNames', ADPRSI_antfMRI_ssl_VOI.data_string), ['ADPRSI_antfMRI_ssl_VOI_',antfMRI_ssl_results.PM{r}], 'FileType', 'text');
end

