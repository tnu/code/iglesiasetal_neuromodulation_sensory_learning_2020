function [coord_vector, hfig1] = ADPRSI_antfMRI_ssl_plot_main_effects_eigenvariate(antfMRI_ssl_results,v,j,m,r,x_text,verbose)
% ADPRSI_antfMRI_ssl_plot_main_effects_eigenvariate: extract and plot first
% eigenvariate
%
% 16-01-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================
%% Initialize
sep1 = '===============================================================================================';
sep2 = '-----------------------------------------------------------------------------------------------';
sep3 = '***********************************************************************************************';

% define cluster-size
cluster_size=verbose(1);

cd(antfMRI_ssl_results.paths.dirResults);

if strcmp(antfMRI_ssl_results.mode,'cluster_level_sign')==1 || strcmp(antfMRI_ssl_results.mode,'cluster_level_notsign')==1
    disp('plotting cluster-level results')
    job.spm.stats.results.spmmat = {antfMRI_ssl_results.paths.dirGlm};
    job.spm.stats.results.conspec.titlestr = '';
    job.spm.stats.results.conspec.contrasts = antfMRI_ssl_results.contrast_index;
    job.spm.stats.results.conspec.threshdesc = 'none';
    job.spm.stats.results.conspec.thresh = 0.001;
    job.spm.stats.results.conspec.extent = cluster_size;
    job.spm.stats.results.conspec.conjunction = 1;
    job.spm.stats.results.conspec.mask.none = 1;
    job.spm.stats.results.units = 1;
    job.spm.stats.results.print = 0;
    job.spm.stats.results.write.none = 1;
    
    spm_jobman('run',{job});
else
    disp('plotting peak-level results')
    job.spm.stats.results.spmmat = {antfMRI_ssl_results.paths.dirGlm};
    job.spm.stats.results.conspec.titlestr = '';
    job.spm.stats.results.conspec.contrasts = antfMRI_ssl_results.contrast_index;
    job.spm.stats.results.conspec.threshdesc = 'FWE';
    
    if strcmp(antfMRI_ssl_results.mode,'peak_level_sign')==1
        job.spm.stats.results.conspec.thresh = 0.05;
    else
        job.spm.stats.results.conspec.thresh = 0.1;
    end
    job.spm.stats.results.conspec.extent = 0;
    job.spm.stats.results.conspec.conjunction = 1;
    job.spm.stats.results.conspec.mask.none = 1;
    job.spm.stats.results.units = 1;
    job.spm.stats.results.print = 0;
    job.spm.stats.results.write.none = 1;
    
    spm_jobman('run',{job});
end
figHandles = get(0,'Children');
FGraph = figHandles(1,1);
saveas(FGraph, ['SPM_',antfMRI_ssl_results.PM_n{r},'_',num2str(1),'_',antfMRI_ssl_results.PM{r},'_contrast_',antfMRI_ssl_results.mode,'_',antfMRI_ssl_results.region,'_',num2str(antfMRI_ssl_results.contrast_index),'_',num2str(j)],'fig');
saveas(FGraph, ['SPM_',antfMRI_ssl_results.PM_n{r},'_',num2str(1),'_',antfMRI_ssl_results.PM{r},'_contrast_',antfMRI_ssl_results.mode,'_',antfMRI_ssl_results.region,'_',num2str(antfMRI_ssl_results.contrast_index),'_',num2str(j)],'jpg');

SPM = evalin('base','SPM');
xSPM = evalin('base','xSPM');
hReg = evalin('base','hReg');
TabDat = evalin('base','TabDat');

%% define effects of interest contrasts
if ~exist([antfMRI_ssl_results.paths.dirGlmResults,'/ess_0022.nii'])
    matlabbatch{1}.spm.stats.con.spmmat = {antfMRI_ssl_results.paths.dirGlm};
    matlabbatch{1}.spm.stats.con.consess{1}.fcon.name = 'effects of interest';          % effects of interest contrast factor group
    matlabbatch{1}.spm.stats.con.consess{1}.fcon.weights = eye(3);
    matlabbatch{1}.spm.stats.con.consess{1}.fcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.fcon.name = 'effects of interest - weight'; % effects of interest contrast covariate weight
    matlabbatch{1}.spm.stats.con.consess{2}.fcon.weights = [zeros(3,3),eye(3)];
    matlabbatch{1}.spm.stats.con.consess{2}.fcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 0;
    spm_jobman('run',{matlabbatch}); % run_nogui
    clear matlabbatch
    SPM = load(antfMRI_ssl_results.paths.dirGlm);
    SPM = SPM.SPM;
end

%% VOI extraction - needed to find all voxels within cluster
disp('extract eigenvariate');
disp(sep2);
disp(['contrast: ',num2str(antfMRI_ssl_results.contrast_index)]);
disp(sep1);

antfMRI_ssl_results.cluster.plot(j).coord = antfMRI_ssl_results.ResData{v(j),12};
p1 = num2str(antfMRI_ssl_results.cluster.plot(j).coord(1,1));
p2 = num2str(antfMRI_ssl_results.cluster.plot(j).coord(2,1));
p3 = num2str(antfMRI_ssl_results.cluster.plot(j).coord(3,1));
coord_vector = antfMRI_ssl_results.cluster.plot(j).coord';

disp(coord_vector);
disp(sep1);

% extract first eigenvariate
xY.xyz = spm_mip_ui('SetCoords',coord_vector');
xY.name = [num2str(antfMRI_ssl_results.contrast_index),'_',sprintf('sphere_%04d',j),'_', ...
    num2str(coord_vector(1,1)),'_',num2str(coord_vector(1,2)),'_',num2str(coord_vector(1,3)),'_', ...
    num2str(r),'_',antfMRI_ssl_results.PM{r},'_',antfMRI_ssl_results.mode];
if strcmp(antfMRI_ssl_results.mode,'cluster_level_sign')==1 || strcmp(antfMRI_ssl_results.mode,'cluster_level_notsign')==1
    xY.def = 'cluster';
    xY.spec = 1;
else
    xY.def = 'sphere';
    xY.spec = 0;
end
if antfMRI_ssl_results.contrast_index > 14
    xY.Ic = 23; % adjust: effects of interest contrast over covariate weight
else
    xY.Ic = 22; % adjust: effects of interest contrast over factor group
end
[Y,xY] = spm_regions(xSPM,SPM,hReg,xY);

%% load extracted first eigenvariate
VOI = load(['VOI_',num2str(antfMRI_ssl_results.contrast_index),'_',sprintf('sphere_%04d',j),'_', ...
    num2str(coord_vector(1,1)),'_',num2str(coord_vector(1,2)),'_',num2str(coord_vector(1,3)),'_', ...
    num2str(r),'_',antfMRI_ssl_results.PM{r},'_',antfMRI_ssl_results.mode,'.mat']);

% plot results
if antfMRI_ssl_results.contrast_index > 14 % differential contrasts over covariates
    
    % sort data based on pharmacological condition
    beta_ami = (VOI.Y(1:antfMRI_ssl_results.ami_nr_subj,1));
    beta_bip = (VOI.Y((antfMRI_ssl_results.ami_nr_subj+1):(antfMRI_ssl_results.ami_nr_subj+antfMRI_ssl_results.bip_nr_subj),1));
    beta_pla = (VOI.Y((antfMRI_ssl_results.ami_nr_subj+antfMRI_ssl_results.bip_nr_subj+1):(antfMRI_ssl_results.ami_nr_subj+antfMRI_ssl_results.bip_nr_subj+antfMRI_ssl_results.pla_nr_subj),1));
    % sort body weight based on pharmacological condition
    weight_ami = antfMRI_ssl_results.cov_weight_ami;
    weight_bip = antfMRI_ssl_results.cov_weight_bip;
    weight_pla = antfMRI_ssl_results.cov_weight_pla;
    
    if antfMRI_ssl_results.contrast_index == 15
        hfig1=figure('Color', [1 1 1]);
        ax1 = subplot(1,2,1);
        s1=scatter(ax1,weight_ami,beta_ami,30,'MarkerEdgeColor',[0.00,0.00,1.00], ...
            'MarkerFaceColor',[0.00,0.00,1.00],'DisplayName','Ami');
        title('amisulpride')
        ylabel('beta estimates','FontSize', 12, 'FontName','Helvetica')
        xlabel('body weight','FontSize', 12, 'FontName','Helvetica')
        
        ax2 = subplot(1,2,2);
        s2=scatter(ax2,weight_bip,beta_bip,30,'MarkerEdgeColor',[0.64,0.08,0.18], ...
            'MarkerFaceColor',[0.64,0.08,0.18],'DisplayName','Bip');
        title('biperiden')
        ylabel('beta estimates','FontSize', 12, 'FontName','Helvetica')
        xlabel('body weight','FontSize', 12, 'FontName','Helvetica')
        hold on;
        
        set(lsline(ax1), 'Color',[0.00,0.00,1.00],'LineWidth',1.5);
        set(lsline(ax2), 'Color',[0.64,0.08,0.18],'LineWidth',1.5);
        ax1.XLim = [50 100];
        ax2.XLim = [50 100];
        
        hold on;
        suptitle('amisulpride > biperiden');
    elseif antfMRI_ssl_results.contrast_index == 16
        hfig1=figure('Color', [1 1 1]);
        ax1 = subplot(1,2,1);
        s1=scatter(ax1,weight_bip,beta_bip,30,'MarkerEdgeColor',[0.64,0.08,0.18], ...
            'MarkerFaceColor',[0.64,0.08,0.18],'DisplayName','Bip');
        title('biperiden')
        ylabel('beta estimates','FontSize', 12, 'FontName','Helvetica')
        xlabel('body weight','FontSize', 12, 'FontName','Helvetica')
        hold on;
        ax2 = subplot(1,2,2);
        s2=scatter(ax2,weight_ami,beta_ami,30,'MarkerEdgeColor',[0.00,0.00,1.00], ...
            'MarkerFaceColor',[0.00,0.00,1.00],'DisplayName','Ami');
        title('amisulpride')
        ylabel('beta estimates','FontSize', 12, 'FontName','Helvetica')
        xlabel('body weight','FontSize', 12, 'FontName','Helvetica')
        
        set(lsline(ax1), 'Color',[0.64,0.08,0.18],'LineWidth',1.5);
        set(lsline(ax2), 'Color',[0.00,0.00,1.00],'LineWidth',1.5);
        ax1.XLim = [50 100];
        ax2.XLim = [50 100];
        
        hold on;
        suptitle('amisulpride < biperiden');
    elseif antfMRI_ssl_results.contrast_index == 17
        hfig1=figure('Color', [1 1 1]);
        ax1 = subplot(1,2,1);
        s1=scatter(ax1,weight_ami,beta_ami,30,'MarkerEdgeColor',[0.00,0.00,1.00], ...
            'MarkerFaceColor',[0.00,0.00,1.00],'DisplayName','Ami');
        title('amisulpride')
        ylabel('beta estimates','FontSize', 12, 'FontName','Helvetica')
        xlabel('body weight','FontSize', 12, 'FontName','Helvetica')
        hold on;
        ax2 = subplot(1,2,2);
        s2=scatter(ax2,weight_pla,beta_pla,30,'MarkerEdgeColor',[0.16,0.67,0.49], ...
            'MarkerFaceColor',[0.16,0.67,0.49],'DisplayName','Pla');
        title('placebo')
        ylabel('beta estimates','FontSize', 12, 'FontName','Helvetica')
        xlabel('body weight','FontSize', 12, 'FontName','Helvetica')
        
        set(lsline(ax1), 'Color',[0.00,0.00,1.00],'LineWidth',1.5);
        set(lsline(ax2), 'Color',[0.16,0.67,0.49],'LineWidth',1.5);
        ax1.XLim = [50 100];
        ax2.XLim = [50 100];
        
        hold on;
        suptitle('amisulpride > placebo');
    elseif antfMRI_ssl_results.contrast_index == 18
        hfig1=figure('Color', [1 1 1]);
        ax1 = subplot(1,2,1);
        s1=scatter(ax1,weight_ami,beta_ami,30,'MarkerEdgeColor',[0.00,0.00,1.00], ...
            'MarkerFaceColor',[0.00,0.00,1.00],'DisplayName','Ami');
        title('amisulpride')
        ylabel('beta estimates','FontSize', 12, 'FontName','Helvetica')
        xlabel('body weight','FontSize', 12, 'FontName','Helvetica')
        hold on;
        ax2 = subplot(1,2,2);
        s2=scatter(ax2,weight_pla,beta_pla,30,'MarkerEdgeColor',[0.16,0.67,0.49], ...
            'MarkerFaceColor',[0.16,0.67,0.49],'DisplayName','Pla');
        title('placebo')
        ylabel('beta estimates','FontSize', 12, 'FontName','Helvetica')
        xlabel('body weight','FontSize', 12, 'FontName','Helvetica')
        
        set(lsline(ax1), 'Color',[0.00,0.00,1.00],'LineWidth',1.5);
        set(lsline(ax2), 'Color',[0.16,0.67,0.49],'LineWidth',1.5);
        ax1.XLim = [50 100];
        ax2.XLim = [50 100];
        
        hold on;
        suptitle('amisulpride < placebo');
        
    elseif antfMRI_ssl_results.contrast_index == 19
        hfig1=figure('Color', [1 1 1]);
        ax1 = subplot(1,2,1);
        s1=scatter(ax1,weight_bip,beta_bip,30,'MarkerEdgeColor',[0.64,0.08,0.18], ...
            'MarkerFaceColor',[0.64,0.08,0.18],'DisplayName','Bip');
        title('biperiden')
        ylabel('beta estimates','FontSize', 12, 'FontName','Helvetica')
        xlabel('body weight','FontSize', 12, 'FontName','Helvetica')
        hold on;
        ax2 = subplot(1,2,2);
        s2=scatter(ax2,weight_pla,beta_pla,30,'MarkerEdgeColor',[0.16,0.67,0.49], ...
            'MarkerFaceColor',[0.16,0.67,0.49],'DisplayName','Pla');
        title('placebo')
        ylabel('beta estimates','FontSize', 12, 'FontName','Helvetica')
        xlabel('body weight','FontSize', 12, 'FontName','Helvetica')
        
        set(lsline(ax1), 'Color',[0.64,0.08,0.18],'LineWidth',1.5);
        set(lsline(ax2), 'Color',[0.16,0.67,0.49],'LineWidth',1.5);
        ax1.XLim = [50 100];
        ax2.XLim = [50 100];
        
        hold on;
        suptitle('biperiden > placebo');
        
    elseif antfMRI_ssl_results.contrast_index == 20
        hfig1=figure('Color', [1 1 1]);
        ax1 = subplot(1,2,1);
        s1=scatter(ax1,weight_bip,beta_bip,30,'MarkerEdgeColor',[0.64,0.08,0.18], ...
            'MarkerFaceColor',[0.64,0.08,0.18],'DisplayName','Bip');
        title('biperiden')
        ylabel('beta estimates','FontSize', 12, 'FontName','Helvetica')
        xlabel('body weight','FontSize', 12, 'FontName','Helvetica')
        hold on;
        ax2 = subplot(1,2,2);
        s2=scatter(ax2,weight_pla,beta_pla,30,'MarkerEdgeColor',[0.16,0.67,0.49], ...
            'MarkerFaceColor',[0.16,0.67,0.49],'DisplayName','Pla');
        title('placebo')
        ylabel('beta estimates','FontSize', 12, 'FontName','Helvetica')
        xlabel('body weight','FontSize', 12, 'FontName','Helvetica')
        
        set(lsline(ax1), 'Color',[0.64,0.08,0.18],'LineWidth',1.5);
        set(lsline(ax2), 'Color',[0.16,0.67,0.49],'LineWidth',1.5);
        ax1.XLim = [50 100];
        ax2.XLim = [50 100];
        
        hold on;
        suptitle('biperiden < placebo');
    end
else % differential contrasts on factor group
    % compute mean, ste and std
    beta_ami = mean(VOI.Y(1:antfMRI_ssl_results.ami_nr_subj,1));
    beta_bip = mean(VOI.Y((antfMRI_ssl_results.ami_nr_subj+1):(antfMRI_ssl_results.ami_nr_subj+antfMRI_ssl_results.bip_nr_subj),1));
    beta_pla = mean(VOI.Y((antfMRI_ssl_results.ami_nr_subj+antfMRI_ssl_results.bip_nr_subj+1):(antfMRI_ssl_results.ami_nr_subj+antfMRI_ssl_results.bip_nr_subj+antfMRI_ssl_results.pla_nr_subj),1));
    
    ste_ami = std(VOI.Y(1:antfMRI_ssl_results.ami_nr_subj,1))/sqrt(antfMRI_ssl_results.ami_nr_subj);
    ste_bip = std(VOI.Y((antfMRI_ssl_results.ami_nr_subj+1):(antfMRI_ssl_results.ami_nr_subj+antfMRI_ssl_results.bip_nr_subj),1))/sqrt(antfMRI_ssl_results.bip_nr_subj);
    ste_pla = std(VOI.Y((antfMRI_ssl_results.ami_nr_subj+antfMRI_ssl_results.bip_nr_subj+1):(antfMRI_ssl_results.ami_nr_subj+antfMRI_ssl_results.bip_nr_subj+antfMRI_ssl_results.pla_nr_subj),1))/sqrt(antfMRI_ssl_results.pla_nr_subj);
    
    std_ami = std(VOI.Y(1:antfMRI_ssl_results.ami_nr_subj,1));
    std_bip = std(VOI.Y((antfMRI_ssl_results.ami_nr_subj+1):(antfMRI_ssl_results.ami_nr_subj+antfMRI_ssl_results.bip_nr_subj),1));
    std_pla = std(VOI.Y((antfMRI_ssl_results.ami_nr_subj+antfMRI_ssl_results.bip_nr_subj+1):(antfMRI_ssl_results.ami_nr_subj+antfMRI_ssl_results.bip_nr_subj+antfMRI_ssl_results.pla_nr_subj),1));
    
    beta_ant = [beta_ami beta_bip beta_pla];
    e_ant = [ste_ami ste_bip ste_pla];
    st_ant = [std_ami std_bip std_pla];
    
    hfig1=figure('Color', [1 1 1]);
    FGraph = hfig1(1,1);
    hResAx = axes('Parent',FGraph);
    
    if antfMRI_ssl_results.contrast_index == 7
        bar([1],beta_ant(1),'FaceColor',[0.00,0.00,1.00]); hold on;%blue tones
        bar([2],beta_ant(2),'FaceColor',[0.64,0.08,0.18]); hold on;
        hold on;
        
        errorbar(beta_ant(1,[1 2]), e_ant(1,[1 2]), 'black','LineStyle','none');
        set(hResAx,'Xcolor',[0 0 0])
        set(hResAx,'XLim',[0 3]); set(hResAx,'XTick',[1:1:2]);
        set(hResAx,'XTickLabel',{'Amisulpride', 'Biperiden', ''}, ...
            'FontSize', 8, 'FontName','Helvetica')
        set(hResAx,'Ycolor',[0 0 0])
        ylabel('beta estimates','FontSize', 12, 'FontName','Helvetica')
        hold on;
        Con_title = 'amisulpride > biperiden';
        t=title({Con_title; ' '}, 'FontWeight', 'bold', 'FontName','Arial');
    elseif antfMRI_ssl_results.contrast_index == 8
        bar([1],beta_ant(1),'FaceColor',[0.00,0.00,1.00]); hold on;%blue tones
        bar([2],beta_ant(2),'FaceColor',[0.64,0.08,0.18]); hold on;
        hold on;
        
        errorbar(beta_ant(1,[1 2]), e_ant(1,[1 2]), 'black','LineStyle','none');
        set(hResAx,'Xcolor',[0 0 0])
        set(hResAx,'XLim',[0 3]); set(hResAx,'XTick',[1:1:2]);
        set(hResAx,'XTickLabel',{'Amisulpride', 'Biperiden', ''}, ...
            'FontSize', 8, 'FontName','Helvetica')
        set(hResAx,'Ycolor',[0 0 0])
        ylabel('beta estimates','FontSize', 12, 'FontName','Helvetica')
        hold on;
        Con_title = 'amisulpride < biperiden';
        t=title({Con_title; ' '}, 'FontWeight', 'bold', 'FontName','Arial');
        
    elseif antfMRI_ssl_results.contrast_index == 9
        bar([1],beta_ant(1),'FaceColor',[0.00,0.00,1.00]); hold on;%blue tones
        bar([2],beta_ant(3),'FaceColor',[0.16,0.67,0.49]); hold on;
        hold on;
        
        errorbar(beta_ant(1,[1 3]), e_ant(1,[1 3]), 'black','LineStyle','none');
        set(hResAx,'Xcolor',[0 0 0])
        set(hResAx,'XLim',[0 3]); set(hResAx,'XTick',[1:1:2]);
        set(hResAx,'XTickLabel',{'Amisulpride', 'Placebo',''}, ...
            'FontSize', 8, 'FontName','Helvetica')
        set(hResAx,'Ycolor',[0 0 0])
        ylabel('beta estimates','FontSize', 12, 'FontName','Helvetica')
        hold on;
        Con_title = 'amisulpride > placebo';
        t=title({Con_title; ' '}, 'FontWeight', 'bold', 'FontName','Arial');
    elseif antfMRI_ssl_results.contrast_index == 10
        bar([1],beta_ant(1),'FaceColor',[0.00,0.00,1.00]); hold on;%blue tones
        bar([2],beta_ant(3),'FaceColor',[0.16,0.67,0.49]); hold on;
        hold on;
        
        errorbar(beta_ant(1,[1 3]), e_ant(1,[1 3]), 'black','LineStyle','none');
        set(hResAx,'Xcolor',[0 0 0])
        set(hResAx,'XLim',[0 3]); set(hResAx,'XTick',[1:1:2]);
        set(hResAx,'XTickLabel',{'Amisulpride', 'Placebo',''}, ...
            'FontSize', 8, 'FontName','Helvetica')
        set(hResAx,'Ycolor',[0 0 0])
        ylabel('beta estimates','FontSize', 12, 'FontName','Helvetica')
        hold on;
        Con_title = 'amisulpride < placebo';
        t=title({Con_title; ' '}, 'FontWeight', 'bold', 'FontName','Arial');
    elseif antfMRI_ssl_results.contrast_index == 11
        bar([1],beta_ant(2),'FaceColor',[0.64,0.08,0.18]); hold on;
        bar([2],beta_ant(3),'FaceColor',[0.16,0.67,0.49]); hold on;
        hold on;
        
        errorbar(beta_ant(1,[2 3]), e_ant(1,[2 3]), 'black','LineStyle','none');
        set(hResAx,'Xcolor',[0 0 0])
        set(hResAx,'XLim',[0 3]); set(hResAx,'XTick',[1:1:2]);
        set(hResAx,'XTickLabel',{'Biperiden', 'Placebo',''}, ...
            'FontSize', 8, 'FontName','Helvetica')
        set(hResAx,'Ycolor',[0 0 0])
        ylabel('beta estimates','FontSize', 12, 'FontName','Helvetica')
        hold on;
        Con_title = 'biperiden > placebo';
        t=title({Con_title; ' '}, 'FontWeight', 'bold', 'FontName','Arial');
    elseif antfMRI_ssl_results.contrast_index == 12
        bar([1],beta_ant(2),'FaceColor',[0.64,0.08,0.18]); hold on;
        bar([2],beta_ant(3),'FaceColor',[0.16,0.67,0.49]); hold on;
        hold on;
        
        errorbar(beta_ant(1,[2 3]), e_ant(1,[2 3]), 'black','LineStyle','none');
        set(hResAx,'Xcolor',[0 0 0])
        set(hResAx,'XLim',[0 3]); set(hResAx,'XTick',[1:1:2]);
        set(hResAx,'XTickLabel',{'Biperiden', 'Placebo',''}, ...
            'FontSize', 8, 'FontName','Helvetica')
        set(hResAx,'Ycolor',[0 0 0])
        ylabel('beta estimates','FontSize', 12, 'FontName','Helvetica')
        hold on;
        Con_title = 'biperiden < placebo';
        t=title({Con_title; ' '}, 'FontWeight', 'bold', 'FontName','Arial');
        
    end
end