function ADPRSI_antfMRI_ssl_secondlevel_DACh_cov_COMT( m , r )
% ADPRSI_antfMRI_ssl_secondlevel_DACh_cov_COMT: runs the second-level GLM 
% across the anatomical mask:
% one factor with three cells: drug (1 = amisulpride; 2 = biperiden; 3 =
% placebo)

% 1. factor drug: cells: 1 = amisulpride; 2 = biperiden; 3 = placebo
% 2. additional covariate COMT: 1 = MET/MET (A:A); 2 = VAL/MET (A:G); 3 = VAL/VAL (G:G);

%   IN:     m                   - first level model
%                                   1 = epsilon (GLM1)
%                                   2 = delta   (GLM2)
%           r                   - regressor, i.e. computational state
%                                 if m == 1
%                                   1 = epsilon2
%                                   2 = epsilon3
%                                   3 = epsilon_ch
%                                   4 = basic regressor
%                                 if m == 2
%                                   1 = da1
%                                   2 = da2
%                                   3 = da_ch
%                                   4 = pw2
%                                   5 = pw3
%                                   6 = basic regressor
%   OUT:    -

% 16-01-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

% Set paths
workdir = ADPRSI_antfMRI_ssl_set_workdir;
options = ADPRSI_antfMRI_ssl_set_analysis_options;
[ details, paths ] = ADPRSI_antfMRI_ssl_subjects( 1 );

% Start diary
diary(paths.logfile_group_ssl);

% Initialize
disp(['running second level COMT analysis for GLM: ', num2str(m),' (regressor: ',num2str(r),')']);
spm('defaults','fmri');
spm_jobman('initcfg');

Bin_folder = {'fixom_bin3_spm12', 'fixom_bin5_spm12'}; 
Bin_folder_group = {'fixom_bin3_comt_spm12', 'fixom_bin5_comt_spm12'};
mkdir([paths.res_group_ssl_glm,'/sslbin/', [Bin_folder_group{m},'_fact/mask/']]);
if m == 1 % GLM1
    PM = {'epsilon2','epsilon3', 'epsilon_ch', 'SSL'};
    con_target = {'con_0009','con_0011','con_0015','con_0007'};
elseif m == 2 % GLM2
    PM = {'da1','da2', 'wCPE', 'pw2', 'pw3', 'SSL'};
    con_target = {'con_0013','con_0015','con_0017','con_0019','con_0023','con_0011'};
end

% load data to determine subjects with <65% CR, >15% missed trials
% and bad model fits:
check_out = load([paths.quality_ssl_behav,'/Check_antfMRI_ssl_behaviour.mat']);

% HGF_1_fixom_v1 was the winning model:
[i_fixom, j_fixom] = find(check_out.model_fixom_v1.out_diff_sa3 ==-1);
[i_fixom2, j_fixom2] = find(check_out.model_fixom_v1.out_diff_mu3 ==-1);
[i_check, j_check] = find(check_out.behav.out_performance_CR==-1);
[i_check2, j_check2] = find(check_out.behav.out_performance_irr==-1);

% load descriptives:
ant_ssl = load([paths.res_group_ssl_descr,'/ADPRSI_antfMRI_ssl.mat']);
% find subjects without comt results
comt = ant_ssl.snp.comt_code;
[comt_i_out,comt_j_out]=find(ant_ssl.snp.comt_code==-99); 
% find all subjects removed from final analysis
out = [i_fixom; i_fixom2; i_check; i_check2; comt_i_out];
out_final = unique(out);
cd([paths.res_group_ssl_glm,'/sslbin/', [Bin_folder_group{m},'_fact'],'/mask/']);
disp(['subjects out: ', num2str(out_final')])
save('antfMRI_ssl_group_comt.mat','out_final');

% determine covariates and remove participants that have been excluded from 
% the final group analyses
cov_kss = ant_ssl.behav.KSS;
cov_kss(out_final) = [];
cov_weight = ant_ssl.weight;
cov_weight(out_final) = [];
comt(out_final) = [];
drug = ant_ssl.group;
drug(out_final) = [];
subj_name = ant_ssl.subj;
subj_name(out_final) = [];

cov_kss_pla = cov_kss(drug == 0);
cov_kss_ami = cov_kss(drug == 1);
cov_kss_bip = cov_kss(drug == 2);
cov_weight_pla = cov_weight(drug == 0);
cov_weight_ami = cov_weight(drug == 1);
cov_weight_bip = cov_weight(drug == 2);
cov_comt_pla = comt(drug == 0);
cov_comt_ami = comt(drug == 1);
cov_comt_bip = comt(drug == 2);

% order based on pharmacological manipulation
cov_kss = [cov_kss_ami; cov_kss_bip; cov_kss_pla];
cov_weight = [cov_weight_ami; cov_weight_bip; cov_weight_pla];
cov_comt = [cov_comt_ami; cov_comt_bip; cov_comt_pla];

%% subject names
subj_ami = subj_name(drug == 1); %amisulpride
subj_bip = subj_name(drug == 2); %biperiden
subj_pla = subj_name(drug == 0); %placebo

for i = 1:numel(subj_ami)
    subjcon_ami{i,1} = ([options.resultsdir, '/results_first_ssl/',subj_ami{i,1},'/glm/ssl/',Bin_folder{m},'/',con_target{r},'.nii']); %amisulpride
    subjpaths_ami{i,1} = ([options.resultsdir, '/results_first_ssl/',subj_ami{i,1},'/glm/ssl/',Bin_folder{m}]); %amisulpride
end

for i = 1:numel(subj_bip)
    subjcon_bip{i,1} = ([options.resultsdir, '/results_first_ssl/',subj_bip{i,1},'/glm/ssl/',Bin_folder{m},'/',con_target{r},'.nii']); %biperiden
    subjpaths_bip{i,1} = ([options.resultsdir, '/results_first_ssl/',subj_bip{i,1},'/glm/ssl/',Bin_folder{m}]); %biperiden
end

for i = 1:numel(subj_pla)
    subjcon_pla{i,1} = ([options.resultsdir, '/results_first_ssl/',subj_pla{i,1},'/glm/ssl/',Bin_folder{m},'/',con_target{r},'.nii']); %placebo
    subjpaths_pla{i,1} = ([options.resultsdir, '/results_first_ssl/',subj_pla{i,1},'/glm/ssl/',Bin_folder{m}]); %placebo
end

%%
disp(['This is COMT single_glm_group_',PM{r},' ',con_target{r}, '   */*   ', Bin_folder_group{m}]);

% ---------------------------------------------------------------------
% NOTE: when rerunning a GLM analysis, make sure to delete all
% old files
cd ([paths.res_group_ssl_glm,'/sslbin/',Bin_folder_group{m},'_fact/mask/']);
unix(['rm -r ', [PM{r},'_basic_DACh']]); %
mkdir ([PM{r},'_basic_DACh']);
save(['antfMRI_ssl_subj_mask_COMT_',PM{r},'.mat'],'subjpaths_ami', 'subjpaths_bip','subjpaths_pla');

%-----------------------------------------------------------------------
% Job configuration
%-----------------------------------------------------------------------
   
matlabbatch{1}.spm.stats.factorial_design.dir = {[paths.res_group_ssl_glm,'/sslbin/', [Bin_folder_group{m},'_fact'],'/mask/',PM{r},'_basic_DACh']}; %
matlabbatch{1}.spm.stats.factorial_design.des.fd.fact(1).name = 'Group';
matlabbatch{1}.spm.stats.factorial_design.des.fd.fact(1).levels = 3;
matlabbatch{1}.spm.stats.factorial_design.des.fd.fact(1).dept = 0;
matlabbatch{1}.spm.stats.factorial_design.des.fd.fact(1).variance = 1;
matlabbatch{1}.spm.stats.factorial_design.des.fd.fact(1).gmsca = 0;
matlabbatch{1}.spm.stats.factorial_design.des.fd.fact(1).ancova = 0;
%%
matlabbatch{1}.spm.stats.factorial_design.des.fd.icell(1).levels = [1];
matlabbatch{1}.spm.stats.factorial_design.des.fd.icell(1).scans = subjcon_ami;

matlabbatch{1}.spm.stats.factorial_design.des.fd.icell(2).levels = [2];
matlabbatch{1}.spm.stats.factorial_design.des.fd.icell(2).scans = subjcon_bip;

matlabbatch{1}.spm.stats.factorial_design.des.fd.icell(3).levels = [3];
matlabbatch{1}.spm.stats.factorial_design.des.fd.icell(3).scans = subjcon_pla;

matlabbatch{1}.spm.stats.factorial_design.des.fd.contrasts = 1;
%%
matlabbatch{1}.spm.stats.factorial_design.cov(1).c = cov_weight;
matlabbatch{1}.spm.stats.factorial_design.cov(1).cname = 'Weight';
matlabbatch{1}.spm.stats.factorial_design.cov(1).iCFI = 2; %interaction
matlabbatch{1}.spm.stats.factorial_design.cov(1).iCC = 2;  %within group mean centering 
matlabbatch{1}.spm.stats.factorial_design.cov(2).c = cov_kss;
matlabbatch{1}.spm.stats.factorial_design.cov(2).cname = 'KSS';
matlabbatch{1}.spm.stats.factorial_design.cov(2).iCFI = 1; %no interaction
matlabbatch{1}.spm.stats.factorial_design.cov(2).iCC = 2;  %within group mean centering
matlabbatch{1}.spm.stats.factorial_design.cov(3).c = cov_comt;
matlabbatch{1}.spm.stats.factorial_design.cov(3).cname = 'COMT';
matlabbatch{1}.spm.stats.factorial_design.cov(3).iCFI = 2; %interaction with factor 1
matlabbatch{1}.spm.stats.factorial_design.cov(3).iCC =  2; %within group mean centering
matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.em = {[workdir.code,'\steps\mask\DAch_anat_red.nii']};
matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
matlabbatch{2}.spm.stats.fmri_est.spmmat(1) = cfg_dep('Factorial design specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{2}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{2}.spm.stats.fmri_est.method.Classical = 1;
matlabbatch{3}.spm.stats.con.spmmat(1) = cfg_dep('Model estimation: SPM.mat File', substruct('.','val', '{}',{2}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{3}.spm.stats.con.consess{1}.tcon.name = ['COMT: ', PM{r},'_amisulpride_pos'];
matlabbatch{3}.spm.stats.con.consess{1}.tcon.convec = 1;
matlabbatch{3}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{2}.tcon.name = ['COMT: ', PM{r},'_biperiden_pos'];
matlabbatch{3}.spm.stats.con.consess{2}.tcon.convec = [0 1];
matlabbatch{3}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{3}.tcon.name = ['COMT: ', PM{r},'_placebo_pos'];
matlabbatch{3}.spm.stats.con.consess{3}.tcon.convec = [0 0 1];
matlabbatch{3}.spm.stats.con.consess{3}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{4}.tcon.name = ['COMT: ', PM{r},'_amisulpride_neg'];
matlabbatch{3}.spm.stats.con.consess{4}.tcon.convec = [-1];
matlabbatch{3}.spm.stats.con.consess{4}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{5}.tcon.name = ['COMT: ', PM{r},'_biperiden_neg'];
matlabbatch{3}.spm.stats.con.consess{5}.tcon.convec = [0 -1];
matlabbatch{3}.spm.stats.con.consess{5}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{6}.tcon.name = ['COMT: ', PM{r},'_placebo_neg'];
matlabbatch{3}.spm.stats.con.consess{6}.tcon.convec = [0 0 -1];
matlabbatch{3}.spm.stats.con.consess{6}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{7}.tcon.name = ['COMT: ', PM{r},'_amisulpride>',PM{r},'_biperiden'];
matlabbatch{3}.spm.stats.con.consess{7}.tcon.convec = [1 -1];
matlabbatch{3}.spm.stats.con.consess{7}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{8}.tcon.name = ['COMT: ', PM{r},'_amisulpride<',PM{r},'_biperiden'];
matlabbatch{3}.spm.stats.con.consess{8}.tcon.convec = [-1 1];
matlabbatch{3}.spm.stats.con.consess{8}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{9}.tcon.name = ['COMT: ', PM{r},'_amisulpride>',PM{r},'_placebo'];
matlabbatch{3}.spm.stats.con.consess{9}.tcon.convec = [1 0 -1];
matlabbatch{3}.spm.stats.con.consess{9}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{10}.tcon.name = ['COMT: ', PM{r},'_amisulpride<',PM{r},'_placebo'];
matlabbatch{3}.spm.stats.con.consess{10}.tcon.convec = [-1 0 1];
matlabbatch{3}.spm.stats.con.consess{10}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{11}.tcon.name = ['COMT: ', PM{r},'_biperiden>',PM{r},'_placebo'];
matlabbatch{3}.spm.stats.con.consess{11}.tcon.convec = [0 1 -1];
matlabbatch{3}.spm.stats.con.consess{11}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{12}.tcon.name = ['COMT: ', PM{r},'_biperiden<',PM{r},'_placebo'];
matlabbatch{3}.spm.stats.con.consess{12}.tcon.convec = [0 -1 1];
matlabbatch{3}.spm.stats.con.consess{12}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{13}.tcon.name = ['COMT: ', PM{r},'_pos'];
matlabbatch{3}.spm.stats.con.consess{13}.tcon.convec = [1 1 1];
matlabbatch{3}.spm.stats.con.consess{13}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{14}.tcon.name = ['COMT: ', PM{r},'_neg'];
matlabbatch{3}.spm.stats.con.consess{14}.tcon.convec = [-1 -1 -1];
matlabbatch{3}.spm.stats.con.consess{14}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{15}.tcon.name = [PM{r},'_weight_amisulpride >',PM{r},'_weight_biperiden'];
matlabbatch{3}.spm.stats.con.consess{15}.tcon.convec = [zeros(1,3) 1 -1];
matlabbatch{3}.spm.stats.con.consess{15}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{16}.tcon.name = [PM{r},'_weight_amisulpride <',PM{r},'_weight_biperiden'];
matlabbatch{3}.spm.stats.con.consess{16}.tcon.convec = [zeros(1,3) -1 1];
matlabbatch{3}.spm.stats.con.consess{16}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{17}.tcon.name = [PM{r},'_weight_amisulpride >',PM{r},'_weight_placebo'];
matlabbatch{3}.spm.stats.con.consess{17}.tcon.convec = [zeros(1,3) 1 0 -1];
matlabbatch{3}.spm.stats.con.consess{17}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{18}.tcon.name = [PM{r},'_weight_amisulpride <',PM{r},'_weight_placebo'];
matlabbatch{3}.spm.stats.con.consess{18}.tcon.convec = [zeros(1,3) -1 0 1];
matlabbatch{3}.spm.stats.con.consess{18}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{19}.tcon.name = [PM{r},'_weight_biperiden >',PM{r},'_weight_placebo'];
matlabbatch{3}.spm.stats.con.consess{19}.tcon.convec = [zeros(1,3) 0 1 -1];
matlabbatch{3}.spm.stats.con.consess{19}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{20}.tcon.name = [PM{r},'_weight_biperiden <',PM{r},'_weight_placebo'];
matlabbatch{3}.spm.stats.con.consess{20}.tcon.convec = [zeros(1,3) 0 -1 1];
matlabbatch{3}.spm.stats.con.consess{20}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{21}.fcon.name = 'KSS';
matlabbatch{3}.spm.stats.con.consess{21}.fcon.convec = [zeros(1,6) 1];
matlabbatch{3}.spm.stats.con.consess{21}.fcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{22}.tcon.name = [PM{r},'_COMT_amisulpride > ', PM{r},'_COMT_placebo'];
matlabbatch{3}.spm.stats.con.consess{22}.tcon.convec = [zeros(1,7) 1 0 -1];
matlabbatch{3}.spm.stats.con.consess{22}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{23}.tcon.name = [PM{r},'_COMT_amisulpride < ', PM{r},'_COMT_placebo'];
matlabbatch{3}.spm.stats.con.consess{23}.tcon.convec = [zeros(1,7) -1 0 1];
matlabbatch{3}.spm.stats.con.consess{23}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{24}.tcon.name = [PM{r},'_COMT_amisulpride > ', PM{r},'_COMT_biperiden'];
matlabbatch{3}.spm.stats.con.consess{24}.tcon.convec = [zeros(1,7) 1 -1];
matlabbatch{3}.spm.stats.con.consess{24}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{25}.tcon.name = [PM{r},'_COMT_amisulpride < ', PM{r},'_COMT_biperiden'];
matlabbatch{3}.spm.stats.con.consess{25}.tcon.convec = [zeros(1,7) -1 1];
matlabbatch{3}.spm.stats.con.consess{25}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{26}.tcon.name = [PM{r},'_COMT_biperiden > ', PM{r},'_COMT_placebo'];
matlabbatch{3}.spm.stats.con.consess{26}.tcon.convec = [zeros(1,7) 0 1 -1];
matlabbatch{3}.spm.stats.con.consess{26}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{27}.tcon.name = [PM{r},'_COMT_biperiden < ', PM{r},'_COMT_placebo'];
matlabbatch{3}.spm.stats.con.consess{27}.tcon.convec = [zeros(1,7) 0 -1 1];
matlabbatch{3}.spm.stats.con.consess{27}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.delete = 1;

% ---------------------------------------------------------------------
% Execute matlabbatch
spm_jobman('run',matlabbatch);

end