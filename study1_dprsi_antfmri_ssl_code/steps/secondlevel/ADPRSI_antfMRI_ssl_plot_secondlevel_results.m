function [coord_vector, handle] = ADPRSI_antfMRI_ssl_plot_secondlevel_results(antfMRI_ssl_results,v,j,m,r,x_text,verbose)
% ADPRSI_antfMRI_ssl_plot_secondlevel_results: visualise second level
% results.
%
% 16-01-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================
%% Initialize
sep1 = '===============================================================================================';
sep2 = '-----------------------------------------------------------------------------------------------';
sep3 = '***********************************************************************************************';

% define cluster-size
cluster_size=verbose(1);

cd(antfMRI_ssl_results.paths.dirResults); % where to store plots

if strcmp(antfMRI_ssl_results.mode,'cluster_level_sign')==1 || strcmp(antfMRI_ssl_results.mode,'cluster_level_notsign')==1
    disp('plotting cluster-level results')
    job.spm.stats.results.spmmat = {antfMRI_ssl_results.paths.dirGlm};
    job.spm.stats.results.conspec.titlestr = '';
    job.spm.stats.results.conspec.contrasts = antfMRI_ssl_results.contrast_index;
    job.spm.stats.results.conspec.threshdesc = 'none';
    job.spm.stats.results.conspec.thresh = 0.001;
    job.spm.stats.results.conspec.extent = cluster_size;
    job.spm.stats.results.conspec.conjunction = 1;
    job.spm.stats.results.conspec.mask.none = 1;
    job.spm.stats.results.units = 1;
    job.spm.stats.results.print = 0;
    job.spm.stats.results.write.none = 1;
    
    spm_jobman('run',{job});
else
    disp('plotting peak-level results')
    job.spm.stats.results.spmmat = {antfMRI_ssl_results.paths.dirGlm};
    job.spm.stats.results.conspec.titlestr = '';
    job.spm.stats.results.conspec.contrasts = antfMRI_ssl_results.contrast_index;
    job.spm.stats.results.conspec.threshdesc = 'FWE';
    
    if strcmp(antfMRI_ssl_results.mode,'peak_level_sign')==1
        job.spm.stats.results.conspec.thresh = 0.05;
    else
        job.spm.stats.results.conspec.thresh = 0.1;
    end
    job.spm.stats.results.conspec.extent = 0;
    job.spm.stats.results.conspec.conjunction = 1;
    job.spm.stats.results.conspec.mask.none = 1;
    job.spm.stats.results.units = 1;
    job.spm.stats.results.print = 0;
    job.spm.stats.results.write.none = 1;
    
    spm_jobman('run',{job});
end

SPM = evalin('base','SPM');
xSPM = evalin('base','xSPM');
hReg = evalin('base','hReg');
TabDat = evalin('base','TabDat');

% plot significant results overlayed on structural image
spm_check_registration(antfMRI_ssl_results.paths.fileStruct);
spm_orthviews('AddBlobs',1,xSPM.XYZ,xSPM.Z,xSPM.M);
spm_orthviews('Reposition',antfMRI_ssl_results.ResData{v(j),12}); % reposition to relevant coordinate {mm}
spm_orthviews('Xhairs','off');
if any(antfMRI_ssl_results.contrast_index==antfMRI_ssl_results.NoZoom) == 1
    disp('No Zoom')
    disp(sep2);
else
    spm_orthviews('Zoom',60);
    spm_orthviews('Redraw');
end
antfMRI_ssl_results.cluster.plot(j).Con_title = xSPM.title;
wCon_title=textwrap({antfMRI_ssl_results.cluster.plot(j).Con_title},30); % introduce line break if title is too long for the figure
handle = title([wCon_title; ' '; antfMRI_ssl_results.mode], 'FontSize', 10);

set(handle,'Position',[14,0]);
antfMRI_ssl_results.cluster.plot(j).coord = antfMRI_ssl_results.ResData{v(j),12};
p1 = num2str(antfMRI_ssl_results.cluster.plot(j).coord(1,1));
p2 = num2str(antfMRI_ssl_results.cluster.plot(j).coord(2,1));
p3 = num2str(antfMRI_ssl_results.cluster.plot(j).coord(3,1));
coord_vector = antfMRI_ssl_results.cluster.plot(j).coord';
disp(coord_vector);
disp(sep1);
coord = [['x = ',p1] ['; y = ',p2] ['; z = ',p3]];
s = xlabel(coord, 'FontSize', 12);
set(s,'Position',[14, 3])
x2_text=textwrap(x_text,70);
text(-30,-0.8,x2_text)

return