function ADPRSI_antfMRI_ssl_behaviour_summary
% ADPRSI_antfMRI_ssl_behaviour_summary: combines the behavioural 
% variables (RT, CR) and model parameters of all subjects 
% into one structure.
%
%   IN:     -

%   OUT:    -

% 10-01-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

% Set paths
workdir = ADPRSI_antfMRI_ssl_set_workdir;
options = ADPRSI_antfMRI_ssl_set_analysis_options;
[ details, paths ] = ADPRSI_antfMRI_ssl_subjects( 1 );

% Start diary
diary(paths.logfile_behav_summary_ssl);

% Initialize
disp(['running hgf summary']);

% define models
maskModel = {'/HGF_fixom_v1',  '/HGF_2l_v1', '/RW_v1'};

% load data
Check_antfMRI_ssl_behaviourData = load([paths.quality_ssl_behav,'/Check_antfMRI_ssl_behaviour.mat']);

%% loop through models
for m = 1:length(maskModel)
    disp(' ');
    disp(['This is model (',mat2str(maskModel{m}),')']);
    mkdir([paths.res_group_ssl_behaviour,maskModel{m}]);
    
    % Go through subjects
    for id = 1:numel(options.subjectIDs)
        [ details, paths ] = ADPRSI_antfMRI_ssl_subjects( id );
        disp(['running "summary of behavioural data" for subj: ', details.subjname,' (',num2str(id),')']);
        disp ('**************************************************************')
        
        %% skip stats for subjects for which model did not run
        if strcmp('/HGF_fixom_v1',maskModel{m})==1
            if Check_antfMRI_ssl_behaviourData.out_matrix(id,4) == -1 || ...
                    Check_antfMRI_ssl_behaviourData.out_matrix(id,4) == 0 % HGF_fixom_v1 did not run
                continue
            end
        elseif strcmp('/HGF_2l_v1',maskModel{m})==1
            if Check_antfMRI_ssl_behaviourData.out_matrix(id,6) == -1 || ...
                    Check_antfMRI_ssl_behaviourData.out_matrix(id,6) == 0 % HGF_2l_v1 did not run
                continue
            end
        elseif strcmp('/RW_v1',maskModel{m})==1
            if Check_antfMRI_ssl_behaviourData.out_matrix(id,7) == -1 || ...
                    Check_antfMRI_ssl_behaviourData.out_matrix(id,7) == 0 % RW_v1 did not run
                continue
            end
        end
        
        behav = load([paths.res_behaviour_ssl,details.behaviour_ssl]);
        
        if exist([paths.res_behaviour_ssl,maskModel{m},'/ssl_traj.jpg']) == 2
            HGFssl = load([paths.res_behaviour_ssl,maskModel{m},'/SSL.VS.mat']);
        else
            ADPRSI_antfMRI_ssl_hgfv1( id , m )
            HGFssl = load([paths.res_behaviour_ssl,maskModel{m},'/SSL.VS.mat']);
        end
        
        if strcmp('/HGF_fixom_v1',maskModel{m})==1 || strcmp('/HGF_2l_v1',maskModel{m})==1

            % estimated parameters
            ParSSL2.param(id).mat = HGFssl.VS.est;
            ParSSL.param(id).ka = ParSSL2.param(id).mat.p_prc.ka;
            ParSSL.param(id).om = ParSSL2.param(id).mat.p_prc.om;
            ParSSL.param(id).th = ParSSL2.param(id).mat.p_prc.th;
            ParSSL.param(id).ze = ParSSL2.param(id).mat.p_obs.ze;
            ParSSL.param(id).F = ParSSL2.param(id).mat.F; % Free energy
            
            cd ([paths.res_group_ssl_behaviour,maskModel{m}]);
            save('ParSSL.mat', '-struct','ParSSL');
            
            % write out number of correct trials and RT per block
            CurrentBlockIndex = 0;
            for nr = 1:length(behav.param.block_length)
                CurrentBlockIndex = CurrentBlockIndex+1;
                ParSSL.nr_corr.trials = numel(find(behav.block(CurrentBlockIndex).exp_data(:,5)==1));
                ParSSL.nr_corr.block(CurrentBlockIndex,1) = ParSSL.nr_corr.trials(1,1);
                irr = [];
                irr_late = [];
                irr_early = [];
                for l = 1:length(behav.block(CurrentBlockIndex).exp_data(:,3))
                    if behav.block(CurrentBlockIndex).exp_data(l,4) == -99
                        irr = [irr, l];
                    elseif behav.block(CurrentBlockIndex).exp_data(l,3) > 1600  % Duration cue presentation: 300; Duration interstimulus interval: 1200; plus 100ms margin, 
                                                                                % as the reaction to the outcome presentation would not be as fast.
                        irr_late = [irr_late, l];
                    elseif behav.block(CurrentBlockIndex).exp_data(l,3) < 0
                        if behav.block(CurrentBlockIndex).exp_data(l,4) ~=-99
                            irr_early = [irr_early, l];
                        end
                    end
                end
                
                irr_all = [irr irr_late irr_early]; %all irregular trials within block
                irr_all = unique(irr_all);
                rtblock = behav.block(CurrentBlockIndex).exp_data(:,3);
                rtblock(irr_all) = [];
                rtmean = mean(rtblock);
                % mean RT per block:
                ParSSL.stat.blockrtmean(CurrentBlockIndex,1) = rtmean(1,1);
            end
            
            % calculate percent correct responses per block:
            % a=[40;32;24;40;32;24;32;24;40;32]; % number of trials per block
            ParSSL.nr_trials = [40;32;24;40;32;24;32;24;40;32];
            % b=[0.9;0.9;0.5;0.7;0.7;0.9;0.7;0.7;0.5;0.9]; % probability for each block
            % results in the following maximal number of corrects possible per block:
            ParSSL.prob_corr = [36;29;12;28;22;22;22;17;20;29];
            ParSSL.block_percent_corr= ParSSL.nr_corr.block(:,1)./ParSSL.prob_corr(:,1);
            % total percent correct responses:
            ParSSL.sum_nr_corr = sum(ParSSL.nr_corr.block(:,1));
            
            % mean RT over all blocks
            ParSSL.stat.rt_mean = Check_antfMRI_ssl_behaviourData.behav.RT.mean(id,1);

            cd ([paths.res_group_ssl_behaviour,maskModel{m}]);
            save('ParSSL.mat', '-struct','ParSSL');
            
            %% laterality - bias towards pressing more often right or left?
            RespSSL.indexR = 0;
            RespSSL.indexL = 0;
            for trial = 1:length(behav.alldata(:,3))
                if behav.alldata(trial,4) == 3
                    RespSSL.indexR = RespSSL.indexR + 1;
                else
                    RespSSL.indexL = RespSSL.indexL + 1;
                    
                end
            end
            RespSSL.indexRp = RespSSL.indexR/length(behav.alldata(:,3));
            RespSSL.indexLp = RespSSL.indexL/length(behav.alldata(:,3));
            cd (paths.res_group_ssl_behaviour);
            save('RespSSL.mat', '-struct','RespSSL');
        end
        % % %
        %% Structure stats for all
        if strcmp('/HGF_fixom_v1',maskModel{m})==1 || strcmp('/HGF_2l_v1',maskModel{m})==1
            cd ([paths.res_group_ssl_behaviour,maskModel{m}]);
            
            A_SSL_VS2.bp =  ParSSL2.param(id).mat;
            A_SSL_VS.ka(id,1)  = [A_SSL_VS2.bp.p_prc.ka];
            A_SSL_VS.om(id,1) = [A_SSL_VS2.bp.p_prc.om];
            A_SSL_VS.th(id,1) = [A_SSL_VS2.bp.p_prc.th];
            A_SSL_VS.ze(id,1) = [A_SSL_VS2.bp.p_obs.ze];
            
            A_SSL_VS.mu2_0(id,1) = [A_SSL_VS2.bp.p_prc.mu2_0];
            A_SSL_VS.sa2_0(id,1) = [A_SSL_VS2.bp.p_prc.sa2_0];
            A_SSL_VS.mu3_0(id,1) = [A_SSL_VS2.bp.p_prc.mu3_0];
            A_SSL_VS.sa3_0(id,1) = [A_SSL_VS2.bp.p_prc.sa3_0];
            A_SSL_VS.F(id,1) = [A_SSL_VS2.bp.F];
            
            A_SSL_VS.param = [A_SSL_VS.ka A_SSL_VS.om A_SSL_VS.th A_SSL_VS.mu2_0 A_SSL_VS.sa2_0 A_SSL_VS.mu3_0 A_SSL_VS.sa3_0 A_SSL_VS.ze A_SSL_VS.F];
            
            A_SSL_VS.stat.mean(id,1) = [ParSSL.stat.rt_mean];
            A_SSL_VS.stat.rt(:,id) = behav.alldata(:,3);
            A_SSL_VS.stat.irr_no(:,id) = Check_antfMRI_ssl_behaviourData.behav.Missed.irr_no(id,1);
            A_SSL_VS.stat.irr_late(:,id) = Check_antfMRI_ssl_behaviourData.behav.Missed.irr_late(id,1);
            A_SSL_VS.stat.irr_early(:,id) = Check_antfMRI_ssl_behaviourData.behav.Missed.irr_early(id,1);
            A_SSL_VS.irr = [A_SSL_VS.stat.irr_no; A_SSL_VS.stat.irr_late; A_SSL_VS.stat.irr_early];
            A_SSL_VS.block_percent_corr(:,id) = ParSSL.block_percent_corr(:,1);
            A_SSL_VS.sum_percent_corr(:,id) = Check_antfMRI_ssl_behaviourData.behav.CR.percent(id,1);
            save ('A_SSL_VS.mat', '-struct','A_SSL_VS');
            
            cd (paths.res_group_ssl_behaviour);
            
            A_RespSSL.RF(id,1) = RespSSL.indexR;
            A_RespSSL.RH(id,1) = RespSSL.indexL;
            A_RespSSL.RFp(id,1) = RespSSL.indexRp;
            A_RespSSL.RHp(id,1) = RespSSL.indexLp;
            save ('A_RespSSL.mat','-struct','A_RespSSL');
        end
        
        
        if strcmp('/RW_v1',maskModel{m})==1
            ParSSL2.param(id).mat = HGFssl.VS.est;
            ParSSL.param(id).v_0 = ParSSL2.param(id).mat.p_prc.v_0;
            ParSSL.param(id).al = ParSSL2.param(id).mat.p_prc.al;
            ParSSL.param(id).ze = ParSSL2.param(id).mat.p_obs.ze;
            ParSSL.param(id).F = ParSSL2.param(id).mat.F;
            
            cd ([paths.res_group_ssl_behaviour,maskModel{m}]);
            save('ParSSL.mat', '-struct','ParSSL');
            
            A_SSL_VS2.bp      =  ParSSL2.param(id).mat;
            A_SSL_VS.v_0(id,1)= [A_SSL_VS2.bp.p_prc.v_0];
            A_SSL_VS.al(id,1) = [A_SSL_VS2.bp.p_prc.al];
            A_SSL_VS.ze(id,1) = [A_SSL_VS2.bp.p_obs.ze];
            A_SSL_VS.F(id,1)  = [A_SSL_VS2.bp.F];

            cd ([paths.res_group_ssl_behaviour,maskModel{m}]);
            save ('A_SSL_VS.mat', '-struct','A_SSL_VS');
        end
    end
end
end

