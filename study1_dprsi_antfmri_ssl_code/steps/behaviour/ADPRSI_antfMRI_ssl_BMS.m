function ADPRSI_antfMRI_ssl_BMS
% ADPRSI_antfMRI_ssl_BMS: runs BMS for the estimated models, excluding
% outliers: 
% - participants with more than 15% missed trials or less than 65%
% correct responses, 
% - participants for which model estimation did not work.

%   IN:     -

%   OUT:    -

% 14-01-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% ========================================================================

workdir = ADPRSI_antfMRI_ssl_set_workdir;
options = ADPRSI_antfMRI_ssl_set_analysis_options;
[ details, paths ] = ADPRSI_antfMRI_ssl_subjects( 1 );

% Start diary
diary(paths.logfile_bms_ssl);

% Initialize
disp(['running BMS ssl']);

% load data
Check_antfMRI_ssl_behaviourData = load([paths.quality_ssl_behav,'/Check_antfMRI_ssl_behaviour.mat']);
check_behav     = load([paths.quality_ssl_behav,'/check_behav_table.mat']);
ADPRSI_ant_descr = load([paths.res_group_ssl_descr,'/ADPRSI_antfMRI_ssl.mat']);

% entries in check_behav_table:
% 1='Index';2='out_performance_CR';3='out_performance_irr';4='fixom_v1_mu3';5='fixom_v1_sa3';
% 6='twol_v1_mu2';7='rw_v1_al';

ADPRSI_ant_ssl.ID = {options.subjectIDs}';

%% load HGF_fixom_v1 results:
hgf_fixom = load (fullfile([paths.res_group_ssl_behaviour,'/HGF_fixom_v1/A_SSL_VS.mat']));

%% behavioral data

% subjects out due to irregular behavior
% percent correct responses:
ADPRSI_ant_ssl.behav.CR = Check_antfMRI_ssl_behaviourData.behav.CR;
[iCR,jCR]=find(check_behav.check_behav_table(:,2)==-1);
ADPRSI_ant_ssl.behav.outCR = iCR;

% irregular trials
[imissed,jmissed]=find(check_behav.check_behav_table(:,3)==-1);
ADPRSI_ant_ssl.behav.IrrSubj = imissed;

% hgf_fixom_v1
[ihgf1,jhgf1]=find(check_behav.check_behav_table(:,4)==-1|check_behav.check_behav_table(:,5)==-1);
ADPRSI_ant_ssl.model.out_hgf_fixom_v1 = ihgf1;

% hgf_2l_v1
[ihgf2,jhgf2]=find(check_behav.check_behav_table(:,6)==-1);
ADPRSI_ant_ssl.model.out_hgf_2l_v1 = ihgf2;

% rw_v1
[irw,jrw]=find(check_behav.check_behav_table(:,7)==-1);
ADPRSI_ant_ssl.model.out_hgf_rw_v1 = irw;

ADPRSI_ant_ssl.out_all = [ADPRSI_ant_ssl.behav.outCR; ADPRSI_ant_ssl.behav.IrrSubj; ...
    ADPRSI_ant_ssl.model.out_hgf_fixom_v1; ADPRSI_ant_ssl.model.out_hgf_2l_v1; ...
    ADPRSI_ant_ssl.model.out_hgf_rw_v1];

ADPRSI_ant_ssl.out_BMS = unique(ADPRSI_ant_ssl.out_all);

%% HGF_fixom_v1
ADPRSI_ant_ssl.hgfv1_0.HGF_1_fixom.ka = hgf_fixom.ka;
ADPRSI_ant_ssl.hgfv1_0.HGF_1_fixom.om = hgf_fixom.om;
ADPRSI_ant_ssl.hgfv1_0.HGF_1_fixom.th = hgf_fixom.th;
ADPRSI_ant_ssl.hgfv1_0.HGF_1_fixom.ze = hgf_fixom.ze;
ADPRSI_ant_ssl.hgfv1_0.HGF_1_fixom.F = hgf_fixom.F;

ADPRSI_ant_ssl.hgfv1_0_BMS.HGF_1_fixom.F = ADPRSI_ant_ssl.hgfv1_0.HGF_1_fixom.F;
ADPRSI_ant_ssl.hgfv1_0_BMS.HGF_1_fixom.F(ADPRSI_ant_ssl.out_BMS) = [];


%% load HGF_2l_v1 results:
HGF_2 = load (fullfile([paths.res_group_ssl_behaviour,'/HGF_2l_v1/A_SSL_VS.mat']));
ADPRSI_ant_ssl.hgfv1_0.HGF_2l_v1.ka = HGF_2.ka;
ADPRSI_ant_ssl.hgfv1_0.HGF_2l_v1.om = HGF_2.om;
ADPRSI_ant_ssl.hgfv1_0.HGF_2l_v1.th = HGF_2.th;
ADPRSI_ant_ssl.hgfv1_0.HGF_2l_v1.ze = HGF_2.ze;
ADPRSI_ant_ssl.hgfv1_0.HGF_2l_v1.F = HGF_2.F;

ADPRSI_ant_ssl.hgfv1_0_BMS.HGF_2l_v1.F = ADPRSI_ant_ssl.hgfv1_0.HGF_2l_v1.F;
ADPRSI_ant_ssl.hgfv1_0_BMS.HGF_2l_v1.F(ADPRSI_ant_ssl.out_BMS) = [];


%% load RW_v1 results
RW_1_hgfv1_0 = load (fullfile([paths.res_group_ssl_behaviour,'/RW_v1/A_SSL_VS.mat']));
ADPRSI_ant_ssl.hgfv1_0.RW_1.al = RW_1_hgfv1_0.al;
ADPRSI_ant_ssl.hgfv1_0.RW_1.v_0 = RW_1_hgfv1_0.v_0;
ADPRSI_ant_ssl.hgfv1_0.RW_1.ze = RW_1_hgfv1_0.ze;
ADPRSI_ant_ssl.hgfv1_0.RW_1.F = RW_1_hgfv1_0.F;

ADPRSI_ant_ssl.hgfv1_0_BMS.RW_1.F = ADPRSI_ant_ssl.hgfv1_0.RW_1.F;
ADPRSI_ant_ssl.hgfv1_0_BMS.RW_1.F(ADPRSI_ant_ssl.out_BMS) = [];

cd (fullfile(paths.res_hgf_ssl_bms));
unix(['rm ', 'ADPRSI_ant_ssl.mat']);
save ('ADPRSI_ant_ssl.mat', '-struct','ADPRSI_ant_ssl');

% clean descriptives for within group BMS:
ADPRSI_ant_descr.group(ADPRSI_ant_ssl.out_BMS) = [];


%% BMS all
BMS_input = [ADPRSI_ant_ssl.hgfv1_0_BMS.HGF_1_fixom.F, ...
    ADPRSI_ant_ssl.hgfv1_0_BMS.HGF_2l_v1.F, ...
    ADPRSI_ant_ssl.hgfv1_0_BMS.RW_1.F]; %

[antfMRI_BMS_hgfv1_0.alpha,antfMRI_BMS_hgfv1_0.exp_r,antfMRI_BMS_hgfv1_0.xp,antfMRI_BMS_hgfv1_0.pxp] = spm_BMS(BMS_input,1e6,1,1,1);
disp(antfMRI_BMS_hgfv1_0.exp_r);
[antfMRI_BMS_hgfv1_0.M,antfMRI_BMS_hgfv1_0.I] = max(antfMRI_BMS_hgfv1_0.exp_r);

save ('antfMRI_BMS_hgfv1_0.mat', '-struct','antfMRI_BMS_hgfv1_0');

%% Create figure
pos0 = get(0,'screenSize');
pos = [1,1,pos0(3)/1.5,pos0(4)/1.5];
figure('position',pos,...
    'color',[1 1 1],...
    'name','BMS all');

%% plot BMS results
% posterior probabilities:
hold on; subplot(1,3,1); bar([1], antfMRI_BMS_hgfv1_0.exp_r(1),'FaceColor',[0,176/255,240/255],'EdgeColor',[0,176/255,240/255]);
hold on; subplot(1,3,1); bar([2], antfMRI_BMS_hgfv1_0.exp_r(2),'FaceColor',[112/255,48/255,160/255],'EdgeColor',[112/255,48/255,160/255]);
hold on; subplot(1,3,1); bar([3], antfMRI_BMS_hgfv1_0.exp_r(3),'FaceColor',[255/255,192/255,0/255],'EdgeColor',[255/255,192/255,0/255]);

ylabel ('posterior probability', 'FontSize', 15); ylim([0 1]);
set(gca, 'XTick', []);
set(gca,'box','off'); get(gca, 'YTick'); set(gca, 'FontSize', 13);
ax1 = subplot(1,3,1);
ax1.YTick = [0 0.25 0.5 0.75 1.0];
h_leg = legend('hgf3l', 'hgf2l', 'RW', 'Location', 'east');
set(h_leg,'box','off','FontSize', 13);

% exceedance probabilities:
hold on; subplot(1,3,2); bar([1], antfMRI_BMS_hgfv1_0.xp(1),'FaceColor',[0,176/255,240/255],'EdgeColor',[0,176/255,240/255]);
hold on; subplot(1,3,2); bar([2], antfMRI_BMS_hgfv1_0.xp(2),'FaceColor',[112/255,48/255,160/255],'EdgeColor',[112/255,48/255,160/255]);
hold on; subplot(1,3,2); bar([3], antfMRI_BMS_hgfv1_0.xp(3),'FaceColor',[255/255,192/255,0/255],'EdgeColor',[255/255,192/255,0/255]);

ylabel('exceedance probability', 'FontSize', 15);
set(gca, 'XTick', []);
set(gca,'box','off'); get(gca, 'YTick'); set(gca, 'FontSize', 13);
ax2 = subplot(1,3,2);
ax2.YTick = [0 0.25 0.5 0.75 1.0];
h_leg2 = legend('hgf3l', 'hgf2l', 'RW', 'Location', 'east');
set(h_leg2,'box','off','FontSize', 13);

% protected exceedance probabilities:
hold on; subplot(1,3,3); bar([1], antfMRI_BMS_hgfv1_0.pxp(1),'FaceColor',[0,176/255,240/255],'EdgeColor',[0,176/255,240/255]);
hold on; subplot(1,3,3); bar([2], antfMRI_BMS_hgfv1_0.pxp(2),'FaceColor',[112/255,48/255,160/255],'EdgeColor',[112/255,48/255,160/255]);
hold on; subplot(1,3,3); bar([3], antfMRI_BMS_hgfv1_0.pxp(3),'FaceColor',[255/255,192/255,0/255],'EdgeColor',[255/255,192/255,0/255]);

ylabel('protected exceedance probability', 'FontSize', 15);
set(gca, 'XTick', []);
set(gca,'box','off'); get(gca, 'YTick'); set(gca, 'FontSize', 13);
ax2 = subplot(1,3,3);
ax2.YTick = [0 0.25 0.5 0.75 1.0];
h_leg2 = legend('hgf3l', 'hgf2l', 'RW', 'Location', 'east');
set(h_leg2,'box','off','FontSize', 13);

suptitle('all');

print -djpeg -r500 all_bms;
saveas (gcf,'all_bms','fig');

%% plot BMS results for each pharmacological condition:
%% placebo
BMS_input_pla_all = [ADPRSI_ant_ssl.hgfv1_0_BMS.HGF_1_fixom.F(ADPRSI_ant_descr.group ==0), ...
    ADPRSI_ant_ssl.hgfv1_0_BMS.HGF_2l_v1.F(ADPRSI_ant_descr.group ==0), ...
    ADPRSI_ant_ssl.hgfv1_0_BMS.RW_1.F(ADPRSI_ant_descr.group ==0)];
[antfMRI_BMS_hgfv1_0_pla.alpha,antfMRI_BMS_hgfv1_0_pla.exp_r,antfMRI_BMS_hgfv1_0_pla.xp, antfMRI_BMS_hgfv1_0_pla.pxp, antfMRI_BMS_hgfv1_0_pla.bor] = spm_BMS(BMS_input_pla_all,1e6,1,1,1);
disp(antfMRI_BMS_hgfv1_0_pla.exp_r);
save ('antfMRI_BMS_hgfv1_0_pla.mat', '-struct','antfMRI_BMS_hgfv1_0_pla');
save ('BMS_pla_all.mat', 'BMS_input_pla_all');

% create figure:
pos0 = get(0,'screenSize');
pos = [1,1,pos0(3)/1.5,pos0(4)/1.5];
figure(...
    'position',pos,...
    'color',[1 1 1],...
    'name','BMS pla');

% posterior probabilities:
hold on; subplot(1,3,1); bar([1], antfMRI_BMS_hgfv1_0_pla.exp_r(1),'FaceColor',[0,176/255,240/255],'EdgeColor',[0,176/255,240/255]);
hold on; subplot(1,3,1); bar([2], antfMRI_BMS_hgfv1_0_pla.exp_r(2),'FaceColor',[112/255,48/255,160/255],'EdgeColor',[112/255,48/255,160/255]);
hold on; subplot(1,3,1); bar([3], antfMRI_BMS_hgfv1_0_pla.exp_r(3),'FaceColor',[255/255,192/255,0/255],'EdgeColor',[255/255,192/255,0/255]);
ylabel ('posterior probability', 'FontSize', 15); ylim([0 1]);
set(gca, 'XTick', []);
set(gca,'box','off'); get(gca, 'YTick'); set(gca, 'FontSize', 13);
ax1 = subplot(1,3,1);
ax1.YTick = [0 0.25 0.5 0.75 1.0];
h_leg = legend('hgf3l', 'hgf2l','RW', 'Location', 'east');
set(h_leg,'box','off','FontSize', 13);

% exceedance probabilities:
hold on; subplot(1,3,2); bar([1], antfMRI_BMS_hgfv1_0_pla.xp(1),'FaceColor',[0,176/255,240/255],'EdgeColor',[0,176/255,240/255]);
hold on; subplot(1,3,2); bar([2], antfMRI_BMS_hgfv1_0_pla.xp(2),'FaceColor',[112/255,48/255,160/255],'EdgeColor',[112/255,48/255,160/255]);
hold on; subplot(1,3,2); bar([3], antfMRI_BMS_hgfv1_0_pla.xp(3),'FaceColor',[255/255,192/255,0/255],'EdgeColor',[255/255,192/255,0/255]);
ylabel('exceedance probability', 'FontSize', 15);
set(gca, 'XTick', []);
set(gca,'box','off'); get(gca, 'YTick'); set(gca, 'FontSize', 13);
ax2 = subplot(1,3,2);
ax2.YTick = [0 0.25 0.5 0.75 1.0];
h_leg2 = legend('hgf3l', 'hgf2l', 'RW','Location', 'east');
set(h_leg2,'box','off','FontSize', 13);

% protected exceedance probabilities:
hold on; subplot(1,3,3); bar([1], antfMRI_BMS_hgfv1_0_pla.pxp(1),'FaceColor',[0,176/255,240/255],'EdgeColor',[0,176/255,240/255]);
hold on; subplot(1,3,3); bar([2], antfMRI_BMS_hgfv1_0_pla.pxp(2),'FaceColor',[112/255,48/255,160/255],'EdgeColor',[112/255,48/255,160/255]);
hold on; subplot(1,3,3); bar([3], antfMRI_BMS_hgfv1_0_pla.pxp(3),'FaceColor',[255/255,192/255,0/255],'EdgeColor',[255/255,192/255,0/255]);
ylabel('protected exceedance probability', 'FontSize', 15);
set(gca, 'XTick', []);
set(gca,'box','off'); get(gca, 'YTick'); set(gca, 'FontSize', 13);
ax2 = subplot(1,3,3);
ax2.YTick = [0 0.25 0.5 0.75 1.0];
h_leg2 = legend('hgf3l', 'hgf2l', 'RW', 'Location', 'east');
set(h_leg2,'box','off','FontSize', 13);

suptitle('placebo');
print -djpeg -r500 pla_bms;
saveas (gcf,'pla_bms','fig');

%% amisulpride
BMS_input_ami_all = [ADPRSI_ant_ssl.hgfv1_0_BMS.HGF_1_fixom.F(ADPRSI_ant_descr.group == 1), ...
    ADPRSI_ant_ssl.hgfv1_0_BMS.HGF_2l_v1.F(ADPRSI_ant_descr.group == 1), ...
    ADPRSI_ant_ssl.hgfv1_0_BMS.RW_1.F(ADPRSI_ant_descr.group == 1)];
[antfMRI_BMS_hgfv1_0_ami.alpha,antfMRI_BMS_hgfv1_0_ami.exp_r,antfMRI_BMS_hgfv1_0_ami.xp, antfMRI_BMS_hgfv1_0_ami.pxp, antfMRI_BMS_hgfv1_0_ami.bor] = spm_BMS(BMS_input_ami_all,1e6,1,1,1);
disp(antfMRI_BMS_hgfv1_0_ami.exp_r); 
save ('antfMRI_BMS_hgfv1_0_ami.mat', '-struct','antfMRI_BMS_hgfv1_0_ami');
save ('BMS_ami_all.mat',  'BMS_input_ami_all');

% create figure:
pos0 = get(0,'screenSize');
pos = [1,1,pos0(3)/1.5,pos0(4)/1.5];
figure(...
    'position',pos,...
    'color',[1 1 1],...
    'name','BMS ami');

% posterior probabilities:
hold on; subplot(1,3,1); bar([1], antfMRI_BMS_hgfv1_0_ami.exp_r(1),'FaceColor',[0,176/255,240/255],'EdgeColor',[0,176/255,240/255]);
hold on; subplot(1,3,1); bar([2], antfMRI_BMS_hgfv1_0_ami.exp_r(2),'FaceColor',[112/255,48/255,160/255],'EdgeColor',[112/255,48/255,160/255]);
hold on; subplot(1,3,1); bar([3], antfMRI_BMS_hgfv1_0_ami.exp_r(3),'FaceColor',[255/255,192/255,0/255],'EdgeColor',[255/255,192/255,0/255]);
ylabel ('posterior probability', 'FontSize', 15); ylim([0 1]);
set(gca, 'XTick', []);
set(gca,'box','off'); get(gca, 'YTick'); set(gca, 'FontSize', 13);
ax1 = subplot(1,3,1);
ax1.YTick = [0 0.25 0.5 0.75 1.0];
h_leg = legend('hgf3l', 'hgf2l', 'RW', 'Location', 'east');
set(h_leg,'box','off','FontSize', 13);

% exceedance probabilities:
hold on; subplot(1,3,2); bar([1], antfMRI_BMS_hgfv1_0_ami.xp(1),'FaceColor',[0,176/255,240/255],'EdgeColor',[0,176/255,240/255]);
hold on; subplot(1,3,2); bar([2], antfMRI_BMS_hgfv1_0_ami.xp(2),'FaceColor',[112/255,48/255,160/255],'EdgeColor',[112/255,48/255,160/255]);
hold on; subplot(1,3,2); bar([3], antfMRI_BMS_hgfv1_0_ami.xp(3),'FaceColor',[255/255,192/255,0/255],'EdgeColor',[255/255,192/255,0/255]);
ylabel('exceedance probability', 'FontSize', 15);
set(gca, 'XTick', []);
set(gca,'box','off'); get(gca, 'YTick'); set(gca, 'FontSize', 13);
ax2 = subplot(1,3,2);
ax2.YTick = [0 0.25 0.5 0.75 1.0];
h_leg2 = legend('hgf3l', 'hgf2l', 'RW', 'Location', 'east');
set(h_leg2,'box','off','FontSize', 13);

% protected exceedance probabilities:
hold on; subplot(1,3,3); bar([1], antfMRI_BMS_hgfv1_0_ami.pxp(1),'FaceColor',[0,176/255,240/255],'EdgeColor',[0,176/255,240/255]);
hold on; subplot(1,3,3); bar([2], antfMRI_BMS_hgfv1_0_ami.pxp(2),'FaceColor',[112/255,48/255,160/255],'EdgeColor',[112/255,48/255,160/255]);
hold on; subplot(1,3,3); bar([3], antfMRI_BMS_hgfv1_0_ami.pxp(3),'FaceColor',[255/255,192/255,0/255],'EdgeColor',[255/255,192/255,0/255]);
ylabel('protected exceedance probability', 'FontSize', 15);
set(gca, 'XTick', []);
set(gca,'box','off'); get(gca, 'YTick'); set(gca, 'FontSize', 13);
ax2 = subplot(1,3,3);
ax2.YTick = [0 0.25 0.5 0.75 1.0];
h_leg2 = legend('hgf3l', 'hgf2l', 'RW', 'Location', 'east');
set(h_leg2,'box','off','FontSize', 13);

suptitle('amisulpride');
print -djpeg -r500 ami_bms;
saveas (gcf,'ami_bms','fig');

%% biperiden
BMS_input_bip_all = [ADPRSI_ant_ssl.hgfv1_0_BMS.HGF_1_fixom.F(ADPRSI_ant_descr.group == 2),  ...
    ADPRSI_ant_ssl.hgfv1_0_BMS.HGF_2l_v1.F(ADPRSI_ant_descr.group == 2), ...
    ADPRSI_ant_ssl.hgfv1_0_BMS.RW_1.F(ADPRSI_ant_descr.group == 2)];
[antfMRI_BMS_hgfv1_0_bip.alpha,antfMRI_BMS_hgfv1_0_bip.exp_r,antfMRI_BMS_hgfv1_0_bip.xp, antfMRI_BMS_hgfv1_0_bip.pxp, antfMRI_BMS_hgfv1_0_bip.bor] = spm_BMS(BMS_input_bip_all,1e6,1,1,1);
disp(antfMRI_BMS_hgfv1_0_bip.exp_r); 
save ('antfMRI_BMS_hgfv1_0_bip.mat', '-struct','antfMRI_BMS_hgfv1_0_bip');
save ('BMS_bip_all.mat', 'BMS_input_bip_all');

% create figure:
pos0 = get(0,'screenSize');
pos = [1,1,pos0(3)/1.5,pos0(4)/1.5];
figure('position',pos,...
    'color',[1 1 1],...
    'name','BMS bip');

% posterior probabilities:
hold on; subplot(1,3,1); bar([1], antfMRI_BMS_hgfv1_0_bip.exp_r(1),'FaceColor',[0,176/255,240/255],'EdgeColor',[0,176/255,240/255]);
hold on; subplot(1,3,1); bar([2], antfMRI_BMS_hgfv1_0_bip.exp_r(2),'FaceColor',[112/255,48/255,160/255],'EdgeColor',[112/255,48/255,160/255]);
hold on; subplot(1,3,1); bar([3], antfMRI_BMS_hgfv1_0_bip.exp_r(3),'FaceColor',[255/255,192/255,0/255],'EdgeColor',[255/255,192/255,0/255]);
ylabel ('posterior probability', 'FontSize', 15); ylim([0 1]);
set(gca, 'XTick', []);
set(gca,'box','off'); get(gca, 'YTick'); set(gca, 'FontSize', 13);
ax1 = subplot(1,3,1);
ax1.YTick = [0 0.25 0.5 0.75 1.0];
h_leg = legend('hgf3l', 'hgf2l', 'RW', 'Location', 'east');
set(h_leg,'box','off','FontSize', 13);

% exceedance probabilities:
hold on; subplot(1,3,2); bar([1], antfMRI_BMS_hgfv1_0_bip.xp(1),'FaceColor',[0,176/255,240/255],'EdgeColor',[0,176/255,240/255]);
hold on; subplot(1,3,2); bar([2], antfMRI_BMS_hgfv1_0_bip.xp(2),'FaceColor',[112/255,48/255,160/255],'EdgeColor',[112/255,48/255,160/255]);
hold on; subplot(1,3,2); bar([3], antfMRI_BMS_hgfv1_0_bip.xp(3),'FaceColor',[255/255,192/255,0/255],'EdgeColor',[255/255,192/255,0/255]);
ylabel('exceedance probability', 'FontSize', 15);
set(gca, 'XTick', []);
set(gca,'box','off'); get(gca, 'YTick'); set(gca, 'FontSize', 13);
ax2 = subplot(1,3,2);
ax2.YTick = [0 0.25 0.5 0.75 1.0];
h_leg2 = legend('hgf3l', 'hgf2l', 'RW', 'Location', 'east');
set(h_leg2,'box','off','FontSize', 13);

% protected exceedance probabilities:
hold on; subplot(1,3,3); bar([1], antfMRI_BMS_hgfv1_0_bip.pxp(1),'FaceColor',[0,176/255,240/255],'EdgeColor',[0,176/255,240/255]);
hold on; subplot(1,3,3); bar([2], antfMRI_BMS_hgfv1_0_bip.pxp(2),'FaceColor',[112/255,48/255,160/255],'EdgeColor',[112/255,48/255,160/255]);
hold on; subplot(1,3,3); bar([3], antfMRI_BMS_hgfv1_0_bip.pxp(3),'FaceColor',[255/255,192/255,0/255],'EdgeColor',[255/255,192/255,0/255]);
ylabel('protected exceedance probability', 'FontSize', 15);
set(gca, 'XTick', []);
set(gca,'box','off'); get(gca, 'YTick'); set(gca, 'FontSize', 13);
ax2 = subplot(1,3,3);
ax2.YTick = [0 0.25 0.5 0.75 1.0];
h_leg2 = legend('hgf3l', 'hgf2l', 'RW', 'Location', 'east');
set(h_leg2,'box','off','FontSize', 13);

suptitle('biperiden');
print -djpeg -r500 bip_bms;
saveas (gcf,'bip_bms','fig');

close all;
end