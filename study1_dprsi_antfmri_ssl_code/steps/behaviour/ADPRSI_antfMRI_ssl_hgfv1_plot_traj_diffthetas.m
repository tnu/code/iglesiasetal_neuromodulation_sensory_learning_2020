function ADPRSI_antfMRI_ssl_hgfv1_plot_traj_diffthetas
% ADPRSI_antfMRI_ssl_hgfv1_plot_traj_diffthetas: plots mu3 and da2 using
% different values of theta
%   IN:     -
%   OUT:    -

% 12-03-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

% Set paths
workdir = ADPRSI_antfMRI_ssl_set_workdir;
options = ADPRSI_antfMRI_ssl_set_analysis_options;
[ details, paths ] = ADPRSI_antfMRI_ssl_subjects( 1 );

% Start diary
diary(paths.logfile_hgf_ssl);

% define models
maskModel = {'/HGF_fixom_v1'};
maskprc = {'hgf_binary_theta'};

% specify the HGF initial values and learning parameters:
mu2_0 = 0;
sa2_0 = 1;
mu3_0 = 1;
sa3_0 = 1;
ka = 0;
om = -4;
th = [0.1 0.05 0.005 0.0005];

% load mat file with inputs from first subject
VSmat = load([paths.res_behaviour_ssl,maskModel{1},'/SSL.VS.mat']);
% loop through different values of theta
for v = 1:numel(th)
    p = [0.2757 0.5250 1 0.9139 1.2719 -4 th(v)];
    % simulate data
    SSL.sim(v) = simModel(VSmat.VS.CongrIn(:,1), 'hgf_binary', p,  'unitsq_sgm', 2);
    
    if ~exist([paths.quality_ssl_behav,'/HGF/theta_traj'], 'dir')
        mkdir([paths.quality_ssl_behav,'/HGF/theta_traj']);
    end
    cd ([paths.quality_ssl_behav,'/HGF/theta_traj']);
    save('SSL.sim.mat', '-struct','SSL','sim');

    hgf_binary_plotTraj(SSL.sim(v));
    print(['theta_traj_',num2str(v)],'-djpeg')
    close all;
end
% plot trajectories
figure('Color', [1 1 1],'Position', [20 10 950 500]);
subplot(2,1,1)
plot(SSL.sim(1).traj.mu3hat,'m');hold on
plot(SSL.sim(2).traj.mu3hat,'b');hold on
plot(SSL.sim(3).traj.mu3hat,'r');hold on
plot(SSL.sim(4).traj.mu3hat,'g');hold on
title('mu3hat');
legend({'th: 0.1','th: 0.05','th: 0.005','th: 0.0005'},'FontSize',10,'Location','northeast')

subplot(2,1,2)
plot(SSL.sim(1).traj.da2,'m');hold on
plot(SSL.sim(2).traj.da2,'b');hold on
plot(SSL.sim(3).traj.da2,'r');hold on
plot(SSL.sim(4).traj.da2,'g');hold on
title('da2');
legend({'th: 0.1','th: 0.05','th: 0.005','th: 0.0005'},'FontSize',10,'Location','northeast')
print(['All_theta_traj'],'-djpeg')
end
