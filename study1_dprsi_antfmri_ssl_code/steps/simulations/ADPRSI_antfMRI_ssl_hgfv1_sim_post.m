function ADPRSI_antfMRI_ssl_hgfv1_sim_post
% ADPRSI_antfMRI_ssl_hgfv1_sim_post:
% simulate data using the participants posterior parameter estimates and
% recover parameter values.

%   IN:     -

%   OUT:    -

% 09-01-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

% Set paths
workdir = ADPRSI_antfMRI_ssl_set_workdir;
options = ADPRSI_antfMRI_ssl_set_analysis_options;
[ details, paths ] = ADPRSI_antfMRI_ssl_subjects( 1 );

% Start diary
diary(paths.logfile_hgf_ssl);

% Initialize
disp('running HGF simulations');

% define where to store the simulations:
ResDir = fullfile([paths.res_group_ssl_behaviour,'/simulations/']);
[~,~]=mkdir(ResDir);
cd(ResDir);

% define model:
maskModel = {'/HGF_fixom_v1'};
config = eval('hgf_binary_config');
config_obs = eval('unitsq_sgm_config');

% load input sequence
traj = load([paths.res_behaviour_ssl,maskModel{1},'/SSL.VS.mat']);
u = traj.VS.est.u;

%% load HGF results
AnaSSL = load([paths.res_group_ssl_behaviour,maskModel{1},'/A_SSL_VS.mat']);
subj_out = load([paths.res_group_ssl_glm,'/sslbin/fixom_bin3_spm12_fact/antfMRI_ssl_group.mat']);

AnaSSL.ka(subj_out.out_final)=[];
AnaSSL.om(subj_out.out_final)=[];
AnaSSL.th(subj_out.out_final)=[];
AnaSSL.mu2_0(subj_out.out_final)=[];
AnaSSL.mu3_0(subj_out.out_final)=[];
AnaSSL.sa2_0(subj_out.out_final)=[];
AnaSSL.sa3_0(subj_out.out_final)=[];
AnaSSL.ze(subj_out.out_final)=[];

%% run simulations
for i = 1:numel(AnaSSL.ka)
    try
        sim(i) = simModel(u,...
            'hgf_binary', [AnaSSL.mu2_0(i) ...
            AnaSSL.sa2_0(i) AnaSSL.mu3_0(i) AnaSSL.sa3_0(i) ...
            AnaSSL.ka(i) AnaSSL.om(i) AnaSSL.th(i)],...
            'unitsq_sgm', AnaSSL.ze(i));
        
        % Simulated responses
        data(i).y = sim(i).y;
        % Experimental inputs
        data(i).u = sim(i).u;
        save('sim.mat','sim')
        save('data.mat', 'data')
    catch e
        fprintf(1,'There was an error! The message was:\n%s',e.message);
        for j = length(e.stack)
            disp(['line: ', num2str(e.stack(j).line),' ',e.stack(j).file])
        end
    end
end

%% Finally, we can run the estimation.
for id = 1:numel(data)
    hgf_est(id) = fitModel(data(id).y, data(id).u, 'hgf_binary_config', 'unitsq_sgm_config', 'quasinewton_optim_config');
    save('hgf_est.mat','hgf_est');
end

%% plot parameter recovery
for i = 1:numel(data)
    ka_est(i,1)=hgf_est(i).p_prc.p(1,5);
    th_est(i,1)=hgf_est(i).p_prc.p(1,7);
    ze_est(i,1)=hgf_est(i).p_obs.p;
    mu2_est(i,1)=hgf_est(i).p_prc.p(1,1);
    sa2_est(i,1)=hgf_est(i).p_prc.p(1,2);
    sa3_est(i,1)=hgf_est(i).p_prc.p(1,4);
end

fig1 = figure('Color',[1,1,1],'pos',[10 10 1050 500]); 
sb1 = subplot(1,3,1); scatter(sb1,AnaSSL.ka',ka_est,'filled');hold on;
refline(sb1); refline(1,0);
line(xlim, [hgf_est(1).c_prc.priormus(1,5), hgf_est(1).c_prc.priormus(1,5)], ...
    'LineWidth', 2, 'Color', 'black');
ylim([(min(ka_est)-0.1) (max(ka_est)+0.1)]);
title('\kappa','Fontsize', 20); hold on;
xlabel('value in')
ylabel('estimated value')

sb2 = subplot(1,3,2); scatter(sb2, AnaSSL.th, th_est,'filled');hold on;
refline(sb2); refline(1,0);
line(xlim, [hgf_est(1).c_prc.priormus(1,7), hgf_est(1).c_prc.priormus(1,7)], ...
    'LineWidth', 2, 'Color', 'black');
ylim([(min(th_est)-0.001) (max(th_est)+0.001)]);
title('\vartheta','Fontsize', 20); hold on;
xlabel('value in')
ylabel('estimated value')

sb3=subplot(1,3,3); scatter(sb3, AnaSSL.ze, ze_est,'filled');hold on;
refline(sb3); refline(1,0);
line(xlim, [hgf_est(1).c_obs.priormus(1,1), hgf_est(1).c_obs.priormus(1,1)], ...
    'LineWidth', 2, 'Color', 'black');
ylim([(min(ze_est)-0.001) (max(ze_est)+0.001)]);
title('\zeta','Fontsize', 20); hold on;
xlabel('value in')
ylabel('estimated value')

suptitle('Parameter Recovery');
saveas(fig1,['hgfv1_0_simulations',], 'fig');


fig2 = figure('Color',[1,1,1],'pos',[10 10 1050 500]); 
sb4 = subplot(1,3,1); scatter(sb4,AnaSSL.mu2_0',mu2_est,'filled');hold on;
refline(sb4); refline(1,0);
ylim([(min(mu2_est)-0.1) (max(mu2_est)+0.1)]);
title('\mu_2','Fontsize', 20); hold on;
xlabel('value in')
ylabel('estimated value')

sb5 = subplot(1,3,2); scatter(sb5, AnaSSL.sa2_0, sa2_est,'filled');hold on;
refline(sb5); refline(1,0);
ylim([(min(sa2_est)-0.001) (max(sa2_est)+0.001)]);
title('\sigma_2','Fontsize', 20); hold on;
xlabel('value in')
ylabel('estimated value')

sb6=subplot(1,3,3); scatter(sb6, AnaSSL.sa3_0, sa3_est,'filled');hold on;
refline(sb6); refline(1,0);
ylim([(min(sa3_est)-0.001) (max(sa3_est)+0.001)]);
title('\sigma_3','Fontsize', 20); hold on;
xlabel('value in')
ylabel('estimated value')
saveas(fig2,['hgfv1_0_simulations_2',], 'fig');

suptitle('Parameter Recovery - initial values of trajectories:');

end
