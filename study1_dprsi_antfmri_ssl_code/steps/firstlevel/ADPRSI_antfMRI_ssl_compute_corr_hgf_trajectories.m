function ADPRSI_antfMRI_ssl_compute_corr_hgf_trajectories
% ADPRSI_antfMRI_ssl_compute_corr_hgf_trajectories: computes for every subject: 
% - correlations between the HGF trajectories entered into the GLM and 
% - correlations between the model parameters of the winning model

% - GLM1 - epsilons:
%   3 trajectories:
%     -epsilon2
%     -epsilon3
%     -epsilon_ch

% - GLM2 - deltas and precision-weights:
%   5 trajectories
%     -da1                 -psi2
%     -da2                 -psi3
%     -da_ch

%   IN:     -
%   OUT:    -

% 19-02-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================
% Set paths
workdir = ADPRSI_antfMRI_ssl_set_workdir;
options = ADPRSI_antfMRI_ssl_set_analysis_options;
[ details, paths ] = ADPRSI_antfMRI_ssl_subjects( 1 );

% Start diary
diary(paths.logfile_hgf_ssl);

%% find the winning model
WinningModel_BMS = load((fullfile(paths.res_hgf_ssl_bms,'/antfMRI_BMS_hgfv1_0.mat')));
[WinningModel_BMS.M,WinningModel_BMS.I] = max(WinningModel_BMS.exp_r);

maskModel = {'/HGF_fixom_v1',  '/HGF_2l_v1', '/RW_v1'};
WinningModel = maskModel{WinningModel_BMS.I};
disp('****************');
disp(WinningModel);
disp('****************');

% load structure with final list of excluded participants:
subj_out = load([paths.res_group_ssl_glm,'/sslbin/fixom_bin3_spm12_fact/antfMRI_ssl_group.mat']);
disp(['subjects out: ', num2str(subj_out.out_final')]);

% load descriptives:
ant_ssl = load([paths.res_group_ssl_descr,'/ADPRSI_antfMRI_ssl.mat']);

% determine number of subjects in final analysis:
Nr_subjects = length(ant_ssl.subj)-length(subj_out.out_final);
indices_subjects = [1:1:length(ant_ssl.subj)];
indices_subjects(subj_out.out_final)=[];

%% plot parameter correlations:
for id = indices_subjects
    disp(['computing hgf correlations for subj: ', details.subjname,' (',num2str(id),')']);
    [ details, paths ] = ADPRSI_antfMRI_ssl_subjects( id );
    % plot parameter correlations for every subject
    AnaSSL(id) = load([paths.res_behaviour_ssl,WinningModel,'/SSL.VS.mat']);
    fit_plotCorr(AnaSSL(id).VS.est);
    print -djpeg corr_hgf_param_ssl;
    
    if ~exist(paths.logdir_group_ssl, 'dir')
        mkdir([paths.quality_ssl_behav,'/HGF/',WinningModel]);
    end
    
    % copy file to quality folder:
    movefile ('corr_hgf_param_ssl.jpg', [paths.quality_ssl_behav,'/HGF/',WinningModel,'/corr_hgf_param_ssl_',details.subjname,'.jpg']);
    delete(findall(0,'Type','figure'));
end


%% plot regressor correlations:
% determine folders with SPM.mat files:
maskGLM = {'/fixom_bin3_spm12/','/fixom_bin5_spm12/'};

corr_hgf_ssl.epsilon.r= cell(1,Nr_subjects);
corr_hgf_ssl.epsilon.z = cell(1,Nr_subjects);
% Go through GLMs
for g = 1:2
    clear corr_matr;
    clear corr_hgf_ssl.epsilon;
    clear A_corr_glm_da;
    clear c_glm_da;
    % Go through subjects
    for id = indices_subjects
        [ details, paths ] = ADPRSI_antfMRI_ssl_subjects( id );
        clear GlmSSL;
        clear SPM_epsilon;
        clear SPM_da;
        
        %% load input regressors:
        GlmSSL(id)= load([paths.res_first_ssl,'/GLM_bin_HGF_fixom_all.mat']);
        cd (paths.res_first_ssl);
        mkdir corr_glmSSL
        cd ([paths.res_first_ssl,'/corr_glmSSL/']);
        !rm c.mat
        !rm *.fig
        
        %% plot HGF trajectories per condition (i.e. face/house)
        % epsilons:
        figure('Color', [1 1 1],'Position', [20 10 1250 1000]);
        subplot(6,1,1);plot(GlmSSL(id).SSL_input.epsilon3_Face,'magenta');title('epsilon3 Face');
        subplot(6,1,3);plot(GlmSSL(id).SSL_input.epsilon_ch_Face,'blue');title('epsilon ch Face');
        subplot(6,1,5);plot(GlmSSL(id).SSL_input.epsilon2_Face,'red');title('epsilon2 Face');
        
        subplot(6,1,2);plot(GlmSSL(id).SSL_input.epsilon3_House,'magenta');title('epsilon3 House');
        subplot(6,1,4);plot(GlmSSL(id).SSL_input.epsilon_ch_House,'blue');title('epsilon ch House');
        subplot(6,1,6);plot(GlmSSL(id).SSL_input.epsilon2_House,'red');title('epsilon2 House');
        print -djpeg GLM_epsilon_trajectories;
        copyfile ('GLM_epsilon_trajectories.jpg', [paths.quality_ssl_behav,'/HGF/',WinningModel,'/GLM_epsilon_trajectories_',details.subjname,'.jpg']);
        
        figure('Color', [1 1 1],'Position', [20 10 1250 1000]);
        subplot(6,1,1);plot(GlmSSL(id).epsilon3_Face,'magenta');title('epsilon3 Face');
        subplot(6,1,3);plot(GlmSSL(id).epsilon_ch_Face,'blue');title('epsilon ch Face');
        subplot(6,1,5);plot(GlmSSL(id).epsilon2_Face,'red');title('epsilon2 Face');
        
        subplot(6,1,2);plot(GlmSSL(id).epsilon3_House,'magenta');title('epsilon3 House');
        subplot(6,1,4);plot(GlmSSL(id).epsilon_ch_House,'blue');title('epsilon ch House');
        subplot(6,1,6);plot(GlmSSL(id).epsilon2_House,'red');title('epsilon2 House');
        print -djpeg GLM_bin_epsilon_trajectories;
        copyfile ('GLM_bin_epsilon_trajectories.jpg', [paths.quality_ssl_behav,'/HGF/',WinningModel,'/GLM_epsilon_trajectories_',details.subjname,'.jpg']);
        
        % da/pw:
        figure('Color', [1 1 1],'Position', [20 10 1250 1000]);
        subplot(10,1,1);plot(GlmSSL(id).SSL_input.pw3_Face,'cyan');title('pw3 Face');
        subplot(10,1,3);plot(GlmSSL(id).SSL_input.sa2_Face,'green');title('pw2 Face');
        subplot(10,1,5);plot(GlmSSL(id).SSL_input.da2_Face,'magenta');title('da2 Face');
        subplot(10,1,7);plot(GlmSSL(id).SSL_input.da_ch_Face,'blue');title('da ch Face');
        subplot(10,1,9);plot(GlmSSL(id).SSL_input.da1_Face,'red');title('da1 Face');
        
        subplot(10,1,2);plot(GlmSSL(id).SSL_input.pw3_House,'cyan');title('pw3 House');
        subplot(10,1,4);plot(GlmSSL(id).SSL_input.sa2_House,'green');title('pw2 House');
        subplot(10,1,6);plot(GlmSSL(id).SSL_input.da2_House,'magenta');title('da2 House');
        subplot(10,1,8);plot(GlmSSL(id).SSL_input.da_ch_House,'blue');title('da ch House');
        subplot(10,1,10);plot(GlmSSL(id).SSL_input.da1_House,'red');title('da1 House');
        print -djpeg GLM_da_trajectories;
        copyfile ('GLM_da_trajectories.jpg', [paths.quality_ssl_behav,'/HGF/',WinningModel,'/GLM_da_trajectories_',details.subjname,'.jpg']);
        
        figure('Color', [1 1 1],'Position', [20 10 1250 1000]);
        subplot(10,1,1);plot(GlmSSL(id).pw3_Face,'cyan');title('pw3 Face');
        subplot(10,1,3);plot(GlmSSL(id).pw2_Face,'green');title('pw2 Face');
        subplot(10,1,5);plot(GlmSSL(id).da2_Face,'magenta');title('da2 Face');
        subplot(10,1,7);plot(GlmSSL(id).da_ch_Face,'blue');title('da ch Face');
        subplot(10,1,9);plot(GlmSSL(id).da1_Face,'red');title('da1 Face');
        
        subplot(10,1,2);plot(GlmSSL(id).pw3_House,'cyan');title('pw3 House');
        subplot(10,1,4);plot(GlmSSL(id).pw2_House,'green');title('pw2 House');
        subplot(10,1,6);plot(GlmSSL(id).da2_House,'magenta');title('da2 House');
        subplot(10,1,8);plot(GlmSSL(id).da_ch_House,'blue');title('da ch House');
        subplot(10,1,10);plot(GlmSSL(id).da1_House,'red');title('da1 House');
        print -djpeg GLM_bin_da_trajectories;
        copyfile ('GLM_bin_da_trajectories.jpg', [paths.quality_ssl_behav,'/HGF/',WinningModel,'/GLM_da_trajectories_',details.subjname,'.jpg']);
        
        close all;
        
        %% compute correlation between input regressors and plot them:
        % epsilons:
        GlmSSL(id).all_epsilon=[GlmSSL(id).Face, GlmSSL(id).epsilon2_Face, GlmSSL(id).epsilon3_Face, GlmSSL(id).epsilon_ch_Face, ...
            GlmSSL(id).House, GlmSSL(id).epsilon2_House, GlmSSL(id).epsilon3_House, GlmSSL(id).epsilon_ch_House];
        GlmSSL(id).corr_epsilon = corrcoef(GlmSSL(id).all_epsilon);
        imagesc(GlmSSL(id).corr_epsilon,[-1 1]);colorbar;
        set(gca, 'XTick', [1:8], 'XTickLabel', {'CondFace' 'epsilon2 Face' 'epsilon3 Face ' 'epsilon ch Face' 'CondHouse' 'epsilon2 House' 'epsilon3 House ' 'epsilon ch House'});
        set(gca, 'YTick', [1:8], 'YTickLabel', {'CondFace' 'epsilon2 Face' 'epsilon3 Face ' 'epsilon ch Face' 'CondHouse' 'epsilon2 House' 'epsilon3 House ' 'epsilon ch House'});
        ax=gca;
        ax.XTickLabelRotation = 45;
        title('Correlation all epsilon input');
        tString = num2str(GlmSSL(id).corr_epsilon(:), '%0.2f'); % Create strings
        tString = strtrim(cellstr(tString));                    % Remove any space
        [x,y] = meshgrid(1:8);                                  % Create x and y coordinates for the strings
        hStrings = text(x(:),y(:),tString(:),...                % Plot the strings
            'HorizontalAlignment','center');
        textColors = repmat(GlmSSL(id).corr_epsilon(:) > 0.5,1,3)|repmat(GlmSSL(id).corr_epsilon(:) < -0.5,1,3);
        set(hStrings,{'Color'},num2cell(~textColors,2));
        print -djpeg Corr_input_glm_epsilon;
        copyfile ('Corr_input_glm_epsilon.jpg', [paths.quality_ssl_first,'/Corr_input_glm_epsilon_',details.subjname,'.jpg']);
        
        % da/pw:
        GlmSSL(id).all_da=[GlmSSL(id).Face, GlmSSL(id).da1_Face, GlmSSL(id).da2_Face, GlmSSL(id).da_ch_Face, GlmSSL(id).pw2_Face, GlmSSL(id).pw3_Face ...
            GlmSSL(id).House, GlmSSL(id).da1_House, GlmSSL(id).da2_House, GlmSSL(id).da_ch_House, GlmSSL(id).pw2_House, GlmSSL(id).pw3_House];
        GlmSSL(id).corr_da = corrcoef(GlmSSL(id).all_da);
        imagesc(GlmSSL(id).corr_da,[-1 1]);colorbar;
        set(gca, 'XTick', [1:12], 'XTickLabel', {'CondFace' 'da1 Face' 'da2 Face ' 'da ch Face' 'pw2 Face' 'pw3 Face' 'CondHouse' 'da1 House' 'da2 House ' 'da ch House' 'pw2 House' 'pw3 House'});
        set(gca, 'YTick', [1:12], 'YTickLabel', {'CondFace' 'da1 Face' 'da2 Face ' 'da ch Face' 'pw2 Face' 'pw3 Face' 'CondHouse' 'da1 House' 'da2 House ' 'da ch House' 'pw2 House' 'pw3 House'});
        ax=gca;
        ax.XTickLabelRotation = 45;
        title('Correlation all da input');
        tString = num2str(GlmSSL(id).corr_da(:), '%0.2f');    % Create strings
        tString = strtrim(cellstr(tString));                  % Remove any space
        [x,y] = meshgrid(1:12);                               % Create x and y coordinates for the strings
        hStrings = text(x(:),y(:),tString(:),...              % Plot the strings
            'HorizontalAlignment','center');
        textColors = repmat(GlmSSL(id).corr_da(:) > 0.5,1,3)|repmat(GlmSSL(id).corr_da(:) < -0.5,1,3);
        set(hStrings,{'Color'},num2cell(~textColors,2));
        print -djpeg Corr_input_glm_da;
        copyfile ('Corr_input_glm_da.jpg', [paths.quality_ssl_first,'/Corr_input_glm_da_',details.subjname,'.jpg']);
        
        save('GlmSSL.mat','GlmSSL');
        
        %% load SPM.mat files:
        SPM_epsilon = load([paths.res_first_ssl,maskGLM{1},'/SPM.mat']);
        SPM_da= load([paths.res_first_ssl,maskGLM{2},'/SPM.mat']);
        
        %% compute correlation between convolved regressors
        % select relevant regressors (only basic regressor):
        [c_spmxKXs_da.ssl(id).r p]= corrcoef(SPM_da.SPM.xX.xKXs.X(:,[1 3 5 7 9 11 13 15 17 19 21 23]));
        [c_spmxKXs_epsilon.ssl(id).r p]= corrcoef(SPM_epsilon.SPM.xX.xKXs.X(:,[1 3 5 7 9 11 13 15]));
        [c_spmxX_da.ssl(id).r p]= corrcoef(SPM_da.SPM.xX.X(:,[1 3 5 7 9 11 13 15 17 19 21 23]));
        [c_spmxX_epsilon.ssl(id).r p]= corrcoef(SPM_epsilon.SPM.xX.X(:,[1 3 5 7 9 11 13 15]));
        
        %% transform to z-values:
        c_spmxKXs_epsilon.ssl(id).z = fisherz(c_spmxKXs_epsilon.ssl(id).r);
        c_spmxKXs_da.ssl(id).z = fisherz(c_spmxKXs_da.ssl(id).r);
        
        c_spmxX_epsilon.ssl(id).z = fisherz(c_spmxX_epsilon.ssl(id).r);
        c_spmxX_da.ssl(id).z = fisherz(c_spmxX_da.ssl(id).r);
        
        c_glm_epsilon.ssl(id).r = GlmSSL(id).corr_epsilon;
        c_glm_da.ssl(id).r = GlmSSL(id).corr_da;
        c_glm_epsilon.ssl(id).z = fisherz(GlmSSL(id).corr_epsilon);
        c_glm_da.ssl(id).z = fisherz(GlmSSL(id).corr_da);
        
        %% load single subject correlation matrix, compute mean over all subjects
        %% and plot mean correlations over all regressors and for each condition
        if g == 1
            % epsilon GLM
            clear tString
            clear x
            clear y
            clear hStrings
            clear textColors
            
            corr_hgf_ssl.epsilon.r{id} = cat(3,c_glm_epsilon.ssl(id).r);
            corr_hgf_ssl.epsilon.dr   = cat(3,corr_hgf_ssl.epsilon.r{:});
            corr_hgf_ssl.epsilon.z{id} = cat(3,c_glm_epsilon.ssl(id).z);
            corr_hgf_ssl.epsilon.dz   = cat(3,corr_hgf_ssl.epsilon.z{:});
            
            cd([paths.quality_ssl_first]);
            save('corr_hgf_ssl', '-struct','corr_hgf_ssl');
            %% compute group mean over z-transformed coefficients and transform back
            A_corr_glm_epsilon.mean_z = mean(corr_hgf_ssl.epsilon.dz,3);
            A_corr_glm_epsilon.mean_r = ifisherz(A_corr_glm_epsilon.mean_z);
            
            cd([paths.quality_ssl_first]);
            save('A_corr_glm_epsilon', '-struct','A_corr_glm_epsilon');
            figure('Color', [1 1 1]);
            
            All_glm_epsilon = A_corr_glm_epsilon.mean_r(:,:);
            imagesc(All_glm_epsilon,[-1 1]);colorbar;
            set(gca, 'XTick', [1:8], 'XTickLabel', {'CondFace' 'epsilon2 Face' 'epsilon3 Face ' 'epsilon ch Face' 'CondHouse' 'epsilon2 House' 'epsilon3 House ' 'epsilon ch House'});
            set(gca, 'YTick', [1:8], 'YTickLabel', {'CondFace' 'epsilon2 Face' 'epsilon3 Face ' 'epsilon ch Face' 'CondHouse' 'epsilon2 House' 'epsilon3 House ' 'epsilon ch House'});
            ax=gca;
            ax.XTickLabelRotation = 45;
            title('Correlation all glm epsilon');
            tString = num2str(All_glm_epsilon(:), '%0.2f');     % Create strings
            tString = strtrim(cellstr(tString));                % Remove any space
            [x,y] = meshgrid(1:8);                              % Create x and y coordinates for the strings
            hStrings = text(x(:),y(:),tString(:),...            % Plot the strings
                'HorizontalAlignment','center');
            textColors = repmat(All_glm_epsilon(:) > 0.5,1,3)|repmat(All_glm_epsilon(:) < -0.5,1,3);
            set(hStrings,{'Color'},num2cell(~textColors,2));
            print -djpeg Corr_input_glm_epsilon_all;
            
            % epsilon SPM.xX
            clear tString
            clear x
            clear y
            clear hStrings
            clear textColors
            
            c_spmxX_epsilon.r{id} = cat(3,c_spmxX_epsilon.ssl(id).r);
            c_spmxX_epsilon.dr   = cat(3,c_spmxX_epsilon.r{:});
            c_spmxX_epsilon.z{id} = cat(3,c_spmxX_epsilon.ssl(id).z);
            c_spmxX_epsilon.dz   = cat(3,c_spmxX_epsilon.z{:});
            
            cd([paths.quality_ssl_first]);
            save('corr_hgf_ssl', '-struct','corr_hgf_ssl');
            %% compute group mean over z-transformed coefficients and transform back
            A_c_spmxX_epsilon.mean_z = mean(c_spmxX_epsilon.dz,3);
            A_c_spmxX_epsilon.mean_r = ifisherz(A_c_spmxX_epsilon.mean_z);
            
            cd([paths.quality_ssl_first]);
            save('A_c_spmxX_epsilon', '-struct','A_c_spmxX_epsilon');
            figure('Color', [1 1 1]);
            
            A_c_spmxX_epsilon_red = A_c_spmxX_epsilon.mean_r(:,:);
            imagesc(A_c_spmxX_epsilon_red,[-1 1]);colorbar;
            set(gca, 'XTick', [1:8], 'XTickLabel', {'CondFace' 'epsilon2 Face' 'epsilon3 Face ' 'epsilon ch Face' 'CondHouse' 'epsilon2 House' 'epsilon3 House ' 'epsilon ch House'});
            set(gca, 'YTick', [1:8], 'YTickLabel', {'CondFace' 'epsilon2 Face' 'epsilon3 Face ' 'epsilon ch Face' 'CondHouse' 'epsilon2 House' 'epsilon3 House ' 'epsilon ch House'});
            ax=gca;
            ax.XTickLabelRotation = 45;
            title('Correlation SPM.xX epsilon');
            tString = num2str(A_c_spmxX_epsilon_red(:), '%0.2f');       % Create strings
            tString = strtrim(cellstr(tString));                        % Remove any space
            [x,y] = meshgrid(1:8);                                      % Create x and y coordinates for the strings
            hStrings = text(x(:),y(:),tString(:),...                    % Plot the strings
                'HorizontalAlignment','center');
            textColors = repmat(A_c_spmxX_epsilon_red(:) > 0.5,1,3)|repmat(A_c_spmxX_epsilon_red(:) < -0.5,1,3);
            set(hStrings,{'Color'},num2cell(~textColors,2));
            print -djpeg Corr_SPMxX_epsilon_all;
            
            % epsilon SPM.xKXs
            clear tString
            clear x
            clear y
            clear hStrings
            clear textColors
            
            c_spmxKXs_epsilon.r{id} = cat(3,c_spmxKXs_epsilon.ssl(id).r);
            c_spmxKXs_epsilon.dr   = cat(3,c_spmxKXs_epsilon.r{:});
            c_spmxKXs_epsilon.z{id} = cat(3,c_spmxKXs_epsilon.ssl(id).z);
            c_spmxKXs_epsilon.dz   = cat(3,c_spmxKXs_epsilon.z{:});
            
            cd([paths.quality_ssl_first]);
            save('corr_hgf_ssl', '-struct','corr_hgf_ssl');
            %% compute group mean over z-transformed coefficients and transform back
            A_c_spmxKXs_epsilon.mean_z = mean(c_spmxKXs_epsilon.dz,3);
            A_c_spmxKXs_epsilon.mean_r = ifisherz(A_c_spmxKXs_epsilon.mean_z);
            
            cd([paths.quality_ssl_first]);
            save('A_c_spmxKXs_epsilon', '-struct','A_c_spmxKXs_epsilon');
            figure('Color', [1 1 1]);
            
            A_c_spmxKXs_epsilon_red = A_c_spmxKXs_epsilon.mean_r(:,:);
            imagesc(A_c_spmxKXs_epsilon_red,[-1 1]);colorbar;
            set(gca, 'XTick', [1:8], 'XTickLabel', {'CondFace' 'epsilon2 Face' 'epsilon3 Face ' 'epsilon ch Face' 'CondHouse' 'epsilon2 House' 'epsilon3 House ' 'epsilon ch House'});
            set(gca, 'YTick', [1:8], 'YTickLabel', {'CondFace' 'epsilon2 Face' 'epsilon3 Face ' 'epsilon ch Face' 'CondHouse' 'epsilon2 House' 'epsilon3 House ' 'epsilon ch House'});
            ax=gca;
            ax.XTickLabelRotation = 45;
            title('Correlation SPM.xKXs epsilon');
            tString = num2str(A_c_spmxKXs_epsilon_red(:), '%0.2f');     % Create strings
            tString = strtrim(cellstr(tString));                        % Remove any space
            [x,y] = meshgrid(1:8);                                      % Create x and y coordinates for the strings
            hStrings = text(x(:),y(:),tString(:),...                    % Plot the strings
                'HorizontalAlignment','center');
            textColors = repmat(A_c_spmxKXs_epsilon_red(:) > 0.5,1,3)|repmat(A_c_spmxKXs_epsilon_red(:) < -0.5,1,3);
            set(hStrings,{'Color'},num2cell(~textColors,2));
            print -djpeg Corr_SPMxKXs_epsilon_all;
            close all;
        elseif g == 2
            
            % da
            clear tString
            clear x
            clear y
            clear hStrings
            clear textColors
            
            corr_hgf_ssl.da.r{id} = cat(3,c_glm_da.ssl(id).r);
            corr_hgf_ssl.da.dr   = cat(3,corr_hgf_ssl.da.r{:});
            corr_hgf_ssl.da.z{id} = cat(3,c_glm_da.ssl(id).z);
            corr_hgf_ssl.da.dz   = cat(3,corr_hgf_ssl.da.z{:});
            
            cd([paths.quality_ssl_first]);
            save('corr_hgf_ssl.mat', '-struct','corr_hgf_ssl');
            %% compute group mean over z-transformed coefficients and transform back
            A_corr_glm_da.mean_z = mean(corr_hgf_ssl.da.dz,3);
            A_corr_glm_da.mean_r = ifisherz(A_corr_glm_da.mean_z);
            
            cd([paths.quality_ssl_first]);
            save('A_corr_glm_da.mat', '-struct','A_corr_glm_da');
            figure('Color', [1 1 1]);
            
            All_glm_da = A_corr_glm_da.mean_r(:,:);
            imagesc(All_glm_da,[-1 1]);colorbar;
            
            set(gca, 'XTick', [1:12], 'XTickLabel', {'CondFace' 'da1 Face' 'da2 Face ' 'da ch Face' 'pw2 Face' 'pw3 Face' 'CondHouse' 'da1 House' 'da2 House ' 'da ch House' 'pw2 House' 'pw3 House'});
            set(gca, 'YTick', [1:12], 'YTickLabel', {'CondFace' 'da1 Face' 'da2 Face ' 'da ch Face' 'pw2 Face' 'pw3 Face' 'CondHouse' 'da1 House' 'da2 House ' 'da ch House' 'pw2 House' 'pw3 House'});
            ax=gca;
            ax.XTickLabelRotation = 45;
            title('Correlation all glm da');
            tString = num2str(All_glm_da(:), '%0.2f');    % Create strings
            tString = strtrim(cellstr(tString));          % Remove any space
            [x,y] = meshgrid(1:12);                       % Create x and y coordinates for the strings
            hStrings = text(x(:),y(:),tString(:),...      % Plot the strings
                'HorizontalAlignment','center');
            textColors = repmat(All_glm_da(:) > 0.5,1,3)|repmat(All_glm_da(:) < -0.5,1,3);
            set(hStrings,{'Color'},num2cell(~textColors,2));
            print -djpeg Corr_input_glm_da_all;
            
            % da SPM.xX
            clear tString
            clear x
            clear y
            clear hStrings
            clear textColors
            
            c_spmxX_da.r{id} = cat(3,c_spmxX_da.ssl(id).r);
            c_spmxX_da.dr   = cat(3,c_spmxX_da.r{:});
            c_spmxX_da.z{id} = cat(3,c_spmxX_da.ssl(id).z);
            c_spmxX_da.dz   = cat(3,c_spmxX_da.z{:});
            
            cd([paths.quality_ssl_first]);
            save('corr_hgf_ssl', '-struct','corr_hgf_ssl');
            %% compute group mean over z-transformed coefficients and transform back
            A_c_spmxX_da.mean_z = mean(c_spmxX_da.dz,3);
            A_c_spmxX_da.mean_r = ifisherz(A_c_spmxX_da.mean_z);
            
            cd([paths.quality_ssl_first]);
            save('A_c_spmxX_da', '-struct','A_c_spmxX_da');
            figure('Color', [1 1 1]);
            
            A_c_spmxX_da_red = A_c_spmxX_da.mean_r(:,:);
            imagesc(A_c_spmxX_da_red,[-1 1]);colorbar;
            set(gca, 'XTick', [1:12], 'XTickLabel', {'CondFace' 'da1 Face' 'da2 Face ' 'da ch Face' 'pw2 Face' 'pw3 Face' 'CondHouse' 'da1 House' 'da2 House ' 'da ch House' 'pw2 House' 'pw3 House'});
            set(gca, 'YTick', [1:12], 'YTickLabel', {'CondFace' 'da1 Face' 'da2 Face ' 'da ch Face' 'pw2 Face' 'pw3 Face' 'CondHouse' 'da1 House' 'da2 House ' 'da ch House' 'pw2 House' 'pw3 House'});
            ax=gca;
            ax.XTickLabelRotation = 45;
            title('Correlation SPM.xX da');
            tString = num2str(A_c_spmxX_da_red(:), '%0.2f');    % Create strings
            tString = strtrim(cellstr(tString));                % Remove any space
            [x,y] = meshgrid(1:12);                             % Create x and y coordinates for the strings
            hStrings = text(x(:),y(:),tString(:),...            % Plot the strings
                'HorizontalAlignment','center');
            textColors = repmat(A_c_spmxX_da_red(:) > 0.5,1,3)|repmat(A_c_spmxX_da_red(:) < -0.5,1,3);
            set(hStrings,{'Color'},num2cell(~textColors,2));
            print -djpeg Corr_SPMxX_da_all;
            
            % da SPM.xKXs
            clear tString
            clear x
            clear y
            clear hStrings
            clear textColors
            
            c_spmxKXs_da.r{id} = cat(3,c_spmxKXs_da.ssl(id).r);
            c_spmxKXs_da.dr   = cat(3,c_spmxKXs_da.r{:});
            c_spmxKXs_da.z{id} = cat(3,c_spmxKXs_da.ssl(id).z);
            c_spmxKXs_da.dz   = cat(3,c_spmxKXs_da.z{:});
            
            cd([paths.quality_ssl_first]);
            save('corr_hgf_ssl', '-struct','corr_hgf_ssl');
            %% compute group mean over z-transformed coefficients and transform back
            A_c_spmxKXs_da.mean_z = mean(c_spmxKXs_da.dz,3);
            A_c_spmxKXs_da.mean_r = ifisherz(A_c_spmxKXs_da.mean_z);
            
            cd([paths.quality_ssl_first]);
            save('A_c_spmxKXs_da', '-struct','A_c_spmxKXs_da');
            figure('Color', [1 1 1]);
            
            A_c_spmxKXs_da_red = A_c_spmxKXs_da.mean_r(:,:);
            imagesc(A_c_spmxKXs_da_red,[-1 1]);colorbar;
            set(gca, 'XTick', [1:12], 'XTickLabel', {'CondFace' 'da1 Face' 'da2 Face ' 'da ch Face' 'pw2 Face' 'pw3 Face' 'CondHouse' 'da1 House' 'da2 House ' 'da ch House' 'pw2 House' 'pw3 House'});
            set(gca, 'YTick', [1:12], 'YTickLabel', {'CondFace' 'da1 Face' 'da2 Face ' 'da ch Face' 'pw2 Face' 'pw3 Face' 'CondHouse' 'da1 House' 'da2 House ' 'da ch House' 'pw2 House' 'pw3 House'});
            ax=gca;
            ax.XTickLabelRotation = 45;
            title('Correlation SPM.xKXs da');
            tString = num2str(A_c_spmxKXs_da_red(:), '%0.2f');    % Create strings
            tString = strtrim(cellstr(tString));                  % Remove any space
            [x,y] = meshgrid(1:12);                               % Create x and y coordinates for the strings
            hStrings = text(x(:),y(:),tString(:),...              % Plot the strings
                'HorizontalAlignment','center');
            textColors = repmat(A_c_spmxKXs_da_red(:) > 0.5,1,3)|repmat(A_c_spmxKXs_da_red(:) < -0.5,1,3);
            set(hStrings,{'Color'},num2cell(~textColors,2));
            print -djpeg Corr_SPMxKXs_da_all;
            close all;
        end
    end
end


