function ADPRSI_antfMRI_ssl_prepare_data( id )
% ADPRSI_antfMRI_ssl_prepare_data: copy data to corresponding 
% "results" folders

%   IN:     id - the subject index

%   OUT:    -

% 21-12-2018; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

%% 1.1. step: copy data 
% download project data from https://www.research-collection.ethz.ch/handle/20.500.11850/454711 
% and move it to a folder called "ADPRSI/antfMRI_ssl/raw/":

%% 1.2. step: from raw folder copy data to corresponding results folder:
% to ensure the project data is not changed, do not run any analyses using
% directly the data from the "raw" folder!

workdir = ADPRSI_antfMRI_ssl_set_workdir;
options = ADPRSI_antfMRI_ssl_set_analysis_options;
[ details, paths ] = ADPRSI_antfMRI_ssl_subjects( id );

% Start diary
diary(paths.logfile_prepare);

% copy behavioural data
disp(['Copying behavioural data in struct ',details.subjname,'.mat, to: ', paths.res_behaviour_ssl]);
copyfile([paths.behaviour_ssl,details.behaviour_ssl],[paths.res_behaviour_ssl,details.behaviour_ssl]);

% copy physiological data
disp(['Copying physiological data to: ', paths.res_physlog_ssl])
copyfile([paths.physlog_ssl,'multiple_regressors_ssl.txt'],[paths.res_physlog_ssl,'multiple_regressors_ssl.txt']);

% copy movement regressors
disp(['Copying movement regressors to: ', paths.res_fMRIssl])
copyfile([paths.fMRIssl,'/rp_fmri_ssl_00001.txt'],paths.res_fMRIssl);

% copy fMRI data:
disp(['Copying fMRI data to: ', paths.res_fMRIssl])
fMRIssl_files = spm_select('List',paths.fMRIssl,'swfmri_ssl_');
fMRIssl_files = cellstr(fMRIssl_files(:,:));
for i = 1:numel(fMRIssl_files)
    fMRIssl_scans(i,1) = fullfile(paths.fMRIssl,fMRIssl_files(i,1));
    copyfile(cell2mat(fMRIssl_scans(i,1)),paths.res_fMRIssl);
end

% copy group data
disp(['Copying questionnaire and genetic data to: ', paths.res_group_ssl_descr]);
copyfile([paths.group_ssl_descr,'ADPRSI_antfMRI_ssl.mat'], [paths.res_group_ssl_descr,'ADPRSI_antfMRI_ssl.mat']);

disp(['Copying mean structural images to: ', paths.res_group_ssl_glm]);
copyfile([paths.group_ssl_glm,'/*.hdr'],paths.res_group_ssl_glm);
copyfile([paths.group_ssl_glm,'/*.img'],paths.res_group_ssl_glm);

end