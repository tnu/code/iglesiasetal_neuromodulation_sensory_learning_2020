function workdir = ADPRSI_antfMRI_ssl_set_workdir(varargin)
% ADPRSI_antfMRI_ssl_set_workdir: set paths to data folder and results
% folder

% 21-12-2018; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% ========================================================================= 

workdir = struct();

% Raw data/original project data store
workdir.datadir = '/ADPRSI/antfMRI_ssl/';

% Where to output results
workdir.resultsdir = '/ADPRSI/antfMRI_ssl/';

% Key code locations
[workdir.code, ~, ~] = fileparts(mfilename('fullpath'));
workdir.spm = fullfile(workdir.code, 'software', 'spm12');

end