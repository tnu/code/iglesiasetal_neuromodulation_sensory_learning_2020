function ADPRSI_antfMRI_ssl_master_4_behav_group
% ADPRSI_antfMRI_ssl_master_4_behav_group function to run analysis steps: 
% 1. check behavioural data (%CR, missed trials, problems with model fit)
% 2. Summary: combine data from all subjects into one structure
% 3. run BMS

%   IN:     -

%   OUT:    -

% 07-01-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

%% Set paths
ADPRSI_antfMRI_ssl_initialise_environment();

%% check behavioural data 
ADPRSI_antfMRI_ssl_check_behaviour_data

%% combine data from all subjects into one structure
ADPRSI_antfMRI_ssl_behaviour_summary

%% run BMS
ADPRSI_antfMRI_ssl_BMS

end
