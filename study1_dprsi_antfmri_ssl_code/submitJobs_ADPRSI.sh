#!/bin/bash

# antfMRI SSL;
# 16-01-2019; Sandra Iglesias
# Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
# This file is released under the terms of the GNU General Public Licence 
# (GPL), version 3. You can redistribute it and/or modify it under the 
# terms of the GPL (either version 3 or, at your option, any later version). 
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details:
# <http://www.gnu.org/licenses/>
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# MATLAB Version: R2016b (9.1.0.441655) 64-bit (glnxa64)
module load matlab/9.1

# To start from raw functional and structural images (raw data), uncomment lines 18 to 25 in ADPRSI_antfMRI_ssl_master_2_preproc_physio (step 2). 
# If you leave those lines commented, you will start from smoothed fMRI data:

# step 1: prepare data and paths
for i in {1..80}; do bsub -R rusage[mem=16000] -R stable -J "job_antfMRI_prepare_ssl$i" -W 0:55 -o o_master1_$i -e e_master1_$i matlab -singleCompThread -r "ADPRSI_antfMRI_ssl_master_1_prepare_data($i)"; done

# step 2: run preprocessing and PhysIO for SSL
for i in {1..80}; do bsub -R rusage[mem=16000] -R stable -J "job_antfMRI_preproc_physio_ssl$i" -W 9:55 -o o_master2_$i -e e_master2_$i -w 'ended("job_antfMRI_prepare_ssl*")' matlab -singleCompThread -r "ADPRSI_antfMRI_ssl_master_2_preproc_physio($i)"; done

# step 3: run behavioural analyses at the single subject level
for i in {1..80}; do bsub -R rusage[mem=16000] -R stable -J "job_antfMRI_hgf_behav_ssl$i" -W 9:55 -o o_master3_$i -e e_master3_$i -w 'ended("job_antfMRI_prepare_ssl*")' matlab -singleCompThread -r "ADPRSI_antfMRI_ssl_master_3_hgf($i)"; done

# step 4: run behavioural analyses at the group level
bsub -R rusage[mem=16000] -R stable -J "job_antfMRI_behav_group_ssl" -W 3:55 -o o_master4 -e e_master4 -w 'ended("job_antfMRI_hgf_behav_ssl*")' matlab -singleCompThread -r "ADPRSI_antfMRI_ssl_master_4_behav_group"

# step 5: run first level analyses
for i in {1..80}; do bsub -R rusage[mem=16000] -R stable -J "job_antfMRI_first_ssl$i" -W 9:55 -o o_master5_$i -e e_master5_$i -w 'ended("job_antfMRI_preproc_physio_ssl*") && ended("job_antfMRI_behav_group_ssl")' matlab -singleCompThread -r "ADPRSI_antfMRI_ssl_master_5_first($i)"; done

# step 6: run second level analyses without SNPs
bsub -R rusage[mem=16000] -R stable -J "job_antfMRI_second_ssl" -W 9:55 -o o_master6 -e e_master6 -w 'ended("job_antfMRI_behav_group_ssl") && ended("job_antfMRI_first_ssl*")' matlab -singleCompThread -r "ADPRSI_antfMRI_ssl_master_6_second"

# step 7: plot correlations between GLM regressors
bsub -R rusage[mem=16000] -R stable -J "job_antfMRI_check_first_ssl" -W 3:55 -o o_master7 -e e_master7 -w 'ended("job_antfMRI_behav_group_ssl") && ended("job_antfMRI_second_ssl")' matlab -singleCompThread -r "ADPRSI_antfMRI_ssl_master_7_check_first"

# step 8: run second level analyses with SNPs
bsub -R rusage[mem=16000] -R stable -J "job_antfMRI_second_snps_ssl" -W 9:55 -o o_master8 -e e_master8 -w 'ended("job_antfMRI_behav_group_ssl") && ended("job_antfMRI_first_ssl*")' matlab -singleCompThread -r "ADPRSI_antfMRI_ssl_master_8_second_snp"

# step 9: plot group level results
bsub -R rusage[mem=16000] -R stable -J "job_antfMRI_second_plots" -W 9:55 -o o_master9 -e e_master9 -w 'ended("job_antfMRI_behav_group_ssl") && ended("job_antfMRI_second_ssl") && ended(job_antfMRI_check_first_ssl) && ended("job_antfMRI_second_snps_ssl")' matlab -singleCompThread -r "ADPRSI_antfMRI_ssl_master_9_second_plots"

# step 10: prepare data for SPSS analyses
bsub -R rusage[mem=16000] -R stable -J "job_antfMRI_stats" -W 9:55 -o o_master10 -e e_master10 -w 'ended("job_antfMRI_behav_group_ssl") && ended("job_antfMRI_second_ssl") && ended(job_antfMRI_check_first_ssl) && ended("job_antfMRI_second_snps_ssl")' matlab -singleCompThread -r "ADPRSI_antfMRI_ssl_master_10_stats"

# step 11: additional analyses
bsub -R rusage[mem=16000] -R stable -J "job_antfMRI_addAnalyses" -W 9:55 -o o_master11 -e e_master11 -w 'ended("job_antfMRI_behav_group_ssl") && ended("job_antfMRI_second_ssl") && ended(job_antfMRI_check_first_ssl) && ended("job_antfMRI_second_snps_ssl")' matlab -singleCompThread -r "ADPRSI_antfMRI_ssl_master_11_addAnalyses"
