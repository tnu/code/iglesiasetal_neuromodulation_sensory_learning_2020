function ADPRSI_antfMRI_ssl_master_3_hgf( id )
% ADPRSI_antfMRI_ssl_master_3_hgf function to run analysis step: 
% 1. run different HGF implementations

%   IN:     id - the subject index

%   OUT:    -

% 07-01-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

%% Set paths
ADPRSI_antfMRI_ssl_initialise_environment();

%% behaviour SSL
for m = 1:3
    ADPRSI_antfMRI_ssl_hgfv1 ( id, m )
end


end
