function ADPRSI_antfMRI_ssl_master_8_second_snp
% ADPRSI_antfMRI_ssl_master_8_second_snp function to run analysis steps:
% 1. run group level analysis for GLM1 and GLM2, with SNPs:
%       - whole brain
%       - across the anatomical mask

%   IN:     -
%   OUT:    -

% 07-01-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

%% Set paths
ADPRSI_antfMRI_ssl_initialise_environment();

%% Secondlevel SSL analyses
for m = 1
    for r = 1:3
        try
            ADPRSI_antfMRI_ssl_secondlevel_cov_COMT( m, r )
        catch e
            fprintf(1,'There was an error! The message was:\n%s',e.message);
            for i = length(e.stack)
                disp(['line: ', num2str(e.stack(i).line),' ',e.stack(i).file])
            end
        end
%         try
%             ADPRSI_antfMRI_ssl_secondlevel_DACh_cov_COMT( m, r )
%         catch e
%             fprintf(1,'There was an error! The message was:\n%s',e.message);
%             for i = length(e.stack)
%                 disp(['line: ', num2str(e.stack(i).line),' ',e.stack(i).file])
%             end
%         end
        try
            ADPRSI_antfMRI_ssl_secondlevel_cov_ChAT( m, r )
        catch e
            fprintf(1,'There was an error! The message was:\n%s',e.message);
            for i = length(e.stack)
                disp(['line: ', num2str(e.stack(i).line),' ',e.stack(i).file])
            end
        end
%         try
%             ADPRSI_antfMRI_ssl_secondlevel_DACh_cov_ChAT( m, r )
%         catch e
%             fprintf(1,'There was an error! The message was:\n%s',e.message);
%             for i = length(e.stack)
%                 disp(['line: ', num2str(e.stack(i).line),' ',e.stack(i).file])
%             end
%         end
    end
end
for m = 2
    for r = 1:5
        try
            ADPRSI_antfMRI_ssl_secondlevel_cov_COMT( m, r )
        catch e
            fprintf(1,'There was an error! The message was:\n%s',e.message);
            for i = length(e.stack)
                disp(['line: ', num2str(e.stack(i).line),' ',e.stack(i).file])
            end
        end
%         try
%             ADPRSI_antfMRI_ssl_secondlevel_DACh_cov_COMT( m, r )
%         catch e
%             fprintf(1,'There was an error! The message was:\n%s',e.message);
%             for i = length(e.stack)
%                 disp(['line: ', num2str(e.stack(i).line),' ',e.stack(i).file])
%             end
%         end
        try
            ADPRSI_antfMRI_ssl_secondlevel_cov_ChAT( m, r )
        catch e
            fprintf(1,'There was an error! The message was:\n%s',e.message);
            for i = length(e.stack)
                disp(['line: ', num2str(e.stack(i).line),' ',e.stack(i).file])
            end
        end
%         try
%             ADPRSI_antfMRI_ssl_secondlevel_DACh_cov_ChAT( m, r )
%         catch e
%             fprintf(1,'There was an error! The message was:\n%s',e.message);
%             for i = length(e.stack)
%                 disp(['line: ', num2str(e.stack(i).line),' ',e.stack(i).file])
%             end
%         end
    end
end

end
