function options = ADPRSI_antfMRI_ssl_set_analysis_options
% ADPRSI_antfMRI_ssl_set_analysis_options; 
% Analysis options function for the ADPRSI project.
% Run this function from the working directory of the analysis pipeline.

%   IN:     -
%   OUT:    options     - a struct with all analysis parameters for the
%                         different steps of the analysis.

% 21-12-2018; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

%% code path and task ----------------------------------------------------%
workdir             = ADPRSI_antfMRI_ssl_set_workdir;
options.maincodedir = workdir.code;
options.workdir     = options.maincodedir;

options.ssl  = 'SSL';
options.part = 'antfMRI_ssl';

%% data paths ------------------------------------------------------------%
options.rawdir          = fullfile(workdir.datadir, 'raw');
options.rawdir_group    = fullfile(options.rawdir, 'group');
options.resultsdir      = fullfile(workdir.resultsdir, 'results_ADPRSI');
if exist(options.resultsdir, 'dir') ~= 7
    mkdir(options.resultsdir);
end

options.group_ssl       = fullfile(options.resultsdir, 'results_group_ssl');

%% subject ---------------------------------------------------------------%
options = ADPRSI_antfMRI_ssl_get_ID_options(options);

%-- preparation ----------------------------------------------------------%
options.prepare.subjectIDs  = options.subjectIDs;

%-- modeling -------------------------------------------------------------%
options.model.subjectIDs    = options.subjectIDs;

%-- preprocessing --------------------------------------------------------%
options.preproc.subjectIDs  = options.subjectIDs;

%-- plot nuisance regressors ---------------------------------------------%
% options:
% templ_meanstruct = use mean structural image as overlay 
%                   (only option possible with shared data)
% templ_wstruct    = use the subject-specific warped structural image
%                   (additional option if analysing own data)
options.nuisance.templ_overlay = 'templ_meanstruct';                                                      

%% stats -----------------------------------------------------------------%
options.stats.subjectIDs    = options.subjectIDs;
options.stats.mode          = 'modelbased';

end

function optionsOut = ADPRSI_antfMRI_ssl_get_ID_options(optionsIn)

optionsOut = optionsIn;

optionsOut.subjectIDs = {'DPRSI_A0101', 'DPRSI_A0102', 'DPRSI_A0103', 'DPRSI_A0104', ...
                         'DPRSI_A0105', 'DPRSI_A0106', 'DPRSI_A0107', 'DPRSI_A0108', ...
                         'DPRSI_A0109', 'DPRSI_A0110', 'DPRSI_A0111', 'DPRSI_A0112', ...
                         'DPRSI_A0113', 'DPRSI_A0114', 'DPRSI_A0115', 'DPRSI_A0116', ...
                         'DPRSI_A0117', 'DPRSI_A0118', 'DPRSI_A0119', 'DPRSI_A0120', ...
                         'DPRSI_A0121', 'DPRSI_A0122', 'DPRSI_A0123', 'DPRSI_A0124', ...
                         'DPRSI_A0125', 'DPRSI_A0126', 'DPRSI_A0127', 'DPRSI_A0128', ...
                         'DPRSI_A0129', 'DPRSI_A0130', 'DPRSI_A0131', 'DPRSI_A0132', ...
                         'DPRSI_A0133', 'DPRSI_A0134', 'DPRSI_A0135', 'DPRSI_A0136', ...
                         'DPRSI_A0137', 'DPRSI_A0138', 'DPRSI_A0139', 'DPRSI_A0140', ...
                         'DPRSI_A0141', 'DPRSI_A0142', 'DPRSI_A0144', 'DPRSI_A0145', ...
                         'DPRSI_A0146', 'DPRSI_A0147', 'DPRSI_A0148', 'DPRSI_A0149', ...
                         'DPRSI_A0150', 'DPRSI_A0151', 'DPRSI_A0152', 'DPRSI_A0153', ...
                         'DPRSI_A0154', 'DPRSI_A0155', 'DPRSI_A0156', 'DPRSI_A0157', ...
                         'DPRSI_A0158', 'DPRSI_A0159', 'DPRSI_A0160', 'DPRSI_A0161', ...
                         'DPRSI_A0162', 'DPRSI_A0163', 'DPRSI_A0164', 'DPRSI_A0165', ...
                         'DPRSI_A0166', 'DPRSI_A0167', 'DPRSI_A0168', 'DPRSI_A0169', ...
                         'DPRSI_A0170', 'DPRSI_A0171', 'DPRSI_A0172', 'DPRSI_A0173', ...
                         'DPRSI_A0174', 'DPRSI_A0175', 'DPRSI_A0176', 'DPRSI_A0177', ...
                         'DPRSI_A0178', 'DPRSI_A0179', 'DPRSI_A0180', 'DPRSI_A0181'};

end