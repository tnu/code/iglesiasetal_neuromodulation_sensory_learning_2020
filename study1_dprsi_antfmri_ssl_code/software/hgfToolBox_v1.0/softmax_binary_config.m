function c = softmax_binary_config
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Contains the configuration for the softmax observation model for binary responses
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% The binary softmax function is the logistic sigmoid
%
% f(x) = 1/(1+exp(-beta*(v1-v0)))
%
% where v1 and v0 are the values of options 1 and 0, respectively, and beta > 0 is a parameter that
% determines the slope of the sigmoid. Beta is sometimes referred to as the (inverse) decision
% temperature. In the formulation above, it represents the probability of choosing option 1.
% Reversing the roles of v1 and v0 yields the probability of choosing option 0.
%
% Beta can be interpreted as inverse decision noise. To have a shrinkage prior on this, choose a
% high value. It is estimated log-space since it has a natural lower bound at zero.
%
% In general, v1 and v0 can be any real numbers. If, however, this observation model is only given
% v1 by the perceptual model in the vector infStates, and v1 is bounded inside the unit interval, it
% assumes that v0 = 1-v1. This is useful in cases where not only the responses but also the inputs
% are binary. The values of these outcomes can then be normalized to 1 and 0, and the perceptual
% model now only needs to track one quantity: the probability of input 1, which remains inside the
% unit interval.
%
% This observation model takes v1 as the first column and v0 as the second column of infStates.
%
% --------------------------------------------------------------------------------------------------
% Copyright (C) 2012 Christoph Mathys, TNU, UZH & ETHZ
%
% This file is part of the HGF toolbox, which is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). For further details, see the file
% COPYING or <http://www.gnu.org/licenses/>.

% Config structure
c = struct;

% Model name
c.model = 'softmax_binary';

% Sufficient statistics of Gaussian parameter priors

% Beta
c.logbemu = log(48);
c.logbesa = 1;

% Gather prior settings in vectors
c.priormus = [
    c.logbemu,...
         ];

c.priorsas = [
    c.logbesa,...
         ];

% Model filehandle
c.obs_fun = @softmax_binary;

% Handle to function that transforms observation parameters to their native space
% from the space they are estimated in
c.transp_obs_fun = @softmax_binary_transp;

return;
