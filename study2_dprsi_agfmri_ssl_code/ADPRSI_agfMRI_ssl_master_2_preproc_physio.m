function ADPRSI_agfMRI_ssl_master_2_preproc_physio( id )
% ADPRSI_agfMRI_ssl_master_2_preproc_physio function to run analysis steps: 
% 1. preprocessing
% 2. Checkreg co-registration
% 3. PhysIO
% 4. Check PhysIO 

%   IN:     id - the subject index

%   OUT:    -

% 01-11-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

%% Set paths
ADPRSI_agfMRI_ssl_initialise_environment();

% %% preprocessing
% ADPRSI_agfMRI_ssl_preprocessing( id )
% disp('preprocessing finished')
% ADPRSI_agfMRI_ssl_checkreg_contour( id )
% disp('checkreg finished')
% %% PhysIO
% ADPRSI_agfMRI_ssl_physio ( id )
% disp('physio ssl finished')
%% plot physio checks
ADPRSI_agfMRI_ssl_check_physio ( id )
disp('physio ssl check finished')
end
