function ADPRSI_agfMRI_ssl_initialise_environment(varargin)
%ADPRSI_agfMRI_ssl_initialise_environment: set all relevant paths

% 24-02-2020; Sam Harrison
% =========================================================================
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

restoredefaultpath();

% Where are we?
[code_dir, ~, ~] = fileparts(mfilename('fullpath'));
addpath(code_dir);

% Local paths
addpath(fullfile(code_dir, 'steps', 'addAnalyses'))
addpath(fullfile(code_dir, 'steps', 'behaviour'))
addpath(fullfile(code_dir, 'steps', 'firstlevel'))
addpath(fullfile(code_dir, 'steps', 'helper'))
% addpath(fullfile(code_dir, 'steps', 'mask'))
addpath(fullfile(code_dir, 'steps', 'prepare'))
addpath(fullfile(code_dir, 'steps', 'preprocessing'))
addpath(fullfile(code_dir, 'steps', 'physio'))
addpath(fullfile(code_dir, 'steps', 'secondlevel'))
addpath(fullfile(code_dir, 'steps', 'simulations'))

% Local software
addpath(fullfile(code_dir, 'software', 'hgfToolBox_v1.0'))

% Start SPM
spm_dir = fullfile(code_dir, 'software', 'spm12');
addpath(spm_dir);
spm('defaults', 'fmri');
spm_jobman('initcfg');

% https://en.wikibooks.org/wiki/SPM/Faster_SPM
spm_get_defaults('stats.maxmem', 4 * 2^30); % 4 GB
spm_get_defaults('stats.resmem', true);
fprintf('SPM initialised.\n');
fprintf('Location: %s\n', which('spm'));
fprintf('Version:  %s\n', spm('version'));
fprintf('\n');

% Start TAPAS 
addpath(fullfile(code_dir, 'software', 'PhysIO', 'code'))
tapas_physio_init();
fprintf('PhysIO initialised.\n');
fprintf('Location: %s\n', which('tapas_physio_version'));
fprintf('Version:  %s\n', tapas_physio_version());
fprintf('\n');

disp('*********************************')
disp('paths set')
disp('*********************************')

end
