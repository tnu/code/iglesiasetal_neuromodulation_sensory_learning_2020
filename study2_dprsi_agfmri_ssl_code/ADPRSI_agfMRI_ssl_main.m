function ADPRSI_agfMRI_ssl_main
% Run entire analysis pipeline for study 2: 
% Run this function from the working directory of the analysis pipeline.

%   IN:     -
%   OUT:    -

% 16-01-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================
% 
%% Set paths
[code_dir, ~, ~] = fileparts(mfilename('fullpath'));
ADPRSI_agfMRI_ssl_initialise_environment();

% To start from raw functional and structural images (raw data), uncomment lines 18 to 25 in ADPRSI_agfMRI_ssl_master_2_preproc_physio (step 2). 
% If you leave those lines commented, you will start from smoothed fMRI data:

% step 1: prepare data and paths
for i = 1:78; ADPRSI_agfMRI_ssl_master_1_prepare_data(i); cd (code_dir); end

% step 2: run preprocessing and PhysIO for SSL
for i = 1:78; ADPRSI_agfMRI_ssl_master_2_preproc_physio(i); close all; cd (code_dir); end

% step 3: run behavioural analyses at the single subject level
for i = 1:78; ADPRSI_agfMRI_ssl_master_3_hgf(i); close all; cd (code_dir); end

% step 4: run behavioural analyses at the group level
ADPRSI_agfMRI_ssl_master_4_behav_group; cd (code_dir);

% step 5: run first level analyses
for i = 1:78; ADPRSI_agfMRI_ssl_master_5_first(i); close all; cd (code_dir); end

% step 6: run second level analyses without SNPs
ADPRSI_agfMRI_ssl_master_6_second; cd (code_dir);

% step 7: plot correlations between GLM regressors
ADPRSI_agfMRI_ssl_master_7_check_first; cd (code_dir);

% step 8: run second level analyses with SNPs
ADPRSI_agfMRI_ssl_master_8_second_snp; cd (code_dir);

% step 9: plot group level results
ADPRSI_agfMRI_ssl_master_9_second_plots; cd (code_dir);

% step 10: prepare data for SPSS analyses
ADPRSI_agfMRI_ssl_master_10_stats; cd (code_dir);

% step 11: additional analyses
ADPRSI_agfMRI_ssl_master_11_addAnalyses; cd (code_dir);
close all;
end
