README file for the analyses performed in:

Iglesias S, Kasper L, Harrison SJ, Manka R, Mathys C, & Stephan KE (2020). 
Cholinergic and dopaminergic effects on prediction error and uncertainty 
responses during sensory associative learning. *NeuroImage*, 117590
===========================================================================
|                               |                                         |
| ----------------------------- | ----------------------------------------|
|Author:                        |Sandra Iglesias                          |
|Created:                       |December 2020                            |
|License:                       |GNU General Public License               |

This software is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details: http://www.gnu.org/licenses/

***************************************************************************

This README contains information on the analysis pipeline for study 2 published in the paper "Cholinergic and dopaminergic effects on prediction error and uncertainty 
responses during sensory associative learning". The repository is meant to provide all code utilised for data analysis.

**Note:** 
- Changes to the code may be necessary if a different MATLAB, HGF or SPM version is used and/or if analyses are performed on a local computer or on a cluster.
- the following parts are not included: 
    - basal forebrain and SN/VTA anatomical masks (the relevant references are listed in the paper).
    - Stimuli of the sensory learning task (the learning trajectory is stored as part of the behavioural data).
    - Software:
        - MATLAB R2016b
        - SPM12 r7487 (https://www.fil.ion.ucl.ac.uk/spm/software/spm12/)
        - IBM SPSS Statistics Version 23.0
        - JASP Version 0.13.1; JASP Team, 2020

**Important**
To reproduce the results, you need to use the same MATLAB, SPM, HGF and PhysIO versions. 
However, small numerical errors might still occur.


# Contributors / Roles
|                               |                                             |
| ----------------------------- | ------------------------------------------- |
| Project lead / analysis:      | Sandra Iglesias (PhD)                       |
| Supervising Prof.:            | Prof. Klaas Enno Stephan (MD Dr. med., PhD) |
| Abbreviation:                 | ADPRSI (study acronym)                      |
| Date:                         | December, 2020                              |

The project was conducted at the Translational Neuromodeling Unit (TNU).

# Reference
Iglesias S, Kasper L, Harrison SJ, Manka R, Mathys C, & Stephan KE (2020). 
Cholinergic and dopaminergic effects on prediction error and uncertainty 
responses during sensory associative learning. *NeuroImage*, 117590



# Data Information

- fMRI study2 (enhancing drugs): 
- subject IDs: DPRSI_A0201-DPRSI_A0281 
- Missing data: claustrophobia, nausea, no data
    - DPRSI_A0206,
    - DPRSI_A0248,
    - DPRSI_A0263.
- Exclusion of subjects: The following participants have been excluded from the 
  group glm analyses due to too many missed trials (>48) or less than 65% correct responses in the SSL task:
    - DPRSI_A0215,
    - DPRSI_A0219,
    - DPRSI_A0224,
    - DPRSI_A0229,
    - DPRSI_A0279.
- Exclusion of subject due to problems with model-fitting
    - DPRSI_A0258.
- Exclusion of subjects due to dropouts in the ROIs:
    - DPRSI_A0227.
- Exclusion of subjects due to large movement artefacts:
    - DPRSI_A0231,
    - DPRSI_A0275.


# Software used:

- local operations: copy/reorder files:
    - MATLAB R2019a (local steps; software not included!)
- Cluster analyses: run HGF/SPM analyses
    - MATLAB, R2016b (on cluster; software not included!)
- other toolboxes (subfolder `software/`):
    - HGF: hgfToolBox_v1.0 (software included)
    - PhysIO: R2019b-v7.2.1 (software included; copy the PhysIO folder into the spm12/toolbox folder)
    - spm12: r7487 (software not included!)
    - IBM SPSS Statistics Version 23.0 (software not included!)
    - JASP Version 0.13.1; JASP Team, 2020 (software not included!)


# Information Code

Structure of the code follows a prvious example by Lilian Weber (many thanks Lilian!).

## Paths and Environment
----------------------
- all paths have been specified here: 
    - `ADPRSI_agfMRI_ssl_subjects.m`
    - `ADPRSI_agfMRI_ssl_set_workdir.m`
- the options have been specified here:
    - `ADPRSI_agfMRI_ssl_set_analysis_options.m`


## Analysis
--------

In order to perform the analysis, you will have to retrieve and adapt  
the analysis code (git repository) and the project data. 

### Prepare Analysis Code
[code_iglesiasetal_neuromodulation_sensory_learning_2020](https://gitlab.ethz.ch/tnu/code/iglesiasetal_neuromodulation_sensory_learning_2020)

- *Change all paths in*  
    - <code>ADPRSI_agfMRI_ssl_subjects.m</code>
    - <code>ADPRSI_agfMRI_ssl_set_workdir.m</code>

### Starting from preprocessed nifti data 
[data_iglesiasetal_neuromodulation_sensory_learning_2020](https://www.research-collection.ethz.ch/handle/20.500.11850/454711)

- from here, analyses can be performed completely automatically by running the shell script `submitJobs_ADPRSI.sh` or the MATLAB function ADPRSI_agfMRI_ssl_main.m.
- Please note that this code has been adapted to run the analysis pipeline starting from the preprocessed project data:
    - Therefore, lines 15 to 25 in script <code>ADPRSI_agfMRI_ssl_master_2_preproc_physio</code> have been commented out.
- Furthermore, additional parts of the code have been commented out for the following reasons (for more details see Section *Description of full analysis pipeline*):
    - the raw fMRI or subject-specific structural images could not be shared to prevent identification.
    - the anatomical mask could not be shared, because we do not have permission.

Nevertheless, we provide all steps performed.


# Description of full analysis pipeline
-------------------------------------

Steps 1.3 to 2.3 contain the preprocessing of the fMRI and physiological data. 
These steps cannot be run using the project data, but they have been included for completeness. 

**********************************************************************

1. download data from https://www.research-collection.ethz.ch/handle/20.500.11850/454711 and move it to a folder called "ADPRSI/agfMRI_ssl/raw/":

1.1 specify paths in: 
    - <code>ADPRSI_agfMRI_ssl_subjects.m</code>
    - <code>ADPRSI_agfMRI_ssl_set_workdir.m</code>

1.2 sort/copy data to the corresponding folders:
<code>ADPRSI_agfMRI_ssl_master_1_prepare</code>
<code>ADPRSI_agfMRI_ssl_prepare_data</code>

*results saved to: /ADPRSI/agfMRI_ssl/results/

1.3 step - manual step; if you are working with your own data:
- manually reorient all functional and structural images. 
        
> ***Importantly***: save the reorientation matrix - this allows you to rerun the reorientation using a batch or script in case the preprocessing step fails.

**********************************************************************
2. step: <code>ADPRSI_agfMRI_ssl_master_2_preproc_physio</code>
the following lines have been commented out:
- 18 - 25: the raw fMRI and structural images (needed for preprocessing) and the images y_struct.nii, c2struct.nii and c3struct.nii (needed for PhysIO) are not included.

The master function calls the following functions:

2.1. preprocessing:
(commented out)
<code>ADPRSI_agfMRI_ssl_preprocessing.m</code>

 *results saved to: /ADPRSI/agfMRI_ssl/results/results_fMRI_ssl/SUBJ/scandata*

2.2. check co-registration between functional and structural scans; 
(commented out: this step needs the anatomical mask of the midbrain/basal forebrain and PPT/LDT which is not included in this pipeline)
check whether subjects have dropouts in the ROIs (midbrain, basal forebrain and/or PPT/LDT)
<code>ADPRSI_agfMRI_ssl_checkreg_contour.m</code>

 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/quality/prep*

2.3. PhysIO
<code>ADPRSI_agfMRI_ssl_physio.m</code>:
(commented out)
- <code>ADPRSI_physio_PCA_batch.m</code>
- <code>ADPRSI_physio_onlyresp_PCA_batch.m</code>

 *results saved to: /ADPRSI/agfMRI_ssl/results/results_phys_ssl/SUBJ/physlog/ssl*

2.4. plot design matrix of physiological and movement artefact regressors
<code>ADPRSI_agfMRI_check_physio_ssl.m</code>

 *saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/quality/physio/*

**********************************************************************
3. step: <code>ADPRSI_agfMRI_ssl_master_3_hgf</code>

3.1. run 3-level HGF, 2-level HGF, and RW model:
<code>ADPRSI_agfMRI_ssl_hgfv1</code>

 *results saved to: /ADPRSI/agfMRI_ssl/results/results_behav_ssl/SUBJ/behavior/ssl/*
 *trajectories saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/quality/behav/HGF/..*

**********************************************************************
4. step: <code>ADPRSI_agfMRI_ssl_master_4_behav_group</code>

4.1. check behavioural data (e.g. %correct responses (CR), number of irregular trials, problems with model fit)
<code>ADPRSI_agfMRI_ssl_check_behaviour_data.m</code>

 *plots saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/quality/behav/check_behav2_behav_ssl.tif*    
 *plots saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/quality/behav/check_behav2_hgfv1_ssl.tif*   
 *plots saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/quality/behav/check_behav2_rw_sutton_ssl.tif*   

4.2. summarise reaction times, %CR and model parameters of all subjects into one structure
<code>ADPRSI_agfMRI_ssl_behaviour_summary.m</code>
 
 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/behav/model/*
    
4.3. run BMS for the 3-level HGF, 2-level HGF, and RW model:
<code>ADPRSI_agfMRI_ssl_BMS.m</code>

 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/behav/bms/*

**********************************************************************
5. step: <code>ADPRSI_agfMRI_master_5_first_ssl</code>
the following lines have been commented out:
- 35: the anatomical mask is not included

5.1. write out regressors for the first level GLM
<code>ADPRSI_agfMRI_ssl_glmInput.m</code>

 *results saved to: /ADPRSI/agfMRI_ssl/results/results_first_ssl/SUBJ/glm/ssl*

5.2. 1stlevel analyses for GLM containing epsilons (GLM1 - epsilon2, epsilon3 and epsilon_ch)
<code>ADPRSI_agfMRI_ssl_firstlevel_epsilon.m</code>

 *results saved to: /ADPRSI/agfMRI_ssl/results/results_first_ssl/SUBJ/glm/ssl/fixom_bin3_spm12*

5.3. 1stlevel analyses for GLM containing da and precision-weights (GLM2 - da1, da2, da_ch, pw2, pw3)
<code>ADPRSI_agfMRI_ssl_firstlevel_da.m</code>
    
 *results saved to: /ADPRSI/agfMRI_ssl/results/results_first_ssl/SUBJ/glm/ssl/fixom_bin5_spm12*

5.4. plot physiological noise and movement contrasts for GLM1
<code>ADPRSI_agfMRI_ssl_nuisance_epsilon.m</code>

 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/quality/prep/*
	
5.5. plot physiological noise and movement contrasts for GLM2
<code>ADPRSI_agfMRI_ssl_nuisance_da.m</code>

 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/quality/prep/*

5.6 plot anatomical mask over mask.nii images
(commented out)
<code>ADPRSI_agfMRI_ssl_checkreg_mask</code>

 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/quality/first/*

**********************************************************************

6. step: <code>ADPRSI_agfMRI_ssl_master_6_second</code>
the following lines have been commented out:
- 19: this step requires the warped fMRI images, however, only the smoothed 
      fMRI data has been shared
- 32-39 & 52-59:  the anatomical mask is not included

6.1. compute average structural image across all subjects 
(commented out)
<code>ADPRSI_agfMRI_ssl_imCalc_meanstruct.m</code>
 
 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/glm*

6.2. run group level analysis for GLM1 and GLM2, without SNPs:

*whole brain:*
<code>ADPRSI_agfMRI_ssl_secondlevel.m</code>

 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/glm/sslbin/fixom_bin3_spm12_fact/*
 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/glm/sslbin/fixom_bin5_spm12_fact/*
        
*anatomical mask:*
(commented out)
<code>ADPRSI_agfMRI_ssl_secondlevel_DACh.m</code>

 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/glm/sslbin/mask/fixom_bin3_spm12_fact/*
 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/glm/sslbin/mask/fixom_bin5_spm12_fact/*

**********************************************************************
7. step: <code>ADPRSI_agfMRI_ssl_master_7_check_first</code>
    
7.1. compute correlation between model parameters and between GLM regressors:
<code>ADPRSI_agfMRI_ssl_compute_corr_hgf_trajectories.m</code>

 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/quality/behav/HGF/HGF_fixom_v1/*
 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/quality/first/*
    
7.2. plot da2 trajectories using different values of theta
<code>ADPRSI_agfMRI_ssl_hgfv1_plot_traj_diffthetas.m</code>

 *saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/quality/behav/HGF/theta_traj/*

7.3 run simulations/ parameter recovery:
<code>ADPRSI_agfMRI_ssl_hgfv1_sim_post.m</code>

 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/behav/simulations/*

**********************************************************************
8. step: <code>ADPRSI_agfMRI_ssl_master_8_second_snp</code>
the following lines have been commented out:
- 27-34, 43-50, 63-70, 79-86: the anatomical mask is not included

8.1. run group level analysis for GLM1 and GLM2, with SNPs:

*whole brain:*
<code>ADPRSI_agfMRI_ssl_secondlevel_cov_COMT.m</code>
<code>ADPRSI_agfMRI_ssl_secondlevel_cov_ChAT.m</code>

 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/glm/sslbin/fixom_bin3_chat_spm12_fact/*
 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/glm/sslbin/fixom_bin3_comt_spm12_fact/*
 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/glm/sslbin/fixom_bin5_chat_spm12_fact/*
 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/glm/sslbin/fixom_bin5_comt_spm12_fact/*
         
*anatomical mask:*
(commented out)
<code>ADPRSI_agfMRI_ssl_secondlevel_DACh_cov_COMT.m</code>
<code>ADPRSI_agfMRI_ssl_secondlevel_DACh_cov_ChAT.m</code>

 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/glm/sslbin/fixom_bin3_chat_spm12_fact/mask/*
 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/glm/sslbin/fixom_bin3_comt_spm12_fact/mask/*
 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/glm/sslbin/fixom_bin5_chat_spm12_fact/mask/*
 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/glm/sslbin/fixom_bin5_comt_spm12_fact/mask/*

**********************************************************************
9. step: <code>ADPRSI_agfMRI_ssl_master_9_second_plots</code>
the following lines have been commented out:
- 25-32 & 47-58:  the anatomical mask is not included

9.1. plot significant second level results (without factor SNPs):

*whole brain:*
<code>ADPRSI_agfMRI_ssl_secondlevel_significant_results.m</code>
<code>ADPRSI_agfMRI_ssl_plot_main_effects_eigenvariate.m</code>

 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/glm/sslbin/fixom_bin3_spm12_fact/GLM_FOLDER/plots_sign_results/*
 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/glm/sslbin/fixom_bin5_spm12_fact/GLM_FOLDER/plots_sign_results/*
        
*anatomical mask:*
(commented out)
<code>ADPRSI_agfMRI_ssl_secondlevel_significant_results_mask.m</code>
<code>ADPRSI_agfMRI_ssl_plot_main_effects_eigenvariate.m</code>

 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/glm/sslbin/mask/fixom_bin3_spm12_fact/mask/GLM_FOLDER/plots_sign_results/*
 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/glm/sslbin/mask/fixom_bin5_spm12_fact/mask/GLM_FOLDER/plots_sign_results/*

**********************************************************************
10. step: <code>ADPRSI_agfMRI_ssl_master_10_stats</code>

10.1 prepare data for SPSS analyses
<code>ADPRSI_agfMRI_ssl_prepare_SPSS.m</code>

 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/behav/SPSS/*
   
**********************************************************************
11. step: <code>ADPRSI_agfMRI_ssl_master_11_addAnalyses</code>
additional analyses based on reviewer comments:

the following lines have been commented out:
- 31-59: the anatomical mask is not included

11.1 summarise behavioural data (adjusted %CR, %CR, and RT) blockwise
<code>ADPRSI_agfMRI_ssl_behav_blockwise.m</code>

 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/behav/SPSS/*

11.2 compute correlations between epsilon (2/3/ch) and da (1/2/ch)
<code>ADPRSI_agfMRI_ssl_corr_eps_da.m</code>

 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/quality/first/*

11.3 plot %CR per trial across all participants
<code>ADPRSI_agfMRI_ssl_behav_change_strtg.m</code>

 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/behav/sum_correct_choices/*

11.4 extract eigenvariates
(commented out)
<code>ADPRSI_agfMRI_ssl_extract_eigenv.m</code>

 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/glm/sslbin/fixom_bin3_spm12_fact/mask/*
 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/glm/sslbin/fixom_bin5_spm12_fact/mask/*

11.5 summarise eigenvariates
(commented out)
<code>ADPRSI_agfMRI_ssl_sum_eigenv.m</code>

 *results saved to: /ADPRSI/agfMRI_ssl/results/results_group_ssl/glm/sslbin/JASP/*

**********************************************************************
