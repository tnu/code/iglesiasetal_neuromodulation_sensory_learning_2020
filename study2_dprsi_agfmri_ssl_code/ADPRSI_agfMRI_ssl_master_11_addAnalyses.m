function ADPRSI_agfMRI_ssl_master_11_addAnalyses
% ADPRSI_agfMRI_ssl_master_11_addAnalyses function to run the following 
% additional analyses based on reviewer comments:
% 1. ADPRSI_agfMRI_ssl_behav_blockwise: summarise behavioural data
% (adjusted %CR, %CR, and RT) blockwise
% 2. ADPRSI_agfMRI_ssl_corr_eps_da: compute correlations between pwPEs and
% PEs
% 3. ADPRSI_agfMRI_ssl_behav_change_strtg: plot %CR per trial across all 
% participants
% 4. ADPRSI_agfMRI_ssl_extract_eigenv - JASP analyses: 
% extract for every computational regressor the first eigenvariate 
% (restricted to each DA and ACh region, separately) from the corresponding 
% second-level design.
% 5. ADPRSI_agfMRI_ssl_sum_eigenv - JASP analyses: summarise in a struct 
% the extracted VOI for every computational regressor.

%   IN:     -

%   OUT:    -

% 28-07-2020; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

%% Set paths
ADPRSI_agfMRI_ssl_initialise_environment();

%% sumarise behavioural results
ADPRSI_agfMRI_ssl_behav_blockwise

%% compute correlations between epsilon (2/3/ch) and da (1/2/ch)
ADPRSI_agfMRI_ssl_corr_eps_da;

%% plot %CR per trial across all participants
ADPRSI_agfMRI_ssl_behav_change_strtg

% %% extract eigenvariates
% for m = 1 % (GLM1)
%     for c = 1:2
%         for r = 1:3
%             ADPRSI_agfMRI_ssl_extract_eigenv(m,r,c)
%         end
%     end
% end
% 
% for m = 2 % (GLM2)
%     for c = 1:2
%         for r = [1,2,3,5]
%             ADPRSI_agfMRI_ssl_extract_eigenv(m,r,c)
%         end
%     end
% end
% 
% %% summarise eigenvariates
% for m = 1 % (GLM1)
%     for r = 1:3
%         ADPRSI_agfMRI_ssl_sum_eigenv(m,r)
%     end
% end
% 
% for m = 2 % (GLM2)
%     for r = [1,2,3,5]
%         ADPRSI_agfMRI_ssl_sum_eigenv(m,r)
%     end
% end
end
