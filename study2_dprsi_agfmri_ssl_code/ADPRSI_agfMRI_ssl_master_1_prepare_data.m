function ADPRSI_agfMRI_ssl_master_1_prepare_data ( id )
% ADPRSI_agfMRI_ssl_master_1_prepare_data function to run analysis step: 
% 1. copy data to results folder

%   IN:     -
%   OUT:    -

% 01-11-2019; Sandra Iglesias
% =========================================================================
%
%% Set paths
ADPRSI_agfMRI_ssl_initialise_environment();

%% prepare data
ADPRSI_agfMRI_ssl_prepare_data ( id )

end
