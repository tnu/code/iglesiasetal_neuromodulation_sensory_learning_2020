function fit_plotCorr(r)
% Plots the posterior correlation matrix of the model parameters estimated by fitModel.
% This is estimated by calculating the Hessian at the MAP estimate. The negative inverse of the
% Hessian is the parameter covariance, which is standardized to yield the correlation.
% Usage:  est = fitModel(responses, inputs); fit_plotCorr(est);
%
% --------------------------------------------------------------------------------------------------
% Copyright (C) 2012 Christoph Mathys, TNU, UZH & ETHZ
%
% This file is part of the HGF toolbox, which is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). For further details, see the file
% COPYING or <http://www.gnu.org/licenses/>.

% Set up display
scrsz = get(0,'screenSize');
outerpos = [0*scrsz(3),0.4*scrsz(4),0.6*scrsz(4),0.6*scrsz(4)];
figure(...
    'OuterPosition', outerpos,...
    'Name','Posterior parameter correlation matrix');

% Determine indices of parameters to optimize (i.e., those that are not fixed)
ind_prc = find(r.c_prc.priorsas);
ind_obs = find(r.c_obs.priorsas);
n_par   = length(ind_prc) + length(ind_obs);

% Find names of optimized parameters to use them as tick labels 
names_prc = fieldnames(r.p_prc);
fields = struct2cell(r.p_prc);
expnms_prc = [];
for k = 1:length(names_prc)
    for l= 1:length(fields{k})
    expnms_prc = [expnms_prc, names_prc(k)];
    end
end
expnms_prc = expnms_prc(1:length(r.p_prc.p))';

names_obs = fieldnames(r.p_obs);
fields = struct2cell(r.p_obs);
expnms_obs = [];
for k = 1:length(names_obs)
    for l= 1:length(fields{k})
    expnms_obs = [expnms_obs, names_obs(k)];
    end
end
expnms_obs = expnms_obs(1:length(r.p_obs.p))';

ticklabels = {[expnms_prc(ind_prc); expnms_obs(ind_obs)]};

% Plot matrix
imagesc(r.optim.Corr, [-1 1]);
axis('square');
set(gca,'xtick',1:n_par);
set(gca,'ytick',1:n_par);
set(gca,'xticklabel',ticklabels{:});
set(gca,'yticklabel',ticklabels{:});
colorbar;
title('Parameter correlation', 'FontSize', 15, 'FontWeight', 'bold');
