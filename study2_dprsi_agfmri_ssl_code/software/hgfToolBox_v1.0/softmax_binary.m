function logp = softmax_binary(r, infStates, ptrans)
% Calculates the log-probability of response y=1 under the binary softmax model
%
% --------------------------------------------------------------------------------------------------
% Copyright (C) 2012 Christoph Mathys, TNU, UZH & ETHZ
%
% This file is part of the HGF toolbox, which is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). For further details, see the file
% COPYING or <http://www.gnu.org/licenses/>.

% Transform zeta to its native space
be = exp(ptrans(1));

% Initialize returned log-probabilities as NaNs so that NaN is
% returned for all irregualar trials
logp = NaN(length(infStates),1);

% Weed irregular trials out from inferred states and responses
x = infStates;
x(r.irr) = [];
y = r.y(:,1);
y(r.irr) = [];

if size(x,2) == 1
    if ~any(x<0) && ~any(x>1)
        % Calculate log-probabilities for non-irregular trials
        logp(not(ismember(1:length(logp),r.irr))) = -log(1+exp(-be.*(2.*x-1).*(2.*y-1)));
    else
        error('infStates incompatible with softmax_binary observation model.')
    end
else
    % Calculate log-probabilities for non-irregular trials
    logp(not(ismember(1:length(logp),r.irr))) = -log(1+exp(-be.*(x(:,1)-x(:,2)).*(2.*y-1)));
end    

return;
