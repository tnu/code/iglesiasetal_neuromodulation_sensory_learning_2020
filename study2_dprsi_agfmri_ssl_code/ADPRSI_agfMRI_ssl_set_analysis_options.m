function options = ADPRSI_agfMRI_ssl_set_analysis_options
% ADPRSI_agfMRI_ssl_set_analysis_options
% Analysis options function for the ADPRSI project.
% Run this function from the working directory of the analysis pipeline.

%   IN:     -
%   OUT:    options     - a struct with all analysis parameters for the
%                       different steps of the analysis.

% 01-11-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

%% code path and task ----------------------------------------------------%
workdir             = ADPRSI_agfMRI_ssl_set_workdir;
options.maincodedir = workdir.code;
options.workdir     = options.maincodedir;

options.ssl  = 'SSL';
options.part = 'agfMRI_ssl';

%% data paths ------------------------------------------------------------%
options.rawdir          = fullfile(workdir.datadir, 'raw');
options.rawdir_group    = fullfile(options.rawdir, 'group');
options.resultsdir      = fullfile(workdir.resultsdir, 'results_ADPRSI');
if exist(options.resultsdir, 'dir') ~= 7
    mkdir(options.resultsdir);
end

options.group_ssl       = fullfile(options.resultsdir, 'results_group_ssl');

%% subject ---------------------------------------------------------------%
options = ADPRSI_agfMRI_ssl_get_ID_options(options);

%-- preparation ----------------------------------------------------------%
options.prepare.subjectIDs  = options.subjectIDs;

%-- modeling -------------------------------------------------------------%
options.model.subjectIDs    = options.subjectIDs;

%-- preprocessing --------------------------------------------------------%
options.preproc.subjectIDs  = options.subjectIDs;

%-- plot nuisance regressors ---------------------------------------------%
% options:
% templ_meanstruct = use mean structural image as overlay
%                   (only option possible with shared data)
% templ_wstruct    = use the subject-specific warped structural image
%                   (additional option if analysing own data)
options.nuisance.templ_overlay = 'templ_meanstruct';

%% stats -----------------------------------------------------------------%
options.stats.subjectIDs    = options.subjectIDs;
options.stats.mode          = 'modelbased';

end

function optionsOut = ADPRSI_agfMRI_ssl_get_ID_options(optionsIn)

optionsOut = optionsIn;

optionsOut.subjectIDs = {'DPRSI_A0201', 'DPRSI_A0202', 'DPRSI_A0203' ,'DPRSI_A0204', ...
            'DPRSI_A0205', 'DPRSI_A0207', 'DPRSI_A0208', 'DPRSI_A0209', 'DPRSI_A0210', ...
            'DPRSI_A0211', 'DPRSI_A0212', 'DPRSI_A0213', 'DPRSI_A0214', 'DPRSI_A0215', ...
            'DPRSI_A0216', 'DPRSI_A0217', 'DPRSI_A0218', 'DPRSI_A0219', 'DPRSI_A0220', ...
            'DPRSI_A0221', 'DPRSI_A0222', 'DPRSI_A0223', 'DPRSI_A0224', 'DPRSI_A0225', ...
            'DPRSI_A0226', 'DPRSI_A0227', 'DPRSI_A0228', 'DPRSI_A0229', 'DPRSI_A0230', ...
            'DPRSI_A0231', 'DPRSI_A0232', 'DPRSI_A0233', 'DPRSI_A0234', 'DPRSI_A0235', ...
            'DPRSI_A0236', 'DPRSI_A0237', 'DPRSI_A0238', 'DPRSI_A0239', 'DPRSI_A0240', ...
            'DPRSI_A0241', 'DPRSI_A0242', 'DPRSI_A0243', 'DPRSI_A0244', 'DPRSI_A0245', ...
            'DPRSI_A0246', 'DPRSI_A0247', 'DPRSI_A0249', 'DPRSI_A0250', 'DPRSI_A0251', ...
            'DPRSI_A0252', 'DPRSI_A0253', 'DPRSI_A0254', 'DPRSI_A0255', 'DPRSI_A0256', ...
            'DPRSI_A0257', 'DPRSI_A0258', 'DPRSI_A0259', 'DPRSI_A0260', 'DPRSI_A0261', ...
            'DPRSI_A0262', 'DPRSI_A0264', 'DPRSI_A0265', 'DPRSI_A0266', 'DPRSI_A0267', ...  
            'DPRSI_A0268', 'DPRSI_A0269', 'DPRSI_A0270', 'DPRSI_A0271', 'DPRSI_A0272', ...
            'DPRSI_A0273', 'DPRSI_A0274', 'DPRSI_A0275', 'DPRSI_A0276', 'DPRSI_A0277', ...
            'DPRSI_A0278', 'DPRSI_A0279', 'DPRSI_A0280', 'DPRSI_A0281'};
        
optionsOut.noMRI.subjectIDs    = {'DPRSI_A0206', 'DPRSI_A0248', 'DPRSI_A0263'};
optionsOut.noMask.subjectIDs   = {'DPRSI_A0227'};
optionsOut.Movement.subjectIDs = {'DPRSI_A0231', 'DPRSI_A0275'};
end
