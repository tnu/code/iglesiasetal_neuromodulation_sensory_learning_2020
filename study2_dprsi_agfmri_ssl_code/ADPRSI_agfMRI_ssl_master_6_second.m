function ADPRSI_agfMRI_ssl_master_6_second
% ADPRSI_agfMRI_ssl_master_6_second function to run analysis steps:
% 1. compute average structural image across all subjects
% 2. run group level analysis for GLM1 and GLM2, without SNPs:
%       - whole brain
%       - across the anatomical mask

%   IN:     -

%   OUT:    -

% 04-11-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

%% Set paths
ADPRSI_agfMRI_ssl_initialise_environment();

%% compute mean structural image
% used as overlay for visualization of glm results
% ADPRSI_agfMRI_ssl_imCalc_meanstruct
% 
%% Secondlevel SSL analyses
for m = 1
    for r = 1:3
        try
            ADPRSI_agfMRI_ssl_secondlevel( m, r )
        catch e
            fprintf(1,'There was an error! The message was:\n%s',e.message);
            for i = length(e.stack)
                disp(['line: ', num2str(e.stack(i).line),' ',e.stack(i).file])
            end
        end
%         try
%             ADPRSI_agfMRI_ssl_secondlevel_DACh( m, r )
%         catch e
%             fprintf(1,'There was an error! The message was:\n%s',e.message);
%             for i = length(e.stack)
%                 disp(['line: ', num2str(e.stack(i).line),' ',e.stack(i).file])
%             end
%         end
    end
end
for m = 2
    for r = 1:5
        try
            ADPRSI_agfMRI_ssl_secondlevel( m, r )
        catch e
            fprintf(1,'There was an error! The message was:\n%s',e.message);
            for i = length(e.stack)
                disp(['line: ', num2str(e.stack(i).line),' ',e.stack(i).file])
            end
        end
%         try
%             ADPRSI_agfMRI_ssl_secondlevel_DACh( m, r )
%         catch e
%             fprintf(1,'There was an error! The message was:\n%s',e.message);
%             for i = length(e.stack)
%                 disp(['line: ', num2str(e.stack(i).line),' ',e.stack(i).file])
%             end
%         end
    end
end

end
