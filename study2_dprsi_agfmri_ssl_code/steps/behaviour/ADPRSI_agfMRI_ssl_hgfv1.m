function ADPRSI_agfMRI_ssl_hgfv1( id , m )
% ADPRSI_agfMRI_ssl_hgfv1:
% 1. writes the structure SSL that contains 
% - the input data for the behavioural model, 
% - the HGF results.
% 2. plots the HGF/RW state trajectories

%   IN:     id - the subject index
%           m  - model

%   OUT:    -

% 04-11-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

% Set paths
workdir = ADPRSI_agfMRI_ssl_set_workdir;
options = ADPRSI_agfMRI_ssl_set_analysis_options;
[ details, paths ] = ADPRSI_agfMRI_ssl_subjects( id );

% Start diary
diary(paths.logfile_hgf_ssl);

% Initialize
disp(['running hgf for subj: ', details.subjname,' (',num2str(id),')']);

% define models
maskModel = {'/HGF_fixom_v1',  '/HGF_2l_v1', '/RW_v1'};
maskprc = {'hgf_binary_config',  'hgf_binary_config_2l', ...
    'rw_binary_config'};

%% check whether results have been plotted
if exist([paths.res_behaviour_ssl,maskModel{m},'/SSL.VS.mat']) == 2;
    VSmat = load([paths.res_behaviour_ssl,maskModel{m},'/SSL.VS.mat']);
    if strcmp('/RW_v1',maskModel{m})==1
        rw_binary_plotTraj(VSmat.VS.est);
    else
        hgf_binary_plotTraj(VSmat.VS.est);
    end
    cd ([paths.res_behaviour_ssl,maskModel{m}]);
    print -djpeg ssl_traj;
    
    mkdir([paths.quality_ssl_behav,'/HGF/',maskModel{m}]);
    copyfile ('ssl_traj.jpg', [paths.quality_ssl_behav,'/HGF/',maskModel{m},'/ssl_traj_',details.subjname,'.jpg']);
    delete(findall(0,'Type','figure'))
else
    copyfile([paths.behaviour_ssl,details.behaviour_ssl],[paths.res_behaviour_ssl,details.behaviour_ssl]);
    behav = load([paths.res_behaviour_ssl,details.behaviour_ssl]);
    
    % info behav.alldata columns: 
    
    % alldata(1) = typeSound;           % 0 = low tone (352Hz); 1 = high tone (576Hz)
    % alldata(2) = typeTarget;          % 0 = House           ; 1 = Face
    % alldata(3) = ReactionTime;        % reaction time
    % alldata(4) = key_press;           % prediction: 0 = House; 1 = Face
    % alldata(5) = correctness;         % 0 = wrong; 1 = correct
    % alldata(6) = block;               % block index (total = 10 blocks)
    % alldata(7) = time_present_sound;  % time of cue presentation
    % alldata(8) = time_present_target; % time of target presentation
    % alldata(9) = time_press;          % time of response
    % alldata(10)= key_number;          % key ID
    % alldata(11)= TargetImageCodes;    % ID target stimuli: 8 different faces: 1-8; 8 different houses: 17-24
    % alldata(12)= typeProb;            % p(face|high tone) = p(house|low tone)
    % alldata(13)= ProbBlock;           % blockwise probability
    % alldata(14)= time_present_iti;    % time of intertrial interval (ITI)
    % alldata(15)= Times_ITI;           % length of intertrial interval (ITI)
        
    %% prepare data
    SSL.VS.CongrIn = (behav.alldata(1:320,1) == behav.alldata(1:320,2));
    irr = [];
    irr_late = [];
    irr_early = [];
    for l = 1:length(behav.alldata(:,3))
        if behav.alldata(l,4) == -99 
            irr = [irr, l];
        elseif behav.alldata(l,3) > 1600    % Duration cue presentation: 300; Duration interstimulus interval: 1200; plus 100ms margin, 
                                            % as the reaction to the outcome presentation would not be as fast.
            irr_late = [irr_late, l];
        elseif behav.alldata(l,3) < 0
            if behav.alldata(l,4) ~=-99
                irr_early = [irr_early, l];
            end
        end
    end
    SSL.VS.irr = irr;
    SSL.VS.irr_late = irr_late;
    SSL.VS.irr_early = irr_early;
    
    irr_all = [SSL.VS.irr SSL.VS.irr_late SSL.VS.irr_early];
    SSL.VS.irr_all = unique(irr_all);
    
    % if subject showed irregular behaviour, mark choices as NaNs
    if isempty(SSL.VS.irr_all)
        irrout_all = 'none';
        SSL.VS.CongrCh = (behav.alldata(1:320,4) == behav.alldata(1:320,1));
    else
        SSL.VS.CongrCh = (behav.alldata(1:320,4) == behav.alldata(1:320,1));
        SSL.VS.CongrCh = double(SSL.VS.CongrCh);
        SSL.VS.CongrCh(SSL.VS.irr_all) = NaN;
        irrout_all = SSL.VS.irr_all;
    end
    
    %% disp irregular trials:
    disp(['irregular trials: ', num2str(irrout_all)]);
    disp('**************************************************************');
    if isempty(irr)
        irrout = 'none';
    else
        irrout = irr;
    end
    if isempty(irr_late)
        irrout_late = 'none';
    else
        irrout_late = irr_late;
    end
    if isempty(irr_early)
        irrout_early = 'none';
    else
        irrout_early = irr_early;
    end
    disp(['missed trials: ', num2str(irrout)]);
    disp(['late trials: ', num2str(irrout_late)]);
    disp(['early trials: ', num2str(irrout_early)]);
    
    %% run model
    SSL.VS.est = fitModel(SSL.VS.CongrCh(:,1), SSL.VS.CongrIn(:,1), maskprc{m}, 'unitsq_sgm_config', 'quasinewton_optim_config');
    
    mkdir([paths.res_behaviour_ssl,maskModel{m}]);
    cd ([paths.res_behaviour_ssl,maskModel{m}]);
    save('SSL.VS.mat', '-struct','SSL','VS');
    
    %% plot trajectories
    VSmat = load ([[paths.res_behaviour_ssl,maskModel{m}],'/SSL.VS.mat']);
    if strcmp('/RW_v1',maskModel{m})==1
        rw_binary_plotTraj(VSmat.VS.est);
    else
        hgf_binary_plotTraj(VSmat.VS.est);
    end
    cd ([paths.res_behaviour_ssl,maskModel{m}]);
    print -djpeg ssl_traj;
    
    % copy plots to quality folder:
    mkdir([paths.quality_ssl_behav,'/HGF/',maskModel{m}]);
    copyfile ('ssl_traj.jpg', [paths.quality_ssl_behav,'/HGF/',maskModel{m},'/ssl_traj_',details.subjname,'.jpg']);
    delete(findall(0,'Type','figure'));
end

end
