function ADPRSI_agfMRI_ssl_check_behaviour_data
% ADPRSI_agfMRI_ssl_check_behaviour_data:
% - writes out the structure Check_antfMRI_ssl_behaviour that contains
%   information about model fit and participants' behaviour 
% - plots these data.
%
% model parameters.
%   IN:     -

%   OUT:    -

% 04-11-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

% Set paths
workdir = ADPRSI_agfMRI_ssl_set_workdir;
options = ADPRSI_agfMRI_ssl_set_analysis_options;
[ details, paths ] = ADPRSI_agfMRI_ssl_subjects( 1 );

% Start diary
diary(paths.logfile_check_behaviour_ssl);

% define models
maskModel = {'/HGF_fixom_v1',  '/HGF_2l_v1', '/RW_v1'};

%% remove Check_agfMRI_ssl_behaviour file before running the script
cd(paths.quality_ssl_behav);
unix(['rm ', 'Check_agfMRI_ssl_behaviour']);
%%
for m = 1:length(maskModel)
    % Go through subjects
    for id = 1:numel(options.subjectIDs)
        [ details, paths ] = ADPRSI_agfMRI_ssl_subjects( id );
        disp(['running check behavioural data for subj: ', details.subjname,' (',num2str(id),')']);
        disp ('**************************************************************')

        % check whether modeling results have been stored:
        if exist([paths.res_behaviour_ssl,maskModel{m},'/SSL.VS.mat'])== 2;
            HGFssl = load([paths.res_behaviour_ssl,maskModel{m},'/SSL.VS.mat']);
            Check_agfMRI_ssl_behaviour.SubjName{id,1} = details.subjname;
            Check_agfMRI_ssl_behaviour.SubjIndex(id,1) = id;
        else
            Check_agfMRI_ssl_behaviour.SubjName{id,1} = details.subjname;
            Check_agfMRI_ssl_behaviour.SubjIndex(id,1) = id;
            cd(paths.quality_ssl_behav);
            save('Check_agfMRI_ssl_behaviour.mat', '-struct','Check_agfMRI_ssl_behaviour');
            continue
        end
        
        % Check modeling of behavioural data:
       if strcmp('/HGF_fixom_v1',maskModel{m})==1
            if any(abs(diff(HGFssl.VS.est.traj.mu3))>4) % find subjects with jumps >4 in mu3
                Check_agfMRI_ssl_behaviour.model_fixom_v1.max_diff_mu3(id,1) = max(abs(diff(HGFssl.VS.est.traj.mu3)));
                Check_agfMRI_ssl_behaviour.model_fixom_v1.fit_diff_mu3{id,1} = 'bad';
                Check_agfMRI_ssl_behaviour.model_fixom_v1.out_diff_mu3(id,1) = -1;
            else
                Check_agfMRI_ssl_behaviour.model_fixom_v1.max_diff_mu3(id,1) = max(abs(diff(HGFssl.VS.est.traj.mu3)));
                Check_agfMRI_ssl_behaviour.model_fixom_v1.fit_diff_mu3{id,1} = 'good';
                Check_agfMRI_ssl_behaviour.model_fixom_v1.out_diff_mu3(id,1) = 1;
            end
            if  any(abs(diff(HGFssl.VS.est.traj.sa3))>4) % find subjects with jumps >4 in sa3
                Check_agfMRI_ssl_behaviour.model_fixom_v1.max_diff_sa3(id,1) = max(abs(diff(HGFssl.VS.est.traj.sa3)));
                Check_agfMRI_ssl_behaviour.model_fixom_v1.fit_diff_sa3{id,1} = 'bad';
                Check_agfMRI_ssl_behaviour.model_fixom_v1.out_diff_sa3(id,1) = -1;
            else
                Check_agfMRI_ssl_behaviour.model_fixom_v1.max_diff_sa3(id,1) = max(abs(diff(HGFssl.VS.est.traj.sa3)));
                Check_agfMRI_ssl_behaviour.model_fixom_v1.fit_diff_sa3{id,1} = 'good';
                Check_agfMRI_ssl_behaviour.model_fixom_v1.out_diff_sa3(id,1) = 1;
            end
       elseif strcmp('/HGF_2l_v1',maskModel{m})==1
            if max(abs(diff(HGFssl.VS.est.traj.mu2))>10) % find subjects with jumps >10 in mu2
                Check_agfMRI_ssl_behaviour.model_2l_v1.max_mu2(id,1) = (max(diff(HGFssl.VS.est.traj.mu2)));
                Check_agfMRI_ssl_behaviour.model_2l_v1.fit_max_mu2{id,1} = 'bad';
                Check_agfMRI_ssl_behaviour.model_2l_v1.out_max_mu2(id,1) = -1;
            else
                Check_agfMRI_ssl_behaviour.model_2l_v1.max_mu2(id,1) = (max(diff(HGFssl.VS.est.traj.mu2)));
                Check_agfMRI_ssl_behaviour.model_2l_v1.fit_max_mu2{id,1} = 'good';
                Check_agfMRI_ssl_behaviour.model_2l_v1.out_max_mu2(id,1) = 1;
            end
        elseif strcmp('/RW_v1',maskModel{m})==1
            if HGFssl.VS.est.p_prc.al>0.9 % find subjects with alpha > 0.9
                Check_agfMRI_ssl_behaviour.model_rw_v1.al(id,1) = HGFssl.VS.est.p_prc.al;
                Check_agfMRI_ssl_behaviour.model_rw_v1.fit_al{id,1} = 'bad';
                Check_agfMRI_ssl_behaviour.model_rw_v1.out_fit_al(id,1) = -1;
            else
                Check_agfMRI_ssl_behaviour.model_rw_v1.al(id,1) = HGFssl.VS.est.p_prc.al;
                Check_agfMRI_ssl_behaviour.model_rw_v1.fit_al{id,1} = 'good';
                Check_agfMRI_ssl_behaviour.model_rw_v1.out_fit_al(id,1) = 1;
            end
        end
        cd(paths.quality_ssl_behav);
        save('Check_agfMRI_ssl_behaviour.mat', '-struct','Check_agfMRI_ssl_behaviour');
        
    end
end

Check_agfMRI_ssl_behaviour = load([paths.quality_ssl_behav,'/Check_agfMRI_ssl_behaviour.mat']);
for id = 1:numel(options.subjectIDs)
    [ details, paths ] = ADPRSI_agfMRI_ssl_subjects( id );
    behav = load([paths.res_behaviour_ssl,details.behaviour_ssl]);
    HGFssl = load([paths.res_behaviour_ssl,maskModel{1},'/SSL.VS.mat']);
    
    % Check behaviour:
    % reaction times
    SSL.VS.irr_all   = unique([HGFssl.VS.irr HGFssl.VS.irr_late HGFssl.VS.irr_early]);
    SSL.VS.irr_no    = HGFssl.VS.irr;
    SSL.VS.irr_late  = HGFssl.VS.irr_late;
    SSL.VS.irr_early = HGFssl.VS.irr_early;
    
    % RT:
    RT = behav.alldata(:,3);

    % mean RT
    RT(SSL.VS.irr_all) = [];
    Check_agfMRI_ssl_behaviour.behav.RT.mean(id,1) = mean(RT);
    % Correct responses
    CR = behav.alldata(:,5);
    CR(SSL.VS.irr_all) = [];
    Check_agfMRI_ssl_behaviour.behav.CR.sum(id,1) = sum(CR);
    Check_agfMRI_ssl_behaviour.behav.CR.percent(id,1) = sum(CR)/237;% 237=max number correct trials possible if we factor in the probability structure of the task:
    
    % a=[40;32;24;40;32;24;32;24;40;32];            % number of trials per block
    % b=[0.9;0.9;0.5;0.7;0.7;0.9;0.7;0.7;0.5;0.9];  % probability for each block
    % sum(a.*b) --> ~237
            
    % Count number of irregular trials
    Check_agfMRI_ssl_behaviour.behav.Missed.irr_total(id,1) = length(SSL.VS.irr_all);
    Check_agfMRI_ssl_behaviour.behav.Missed.irr_no(id,1) = length(SSL.VS.irr_no);
    Check_agfMRI_ssl_behaviour.behav.Missed.irr_late(id,1) = length(SSL.VS.irr_late);
    Check_agfMRI_ssl_behaviour.behav.Missed.irr_early(id,1) = length(SSL.VS.irr_early);

    % find subjects with less than 65% correct responses
    if Check_agfMRI_ssl_behaviour.behav.CR.percent(id,1) < 0.65
        Check_agfMRI_ssl_behaviour.behav.performance_CR{id,1} = 'bad';
        Check_agfMRI_ssl_behaviour.behav.out_performance_CR(id,1) = -1;
    else
        Check_agfMRI_ssl_behaviour.behav.performance_CR{id,1} = 'good';
        Check_agfMRI_ssl_behaviour.behav.out_performance_CR(id,1) = 1;
    end
    % find subjects with moren than 48 irregular trials
    if Check_agfMRI_ssl_behaviour.behav.Missed.irr_total(id,1) > 48
        Check_agfMRI_ssl_behaviour.behav.performance_irr{id,1} = 'bad';
        Check_agfMRI_ssl_behaviour.behav.out_performance_irr(id,1) = -1;
    else
        Check_agfMRI_ssl_behaviour.behav.performance_irr{id,1} = 'good';
        Check_agfMRI_ssl_behaviour.behav.out_performance_irr(id,1) = 1;
    end
end

cd(paths.quality_ssl_behav);
save('Check_agfMRI_ssl_behaviour.mat', '-struct','Check_agfMRI_ssl_behaviour');

%% create data structure with all "outliers"
Check_agfMRI_ssl_behaviourData = load([paths.quality_ssl_behav,'/Check_agfMRI_ssl_behaviour.mat']);

Check_agfMRI_ssl_behaviour.out_matrix = [Check_agfMRI_ssl_behaviourData.SubjIndex ...
    Check_agfMRI_ssl_behaviourData.behav.out_performance_CR Check_agfMRI_ssl_behaviourData.behav.out_performance_irr ...
    Check_agfMRI_ssl_behaviourData.model_fixom_v1.out_diff_mu3 Check_agfMRI_ssl_behaviourData.model_fixom_v1.out_diff_sa3 ...
    Check_agfMRI_ssl_behaviourData.model_2l_v1.out_max_mu2 ...
    Check_agfMRI_ssl_behaviourData.model_rw_v1.out_fit_al ...
    ];

Check_agfMRI_ssl_behaviour.out_label = {'SubjIndex' ...
    'out_performance_CR' 'out_performance_irr' ...
    'fixom_v1.out_diff_mu3' 'fixom_v1.out_diff_sa3' ...
    '2l_v1.out_fit_max_mu2' ...
    'rw_v1.out_fit_al' ...
    };

%% check models estimated with hgfv1.0 and for which more than 30% showed a failure of parameter estimation (~26)
for c = 4:7 % indices for model entries in Check_antfMRI_ssl_behaviourData.out_matrix
    if numel(find(Check_agfMRI_ssl_behaviour.out_matrix(:,c) == -1))>26
        Check_agfMRI_ssl_behaviour.out_model_v1_0(1,c) = -1;
    else
        Check_agfMRI_ssl_behaviour.out_model_v1_0(1,c) = 1;
    end
end

index_goodModels_v1_0 = find(Check_agfMRI_ssl_behaviour.out_model_v1_0 == 1);

%% display and store name of models estimated with hgfv1.0 for which more than 26 subjects showed a failure of parameter estimation
model_label = {'SubjIndex' ...
    'performance_CR' 'performance_irr' ...
    'HGF_1_fixom_v1_0' 'HGF_1_fixom_v1_0' ...
    'HGF_3_fixth_fixka_v1_0' ...
    'RW_v1_0' ...
    };

A = 4:7;
b=ismember(A, index_goodModels_v1_0);
model_label(b==0);
disp(['HGFv1.0 - failure of parameter estimation in more than 26 subjects: ']);
disp(model_label(b==0));
Check_agfMRI_ssl_behaviour.model_out_label = model_label(b==0);


%% check subjects with failure of parameter estimation in any model, estimated with hgfv1.0 
j=1;
for i = 1:numel(options.subjectIDs)
    if any(Check_agfMRI_ssl_behaviour.out_matrix(i,index_goodModels_v1_0) == -1)
        Check_agfMRI_ssl_behaviour.out_badEst_v1_0(1,j) = i;
        j=j+1;
    end
end
j=1;
for i = 1:numel(options.subjectIDs)
    if any(Check_agfMRI_ssl_behaviour.out_matrix(i,index_goodModels_v1_0) == 0)  
        Check_agfMRI_ssl_behaviour.out_nan_v1_0(1,j) = i;
        j=j+1;
    end
end

%% create vector of subjects with failure of parameter estimation, using hgfv1.0
if isfield(Check_agfMRI_ssl_behaviour,'out_nan_v1_0') == 1 && isfield(Check_agfMRI_ssl_behaviour,'out_badEst_v1_0')==1
    Check_agfMRI_ssl_behaviour.out_all_v1_0 = [Check_agfMRI_ssl_behaviour.out_badEst_v1_0 Check_agfMRI_ssl_behaviour.out_nan_v1_0];
elseif isfield(Check_agfMRI_ssl_behaviour,'out_badEst_v1_0')==1
    Check_agfMRI_ssl_behaviour.out_all_v1_0 = [Check_agfMRI_ssl_behaviour.out_badEst_v1_0];
elseif isfield(Check_agfMRI_ssl_behaviour,'out_nan_v1_0') == 1
    Check_agfMRI_ssl_behaviour.out_all_v1_0 = [Check_agfMRI_ssl_behaviour.out_nan_v1_0];
else
    Check_agfMRI_ssl_behaviour.out_all_v1_0 = [];
end
Check_agfMRI_ssl_behaviour.out_v1_0 = unique(Check_agfMRI_ssl_behaviour.out_all_v1_0);

%% plot table
check_behav = table(Check_agfMRI_ssl_behaviourData.SubjName, Check_agfMRI_ssl_behaviourData.SubjIndex, ...
    Check_agfMRI_ssl_behaviour.out_matrix(:,2), Check_agfMRI_ssl_behaviour.out_matrix(:,3), ...
    Check_agfMRI_ssl_behaviour.out_matrix(:,4),  ...
    Check_agfMRI_ssl_behaviour.out_matrix(:,5), Check_agfMRI_ssl_behaviour.out_matrix(:,6), Check_agfMRI_ssl_behaviour.out_matrix(:,7));
check_behav.Properties.VariableNames{'Var1'}='Names';
check_behav.Properties.VariableNames{'Var2'}='Index';
check_behav.Properties.VariableNames{'Var3'}='out_performance_CR';
check_behav.Properties.VariableNames{'Var4'}='out_performance_irr';
check_behav.Properties.VariableNames{'Var5'}='fixom_v1_mu3';
check_behav.Properties.VariableNames{'Var6'}='fixom_v1_sa3';
check_behav.Properties.VariableNames{'Var7'}='twol_v1_mu2';
check_behav.Properties.VariableNames{'Var8'}='rw_v1_al';

check_behav

check_behav_table = [Check_agfMRI_ssl_behaviourData.SubjIndex, ...
    Check_agfMRI_ssl_behaviour.out_matrix(:,2), Check_agfMRI_ssl_behaviour.out_matrix(:,3), ...
    Check_agfMRI_ssl_behaviour.out_matrix(:,4),  ...
    Check_agfMRI_ssl_behaviour.out_matrix(:,5), Check_agfMRI_ssl_behaviour.out_matrix(:,6), Check_agfMRI_ssl_behaviour.out_matrix(:,7)];

cd(paths.quality_ssl_behav);
save('Check_agfMRI_ssl_behaviour.mat', '-struct','Check_agfMRI_ssl_behaviour');
save('check_behav_table.mat', 'check_behav_table');

%% plot behavioural data for quality check

x=1:numel(options.subjectIDs);
figure('Color',[1 1 1]);
% plot reaction times:
subplot(1,3,1);bar(Check_agfMRI_ssl_behaviourData.behav.RT.mean, 'FaceColor',[0.25 .55 .79]);hold on; title('mean RT (msec)');

% plot percent correct responses:
subplot(1,3,2);bar(Check_agfMRI_ssl_behaviourData.behav.CR.percent, 'FaceColor',[0.2 0.71 0.3]);hold on;
hold on; plot([0 numel(options.subjectIDs)],[0.65 0.65],'red');
bigIdx = Check_agfMRI_ssl_behaviourData.behav.CR.percent<0.65;
text(x(bigIdx),Check_agfMRI_ssl_behaviourData.behav.CR.percent(bigIdx)+0.3,strrep(Check_agfMRI_ssl_behaviourData.SubjName(bigIdx),'_',' '),'horizontalAlignment','center'); hold on;
title('% correct responses');
clear bigIdx;

% plot number of missed responses:
subplot(1,3,3);bar(Check_agfMRI_ssl_behaviourData.behav.Missed.irr_total, 'FaceColor',[0.25 .55 .79]);hold on;
hold on; plot([0 numel(options.subjectIDs)],[48 48],'red'); title('missed responses');
bigIdx = Check_agfMRI_ssl_behaviourData.behav.Missed.irr_total>48;
text(x(bigIdx),Check_agfMRI_ssl_behaviourData.behav.Missed.irr_total(bigIdx)+1,strrep(Check_agfMRI_ssl_behaviourData.SubjName(bigIdx),'_',' '),'horizontalAlignment','center'); hold on;
clear bigIdx;

print -djpeg check_behav2_behav_ssl
saveas (gcf,'check_behav2_behav_ssl','fig');

%% plot modeled data for quality check - hgfv1.0
figure('Color',[1 1 1]);
% 3l-hgf: plot largest jump in mu3 trajectory:
subplot(1,3,1); bar(Check_agfMRI_ssl_behaviourData.model_fixom_v1.max_diff_mu3, 'FaceColor',[0.2 0.71 0.3]); hold on;
title('diff mu3 HGF1 fixom','FontSize', 8);
plot([0 numel(options.subjectIDs)],[4 4],'red');
bigIdx = Check_agfMRI_ssl_behaviourData.model_fixom_v1.max_diff_mu3>4;
text(x(bigIdx),Check_agfMRI_ssl_behaviourData.model_fixom_v1.max_diff_mu3(bigIdx)+2, ...
    strrep(Check_agfMRI_ssl_behaviourData.SubjName(bigIdx),'_',' '),'horizontalAlignment','center'); hold on;
clear bigIdx;

% 3l-hgf: plot largest jump in sa3 trajectory:
subplot(1,3,2); bar(Check_agfMRI_ssl_behaviourData.model_fixom_v1.max_diff_sa3, 'FaceColor',[0.25 .55 .79]); hold on;
title('mean mu3 HGF1 fixom','FontSize', 8);
plot([0 numel(options.subjectIDs)],[4 4],'red');
bigIdx = Check_agfMRI_ssl_behaviourData.model_fixom_v1.max_diff_sa3>4;
text(x(bigIdx),Check_agfMRI_ssl_behaviourData.model_fixom_v1.max_diff_sa3(bigIdx)+2, ...
    strrep(Check_agfMRI_ssl_behaviourData.SubjName(bigIdx),'_',' '),'horizontalAlignment','center'); hold on;
clear bigIdx;

% 2l-hgf: plot largest jump in mu2 trajectory:
subplot(1,3,3); bar(Check_agfMRI_ssl_behaviourData.model_2l_v1.max_mu2, 'FaceColor',[0.2 0.71 0.3]); hold on;
title('max mu2 HGF3 fixth fixka','FontSize', 8);
plot([0 numel(options.subjectIDs)],[10 10],'red');
bigIdx = Check_agfMRI_ssl_behaviourData.model_2l_v1.max_mu2>10;
text(x(bigIdx),Check_agfMRI_ssl_behaviourData.model_2l_v1.max_mu2(bigIdx), ...
    strrep(Check_agfMRI_ssl_behaviourData.SubjName(bigIdx),'_',' '),'horizontalAlignment','center'); hold on;
clear bigIdx;
% set figure title:
set(gcf,'NextPlot','add');
axes;
h = title('Using hgfToolBox v1.0');
set(gca,'Visible','off');
set(h,'Visible','on');
print -djpeg check_behav2_hgfv1_ssl
saveas (gcf,'check_behav2_hgfv1_ssl','fig');

%%%%%%%%%%%%%%%%
% RW: plot largest jump in al trajectory:
figure('Color',[1 1 1]);
bar(Check_agfMRI_ssl_behaviourData.model_rw_v1.al, 'FaceColor',[0.25 .55 .79]); hold on;
title('al RW v1','FontSize', 8);
plot([0 numel(options.subjectIDs)],[0.9 0.9],'red');
bigIdx = Check_agfMRI_ssl_behaviourData.model_rw_v1.al>0.9;
text(x(bigIdx),Check_agfMRI_ssl_behaviourData.model_rw_v1.al(bigIdx)-0.1, ...
    strrep(Check_agfMRI_ssl_behaviourData.SubjName(bigIdx),'_',' '),'horizontalAlignment','left'); hold on;
clear bigIdx;
% set figure title:
set(gcf,'NextPlot','add');
axes;
h = title('Using hgfToolBox v1.0');
set(gca,'Visible','off');
set(h,'Visible','on');

print -djpeg check_behav2_rw_ssl
saveas (gcf,'check_behav2_rw_ssl','fig');

end