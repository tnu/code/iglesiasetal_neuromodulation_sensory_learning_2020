function ADPRSI_agfMRI_ssl_prepare_SPSS
% ADPRSI_agfMRI_ssl_prepare_SPSS: writes the structure ADPRSI_agfMRI_ssl_SPSS 
% with all behavioural data needed for statistical analyses.

%   IN:     -

%   OUT:    -

% 09-01-2020; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

% Set paths
workdir = ADPRSI_agfMRI_ssl_set_workdir;
options = ADPRSI_agfMRI_ssl_set_analysis_options;
[ details, paths ] = ADPRSI_agfMRI_ssl_subjects( 1 );

% Start diary
diary(paths.logfile_hgf_ssl);

% Initialize
disp(['preparing struct for SPSS analyses']);

descr = load([paths.res_group_ssl_descr,'/ADPRSI_agfMRI_ssl.mat']);
hgf1=load([paths.res_group_ssl_behaviour,'/HGF_fixom_v1/A_SSL_VS.mat']);
out=load([paths.res_group_ssl_glm,'/sslbin/fixom_bin3_spm12_fact/agfMRI_ssl_group.mat']);
check_behav = load([paths.quality_ssl_behav,'/Check_agfMRI_ssl_behaviour.mat']);

ADPRSI_agfMRI_ssl_SPSS.labels = [descr.subj, descr.drug, descr.snp.comt];
ADPRSI_agfMRI_ssl_SPSS.labels_string = [{'SubjName'}, {'SubstanceString'}, {'COMTString'}]';
ADPRSI_agfMRI_ssl_SPSS.data = [descr.age, descr.weight, descr.group, check_behav.behav.CR.percent, check_behav.behav.RT.mean ...
    hgf1.ka hgf1.om hgf1.th hgf1.ze ...
    descr.behav.KSS, descr.snp.comt_code, 1./check_behav.behav.RT.mean];
ADPRSI_agfMRI_ssl_SPSS.data_string = [{'Age'}, {'Weight'}, {'Substance'}, {'CR'}, {'RT'}, ...
    {'ka'}, {'om'}, {'th'}, {'ze'}, ...
    {'KSS'}, {'COMTrs4680'}, {'InverseRT'}]';

% delete entries from excluded subjects:
ADPRSI_agfMRI_ssl_SPSS.labels(out.out_final,:)=[];
ADPRSI_agfMRI_ssl_SPSS.data(out.out_final,:)=[];

[~,~]= mkdir([paths.res_group_ssl_behaviour,'/SPSS']);
cd([paths.res_group_ssl_behaviour,'/SPSS'])
save('ADPRSI_agfMRI_ssl_SPSS.mat', '-struct','ADPRSI_agfMRI_ssl_SPSS');
end

