function ADPRSI_agfMRI_ssl_imCalc_meanstruct
% ADPRSI_agfMRI_ssl_imCalc_meanstruct: Compute mean structural image accross
% all participants' wstruct images.
% Only include participants which are part of the final analysis.

%   IN:     -

%   OUT:    -
%
% 04-11-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

% Set paths
workdir = ADPRSI_agfMRI_ssl_set_workdir;
options = ADPRSI_agfMRI_ssl_set_analysis_options;
[ details, paths ] = ADPRSI_agfMRI_ssl_subjects( 1 );

% Start diary
diary(paths.logfile_group_ssl);

% Initialize
disp(['running mean struct']);

% load data to determine subjects with <65% CR, >15% missed trials
% and bad model fits:
check_out = load([paths.quality_ssl_behav,'/Check_agfMRI_ssl_behaviour.mat']);

% HGF_1_fixom_v1 was the winning model:
[i_fixom, j_fixom] = find(check_out.model_fixom_v1.out_diff_sa3 ==-1);
[i_fixom2, j_fixom2] = find(check_out.model_fixom_v1.out_diff_mu3 ==-1);
[i_check, j_check] = find(check_out.behav.out_performance_CR==-1);
[i_check2, j_check2] = find(check_out.behav.out_performance_irr==-1);
out = [i_fixom; i_fixom2; i_check; i_check2];
out_final = unique(out);
disp(['subjects out: ', num2str(out_final')])

% exclude subjects:
check_out.SubjName(out_final)=[];

for i = 1:numel(check_out.SubjName)
    subjstruct{i,1} = ([options.resultsdir, '/results_fMRI_ssl/',check_out.SubjName{i,1},'/scandata/struct/wmstruct.nii']);   
end

%-----------------------------------------------------------------------
% Job configuration
%-----------------------------------------------------------------------

%% mean struct:
matlabbatch{1}.spm.util.imcalc.input = subjstruct;

%%
matlabbatch{1}.spm.util.imcalc.output = 'mean_struct_agfMRI_ssl.img';
matlabbatch{1}.spm.util.imcalc.outdir = {[paths.res_group_ssl_glm]};
matlabbatch{1}.spm.util.imcalc.expression = 'mean(X)';
matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
matlabbatch{1}.spm.util.imcalc.options.dmtx = 1;
matlabbatch{1}.spm.util.imcalc.options.mask = 0;
matlabbatch{1}.spm.util.imcalc.options.interp = -7;
matlabbatch{1}.spm.util.imcalc.options.dtype = 4;

% ---------------------------------------------------------------------
% Execute matlabbatch
spm_jobman('run',matlabbatch);

clear matlabbatch;

%% mean skull-stripped struct:
matlabbatch{1}.spm.util.imcalc.input = subjstruct;
matlabbatch{1}.spm.util.imcalc.output = 'mean_Brain_agfMRI_ssl.img';
matlabbatch{1}.spm.util.imcalc.outdir = {[paths.res_group_ssl_glm]};
matlabbatch{1}.spm.util.imcalc.expression = 'mean(X)';
matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
matlabbatch{1}.spm.util.imcalc.options.dmtx = 1;
matlabbatch{1}.spm.util.imcalc.options.mask = 0;
matlabbatch{1}.spm.util.imcalc.options.interp = -7;
matlabbatch{1}.spm.util.imcalc.options.dtype = 4;

% ---------------------------------------------------------------------
% Execute matlabbatch
spm_jobman('run',matlabbatch);
end