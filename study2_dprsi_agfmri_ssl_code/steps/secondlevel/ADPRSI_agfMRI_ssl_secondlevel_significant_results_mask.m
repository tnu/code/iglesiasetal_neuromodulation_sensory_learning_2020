function ADPRSI_agfMRI_ssl_secondlevel_significant_results_mask(m,r)
% ADPRSI_agfMRI_ssl_secondlevel_significant_results_mask:
% - plots the significant results across the anatomical mask at FWE p<0.05
%   peak-level and/or cluster-level with a CDT of p<0.001

%   IN:     m                   - first level model
%                                   1 = epsilon (GLM1)
%                                   2 = delta   (GLM2)
%           r                   - regressor, i.e. computational state
%                                 if m == 1
%                                   1 = epsilon2
%                                   2 = epsilon3
%                                   3 = epsilon_ch
%                                   4 = basic regressor
%                                 if m == 2
%                                   1 = da1
%                                   2 = da2
%                                   3 = da_ch
%                                   4 = pw2
%                                   5 = pw3
%                                   6 = basic regressor
%   OUT:    coord_vector        - coordinates of plotted region
%           handle              - handle of figure with plotted results

%   VARIABLES:
%   - m: model:
%       - 1: model with 3 parametric modulators (GLM1): 'fixom_bin3_spm12_fact'
%       - 2: model with 5 parametric modulators (GLM2): 'fixom_bin5_spm12_fact'
%   - r: index for parametric modulator (regressor)

%   - SubjOut: subjects that have been excluded for estimating the second-level GLM
%   - ag_ssl: load information about groups (drug/ genetics)

%   - pla_nr_subj: number of subjects in placebo group
%   - lev_nr_subj: number of subjects in levodopa group
%   - gal_nr_subj: number of subjects in galantamine group

%   - Result_index:         index of the n-th significant result
%   - ResultRow_indices:    vector with indices of significant results
%   - cluster_col:          contains all cluster-level p-values
%   - peak_col:             contains all peak-level p-values
%   - x_text:               text plotted below SPM results specifying the
%                           significance threshold

%   - ClusterLevel.sig:
%       - 1: significant cluster-level results
%       - 0: no significant cluster-level results

%   - PeakLevel.sig:
%       - 1: significant peak-level results
%       - 0: no significant peak-level results

%   - agfMRI_ssl_results:
%       - Bin_folder: folder where 2nd-level results have been stored
%           - two different GLMs:
%               - 3 parametric modulators (GLM1): 'fixom_bin3_spm12_fact'
%               - 5 parametric modulators (GLM2): 'fixom_bin5_spm12_fact'
%       - paths
%           - fileStruct:     path to average warped structural image (used as overlay)
%           - dirResultsSign: path to store 2nd-level significant results
%           - dirGlm:         path to 2nd-level SPM.mat file
%           - dirGlmResults:  path to 2nd-level GLM results

%       - PM:               parametric modulator
%       - PM_n:             index of parametric modulator
%       - Nconstrasts:      number of contrasts
%       - NoZoom:           do not apply 'Zoom' for the specified contrasts
%       - ResData:          contains all entries from SPM results table
%       - Stats_output:     contains only columns 1:11 from SPM results table
%       - contrast_index:   index of the current contrast
%
%       - mode: which threshold was used
%           - cluster-level or peak-level
%       - cluster: cluster-level results
%           - pvalue:           contains all cluster-level p-values
%           - pvalue_sign:      contains all significant cluster-level p-values
%           - pvalue_notsign:   contains all nearly signifcant cluster-level p-values
%       - peak: peak-level results
%           - pvalue:           contains all peak-level p-values
%           - pvalue_sign:      contains all significant peak-level p-values
%           - pvalue_notsign:   contains all nearly signifcant peak-level p-values
%
%   - results_sig: structure with results information (cluster size, 
%                  p-value, coordinate, t-score) 
%       - l:    headings 
%       - dat:  data

% 04-11-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

% Set paths
workdir = ADPRSI_agfMRI_ssl_set_workdir;
options = ADPRSI_agfMRI_ssl_set_analysis_options;
[ details, paths ] = ADPRSI_agfMRI_ssl_subjects( 1 );

% Start diary
diary(paths.logfile_group_ssl);

%% Initialize
disp(['running "plot results accross the anatomical mask" for second level analysis; model: ', num2str(m),' (regressor: ',num2str(r),')']);

sep1 = '===============================================================================================';
sep2 = '-----------------------------------------------------------------------------------------------';
sep3 = '***********************************************************************************************';
agfMRI_ssl_results.region = 'mask'; %anatomical mask

% specify where 2nd-level results have been stored
agfMRI_ssl_results.Bin_folder = {'fixom_bin3_spm12_fact', 'fixom_bin5_spm12_fact'};

if m == 1 % GLM1
    agfMRI_ssl_results.PM = {'epsilon2','epsilon3', 'epsilon_ch', 'SSL'};
    agfMRI_ssl_results.firstlevel_contrasts = {'con_0009','con_0011','con_0015','con_0007'};
    agfMRI_ssl_results.firstlevel_con = agfMRI_ssl_results.firstlevel_contrasts{r};
    agfMRI_ssl_results.PM_n = {'1','2','3','6'};
    agfMRI_ssl_results.PM_name = {'\epsilon_2', '\epsilon_3', '\epsilon_c','_h', 'trial per se'};
    agfMRI_ssl_results.Ncontrasts = 21;
elseif m == 2 % GLM2
    agfMRI_ssl_results.PM = {'da1','da2', 'wCPE', 'pw2', 'pw3', 'SSL'};
    agfMRI_ssl_results.firstlevel_contrasts = {'con_0013','con_0015','con_0017','con_0019','con_0023','con_0011'};
    agfMRI_ssl_results.firstlevel_con = agfMRI_ssl_results.firstlevel_contrasts{r};
    agfMRI_ssl_results.PM_n = {'1','2','3','4','5','6'};
    agfMRI_ssl_results.PM_name = {'\delta_1', '\delta_2', '\delta_c','_h', 'psi_2', 'psi_3', 'trial per se'};
    agfMRI_ssl_results.Ncontrasts = 21;
end
disp(sep3);
%% specify contrasts without zoom
agfMRI_ssl_results.NoZoom = [1:6, 13,14,21]; %average effects

%% load data:
% load structural image:
agfMRI_ssl_results.paths.fileStruct = fullfile([paths.res_group_ssl_glm,'/mean_Brain_agfMRI_ssl.img']);
% load vector of outliers
SubjOut = load([paths.res_group_ssl_glm,'/sslbin/', agfMRI_ssl_results.Bin_folder{m},'/mask/agfMRI_ssl_group.mat']);
% load descriptives:
ag_ssl = load([paths.res_group_ssl_descr,'/ADPRSI_agfMRI_ssl.mat']);
% count number of subjects per condition:
drug = ag_ssl.group;
drug(SubjOut.out_final) = [];
agfMRI_ssl_results.group_info.weight = ag_ssl.weight;
agfMRI_ssl_results.group_info.weight(SubjOut.out_final) = [];

agfMRI_ssl_results.pla_nr_subj = numel(drug(drug == 0));
agfMRI_ssl_results.lev_nr_subj = numel(drug(drug == 1));
agfMRI_ssl_results.gal_nr_subj = numel(drug(drug == 2));

agfMRI_ssl_results.cov_weight_pla = agfMRI_ssl_results.group_info.weight(drug == 0);
agfMRI_ssl_results.cov_weight_lev = agfMRI_ssl_results.group_info.weight(drug == 1);
agfMRI_ssl_results.cov_weight_gal = agfMRI_ssl_results.group_info.weight(drug == 2);

agfMRI_ssl_results.subjPaths = load([paths.res_group_ssl_glm,'/sslbin/', agfMRI_ssl_results.Bin_folder{m},'/mask/agfMRI_ssl_subj_mask_',agfMRI_ssl_results.PM{r},'.mat']);
agfMRI_ssl_results.subjPaths = [agfMRI_ssl_results.subjPaths.subjpaths_lev; agfMRI_ssl_results.subjPaths.subjpaths_gal; agfMRI_ssl_results.subjPaths.subjpaths_pla];

%% specify folders
agfMRI_ssl_results.paths.dirResults = ([paths.res_group_ssl_glm,'/sslbin/', agfMRI_ssl_results.Bin_folder{m},'/mask/',agfMRI_ssl_results.PM{r},'_basic_DACh']);
agfMRI_ssl_results.paths.dirResultsSign = ([paths.res_group_ssl_glm,'/sslbin/', agfMRI_ssl_results.Bin_folder{m},'/mask/',agfMRI_ssl_results.PM{r},'_basic_DACh/plots_sign_results']);
mkdir (agfMRI_ssl_results.paths.dirResultsSign);

% select SPM.mat file
agfMRI_ssl_results.paths.dirGlm        = ([paths.res_group_ssl_glm,'/sslbin/', agfMRI_ssl_results.Bin_folder{m},'/mask/',agfMRI_ssl_results.PM{r},'_basic_DACh/SPM.mat']);
agfMRI_ssl_results.paths.dirGlmResults = ([paths.res_group_ssl_glm,'/sslbin/', agfMRI_ssl_results.Bin_folder{m},'/mask/',agfMRI_ssl_results.PM{r},'_basic_DACh/']);

for i = 1:agfMRI_ssl_results.Ncontrasts
    agfMRI_ssl_results.contrast_index = i;
    %-----------------------------------------------------------------------
    matlabbatch{1}.spm.stats.results.spmmat = {agfMRI_ssl_results.paths.dirGlm};
    matlabbatch{1}.spm.stats.results.conspec.titlestr = '';
    matlabbatch{1}.spm.stats.results.conspec.contrasts = agfMRI_ssl_results.contrast_index;
    matlabbatch{1}.spm.stats.results.conspec.threshdesc = 'none';
    matlabbatch{1}.spm.stats.results.conspec.thresh = 0.001;
    matlabbatch{1}.spm.stats.results.conspec.extent = 0;
    matlabbatch{1}.spm.stats.results.conspec.conjunction = 1;
    matlabbatch{1}.spm.stats.results.conspec.mask.none = 1;
    matlabbatch{1}.spm.stats.results.units = 1;
    matlabbatch{1}.spm.stats.results.export{1}.pdf = true;
    
    spm_jobman('run',matlabbatch); % run_nogui
    SPM = evalin('base','SPM');
    xSPM = evalin('base','xSPM');
    hReg = evalin('base','hReg');
    TabDat = evalin('base','TabDat');
    
    agfMRI_ssl_results.ResData = TabDat.dat;
    TabDat.dat( cellfun(@isempty, TabDat.dat) ) = {99};
    agfMRI_ssl_results.Stats_output = (cell2mat(TabDat.dat(:,1:11)));
    if isempty(TabDat.dat) == 1 % no results
        ClusterLevel.sig(agfMRI_ssl_results.contrast_index) = 0;% cluster-level
        PeakLevel.sig(agfMRI_ssl_results.contrast_index) = 0;   % peak-level
        continue
    else
        %% cluster-level results
        agfMRI_ssl_results.cluster.pvalue = cell2mat(TabDat.dat(:,3)); % find cluster-level FWE-corrected results with CDT < 0.001
        agfMRI_ssl_results.cluster.pvalue_sign = agfMRI_ssl_results.cluster.pvalue(:,1) < 0.05; % significant
        agfMRI_ssl_results.cluster.pvalue_notsign = (agfMRI_ssl_results.cluster.pvalue(:,1) > 0.05 & agfMRI_ssl_results.cluster.pvalue(:,1) < 0.1);  % not-significant
        
        if find(agfMRI_ssl_results.cluster.pvalue_sign == 1) % if significant
            agfMRI_ssl_results.mode = 'cluster_level_sign';
            ClusterLevel.sig(agfMRI_ssl_results.contrast_index) = 1; % contrast is significant FWE cluster-level
            cd(agfMRI_ssl_results.paths.dirResultsSign);
            save(['contrast_cluster_',num2str(agfMRI_ssl_results.contrast_index)],'-struct','TabDat');
            % save results figure
            cluster_col = agfMRI_ssl_results.Stats_output(:,3); % FWE cluster-level
            [ResultRow_indices,w] = find(cluster_col<0.05 & cluster_col>=0);
            if any(agfMRI_ssl_results.contrast_index==agfMRI_ssl_results.NoZoom) == 0 % if not main effects
                for Result_index = 1:length(ResultRow_indices)
                    x_text = ({['p<0.05 FWE cluster-level (with an initial threshold of p <0.001) corrected across the anatomical mask: p = ', num2str(cluster_col(ResultRow_indices(Result_index)))]});
                    % find cluster size:
                    cluster_size=agfMRI_ssl_results.Stats_output(ResultRow_indices(Result_index),5);
                    % plot significant results
                    [coord_vector,handle] = ADPRSI_agfMRI_ssl_plot_secondlevel_results(agfMRI_ssl_results,ResultRow_indices,Result_index,m,r,x_text,cluster_size);
                    % save plot
                    saveas(handle,[agfMRI_ssl_results.PM_n{r},'_',num2str(1),'_',agfMRI_ssl_results.PM{r},'_contrast_cluster_mask_',num2str(agfMRI_ssl_results.contrast_index),'_',num2str(Result_index)],'fig');
                    saveas(handle,[agfMRI_ssl_results.PM_n{r},'_',num2str(1),'_',agfMRI_ssl_results.PM{r},'_contrast_cluster_mask_',num2str(agfMRI_ssl_results.contrast_index),'_',num2str(Result_index)],'jpg');
                    % store information in structure:
                    results_sig.cluster.l = {'cluster size';'p-value (FWEcluster)'; 'x; y; z';'t score'};
                    results_sig.cluster.dat(:,Result_index) = {sprintf('%0.4f', cell2mat(agfMRI_ssl_results.ResData(ResultRow_indices(Result_index),5)));sprintf('%0.2f', cell2mat(agfMRI_ssl_results.ResData(ResultRow_indices(Result_index),3))); coord_vector; sprintf('%0.2f',cell2mat(agfMRI_ssl_results.ResData(ResultRow_indices(Result_index),9)))};
                    cd(agfMRI_ssl_results.paths.dirResultsSign);
                    save(['contrast_cluster_results_',num2str(agfMRI_ssl_results.contrast_index),'sig.mat'],'-struct','results_sig');
                    
                    % plot responses for differential effects:
                    x_text_eigenv=({['significant at p<0.05 ',agfMRI_ssl_results.mode,' correction: p = ', num2str(cluster_col(ResultRow_indices(Result_index)))]});
                    [coord_vector, handle1] = ADPRSI_agfMRI_ssl_plot_main_effects_eigenvariate(agfMRI_ssl_results,ResultRow_indices,Result_index,m,r,x_text_eigenv,cluster_size);
                    % save results and plots:
                    save(['Data_',num2str(agfMRI_ssl_results.contrast_index),'_',sprintf('sphere%04d',Result_index),'_',num2str(r),'_',agfMRI_ssl_results.PM{r},'_',num2str(coord_vector(1,1)),'_',num2str(coord_vector(1,2)),'_',num2str(coord_vector(1,3)),'_cluster.mat'],'agfMRI_ssl_results');
                    cd(agfMRI_ssl_results.paths.dirResultsSign);
                    saveas (handle1, [num2str(agfMRI_ssl_results.contrast_index),'_',sprintf('sphere%04d',Result_index),'_',num2str(r),'_',agfMRI_ssl_results.PM{r},'_mask_cluster'], 'fig');
                    saveas (handle1, [num2str(agfMRI_ssl_results.contrast_index),'_',sprintf('sphere%04d',Result_index),'_',num2str(r),'_',agfMRI_ssl_results.PM{r},'_mask_cluster'], 'jpg');
                end % for differential effects
            else
                for Result_index = 1 % only plot the global maximum
                    % find the size of the smallest cluster that is still
                    % significant:
                    pvalue_sign=(agfMRI_ssl_results.cluster.pvalue(agfMRI_ssl_results.cluster.pvalue_sign));
                    [max_pvalue,w3]=max(pvalue_sign);
                    [cluster_size,w3]=find(agfMRI_ssl_results.cluster.pvalue==max_pvalue);
                    x_text = ({['p<0.05 FWE cluster-level (with an initial threshold of p <0.001) corrected across the anatomical mask: p = ', num2str(cluster_col(ResultRow_indices(Result_index)))]});
                    %% plot results
                    [coord_vector,handle]=ADPRSI_agfMRI_ssl_plot_secondlevel_results(agfMRI_ssl_results,ResultRow_indices,Result_index,m,r,x_text, ...
                        cluster_size);
                    saveas(handle,[agfMRI_ssl_results.PM_n{r},'_',num2str(1),'_',agfMRI_ssl_results.PM{r},'_contrast_cluster_mask_',num2str(agfMRI_ssl_results.contrast_index),'_',num2str(Result_index)],'fig');
                    saveas(handle,[agfMRI_ssl_results.PM_n{r},'_',num2str(1),'_',agfMRI_ssl_results.PM{r},'_contrast_cluster_mask_',num2str(agfMRI_ssl_results.contrast_index),'_',num2str(Result_index)],'jpg');
                    % store information in structure:
                    for Result_index = 1:length(ResultRow_indices)
                        results_sig.cluster.l = {'cluster size';'p-value (FWEcluster)'; 'x; y; z';'t score'};
                        results_sig.cluster.dat(:,Result_index) = {sprintf('%0.4f', cell2mat(agfMRI_ssl_results.ResData(ResultRow_indices(Result_index),5)));sprintf('%0.2f', cell2mat(agfMRI_ssl_results.ResData(ResultRow_indices(Result_index),3))); coord_vector; sprintf('%0.2f',cell2mat(agfMRI_ssl_results.ResData(ResultRow_indices(Result_index),9)))};
                        cd(agfMRI_ssl_results.paths.dirResultsSign);
                        save(['contrast_cluster_results_',num2str(agfMRI_ssl_results.contrast_index),'sig.mat'],'-struct','results_sig');
                        save(['Data_',num2str(agfMRI_ssl_results.contrast_index),'_',sprintf('sphere%04d',Result_index),'_',num2str(r),'_',agfMRI_ssl_results.PM{r},'_',num2str(coord_vector(1,1)),'_',num2str(coord_vector(1,2)),'_',num2str(coord_vector(1,3)),'_cluster.mat'],'agfMRI_ssl_results');
                    end
                end % for average effects
            end % for loop
        else
            ClusterLevel.sig(agfMRI_ssl_results.contrast_index) = 0; % contrast is not significant FWE cluster-level
        end % for cluster-level significance
        %% clear variables
        clear cluster_col;
        clear ResultRow_indices
        clear w
        clear w2
        clear w3
        clear max_pvalue
        clear cluster_size
        
        %% peak-level results
        agfMRI_ssl_results.peak.pvalue = cell2mat(TabDat.dat(:,7)); % find peak-level FWE-corrected results
        agfMRI_ssl_results.peak.pvalue_sign = agfMRI_ssl_results.peak.pvalue(:,1) < 0.05; %significant
        agfMRI_ssl_results.peak.pvalue_notsign = (agfMRI_ssl_results.peak.pvalue(:,1) > 0.05 & agfMRI_ssl_results.peak.pvalue(:,1) < 0.1);  % not-significant
        
        if find(agfMRI_ssl_results.peak.pvalue_sign == 1) % if significant
            agfMRI_ssl_results.mode = 'peak_level_sign';
            PeakLevel.sig(agfMRI_ssl_results.contrast_index) = 1; % contrast is significant peak-level, FWE
            cd(agfMRI_ssl_results.paths.dirResultsSign);
            save(['contrast_peak_',num2str(agfMRI_ssl_results.contrast_index)],'-struct','TabDat');
            % save results figure
            peak_col = agfMRI_ssl_results.Stats_output(:,7); % FWE peak-level
            [ResultRow_indices,w] = find(peak_col<0.05 & peak_col>=0);
            if any(agfMRI_ssl_results.contrast_index==agfMRI_ssl_results.NoZoom) == 0 % if not main effects
                for Result_index = 1:length(ResultRow_indices)
                    cluster_size = 0; % cluster-size
                    x_text = ({['p<0.05 FWE peak-level corrected across the anatomical mask: p = ', num2str(peak_col(ResultRow_indices(Result_index)))]});
                    % plot significant results
                    [coord_vector,handle] = ADPRSI_agfMRI_ssl_plot_secondlevel_results(agfMRI_ssl_results,ResultRow_indices,Result_index,m,r,x_text,cluster_size);
                    % save plot
                    saveas(handle,[agfMRI_ssl_results.PM_n{r},'_',num2str(1),'_',agfMRI_ssl_results.PM{r},'_contrast_peak_mask_',num2str(agfMRI_ssl_results.contrast_index),'_',num2str(Result_index)],'fig');
                    saveas(handle,[agfMRI_ssl_results.PM_n{r},'_',num2str(1),'_',agfMRI_ssl_results.PM{r},'_contrast_peak_mask_',num2str(agfMRI_ssl_results.contrast_index),'_',num2str(Result_index)],'jpg');
                    % store information in structure:
                    results_sig.peak.l = {'p-value (FWEpeak)'; 'x; y; z';'t score'};
                    results_sig.peak.dat(:,Result_index) = {sprintf('%0.2f', cell2mat(agfMRI_ssl_results.ResData(ResultRow_indices(Result_index),7))); coord_vector; sprintf('%0.2f',cell2mat(agfMRI_ssl_results.ResData(ResultRow_indices(Result_index),9)))};
                    cd(agfMRI_ssl_results.paths.dirResultsSign);
                    save(['contrast_peak_results_',num2str(agfMRI_ssl_results.contrast_index),'sig.mat'],'-struct','results_sig');
                    
                    % plot responses for differential effects:
                    x_text_eigenv=({['significant at p<0.05 ',agfMRI_ssl_results.mode,' correction: p = ', num2str(peak_col(ResultRow_indices(Result_index)))]});
                    [coord_vector, handle1] = ADPRSI_agfMRI_ssl_plot_main_effects_eigenvariate(agfMRI_ssl_results,ResultRow_indices,Result_index,m,r,x_text_eigenv,cluster_size);
                    
                    save(['Data_',num2str(agfMRI_ssl_results.contrast_index),'_',sprintf('sphere%04d',Result_index),'_',num2str(r),'_',agfMRI_ssl_results.PM{r},'_',num2str(coord_vector(1,1)),'_',num2str(coord_vector(1,2)),'_',num2str(coord_vector(1,3)),'_peak.mat'],'agfMRI_ssl_results');
                    cd(agfMRI_ssl_results.paths.dirResultsSign);
                    saveas (handle1, [num2str(agfMRI_ssl_results.contrast_index),'_',sprintf('sphere%04d',Result_index),'_',num2str(r),'_',agfMRI_ssl_results.PM{r},'_mask_peak'], 'fig');
                    saveas (handle1, [num2str(agfMRI_ssl_results.contrast_index),'_',sprintf('sphere%04d',Result_index),'_',num2str(r),'_',agfMRI_ssl_results.PM{r},'_mask_peak'], 'jpg');
                end
            else
                for Result_index = 1  % only plot the global maximum
                    cluster_size = 0; % cluster-size
                    x_text = ({['p<0.05 FWE peak-level corrected across the anatomical mask: p = ', num2str(peak_col(ResultRow_indices(Result_index)))]});
                    %% plot results
                    [coord_vector,handle]=ADPRSI_agfMRI_ssl_plot_secondlevel_results(agfMRI_ssl_results,ResultRow_indices,Result_index,m,r,x_text, ...
                        cluster_size);
                    saveas(handle,[agfMRI_ssl_results.PM_n{r},'_',num2str(1),'_',agfMRI_ssl_results.PM{r},'_contrast_peak_mask_',num2str(agfMRI_ssl_results.contrast_index),'_',num2str(Result_index)],'fig');
                    saveas(handle,[agfMRI_ssl_results.PM_n{r},'_',num2str(1),'_',agfMRI_ssl_results.PM{r},'_contrast_peak_mask_',num2str(agfMRI_ssl_results.contrast_index),'_',num2str(Result_index)],'jpg');
                    % store information in structure:
                    for Result_index = 1:length(ResultRow_indices)
                        results_sig.peak.l = {'p-value (FWEpeak)'; 'x; y; z';'t score'};
                        results_sig.peak.dat(:,Result_index) = {sprintf('%0.2f', cell2mat(agfMRI_ssl_results.ResData(ResultRow_indices(Result_index),7))); coord_vector; sprintf('%0.2f',cell2mat(agfMRI_ssl_results.ResData(ResultRow_indices(Result_index),9)))};
                        cd(agfMRI_ssl_results.paths.dirResultsSign);
                        save(['contrast_peak_results_',num2str(agfMRI_ssl_results.contrast_index),'sig.mat'],'-struct','results_sig');
                        save(['Data_',num2str(agfMRI_ssl_results.contrast_index),'_',sprintf('sphere%04d',Result_index),'_',num2str(r),'_',agfMRI_ssl_results.PM{r},'_',num2str(coord_vector(1,1)),'_',num2str(coord_vector(1,2)),'_',num2str(coord_vector(1,3)),'_peak.mat'],'agfMRI_ssl_results');
                    end
                end % for average effects
            end % for loop
        else
            PeakLevel.sig(agfMRI_ssl_results.contrast_index) = 0; % contrast is not significant FWE peak-level
        end % for peak-level significance
        %% clear variables
        clear peak_col;
        clear ResultRow_indices
        clear w
        clear w2
        clear w3
        clear max_pvalue
        clear cluster_size
    end
    
    try
        cd(agfMRI_ssl_results.paths.dirResultsSign);
        save(['ClusterLevel_',agfMRI_ssl_results.Bin_folder{m},agfMRI_ssl_results.PM{r},'_basic'],'-struct','ClusterLevel');
        save(['PeakLevel_',agfMRI_ssl_results.Bin_folder{m},agfMRI_ssl_results.PM{r},'_basic'],'-struct','PeakLevel');
    catch e
        fprintf(1,'There was an error! The message was:\n%s',e.message);
    end
    close all;
end
