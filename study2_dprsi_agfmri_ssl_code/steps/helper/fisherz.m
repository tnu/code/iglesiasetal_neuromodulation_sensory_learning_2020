function[z]=fisherz(r)
% Fisher's Z-transform
% r = r(:);
z=0.5.*log((1+r)./(1-r));