function[r]=ifisherz(z)
% Inverse Fisher's Z-transform

% z = z(:);
r=(exp(2*z)-1)./(exp(2*z)+1);