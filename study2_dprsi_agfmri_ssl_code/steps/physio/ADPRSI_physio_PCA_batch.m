function ADPRSI_physio_PCA_batch( physio_input, paths )
% ADPRSI_physio_PCA_batch: Runs the PhysIO toolbox
% PHYSIO Version 2019b, v7.2.1

%   IN:     physio_input        - input for PhysIO
%           paths               - subject specific paths
%   OUT:    -

% Info:
% 04-11-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

% Set paths
workdir = ADPRSI_agfMRI_ssl_set_workdir;

%% PhysIO
matlabbatch{1}.spm.spatial.normalise.write.subj.def = {fullfile([paths.res_struct,'/y_struct.nii'])};
matlabbatch{1}.spm.spatial.normalise.write.subj.resample = {
    fullfile([paths.res_struct,'/c2struct.nii,1'])
    fullfile([paths.res_struct,'/c3struct.nii,1'])
    };
matlabbatch{1}.spm.spatial.normalise.write.woptions.bb = [-78 -112 -70
    78 76 85];
matlabbatch{1}.spm.spatial.normalise.write.woptions.vox = [1 1 1];
matlabbatch{1}.spm.spatial.normalise.write.woptions.interp = 7;
matlabbatch{1}.spm.spatial.normalise.write.woptions.prefix = 'w';

matlabbatch{2}.spm.tools.physio.save_dir = {physio_input.res_physlog};
matlabbatch{2}.spm.tools.physio.log_files.vendor = 'Philips';
matlabbatch{2}.spm.tools.physio.log_files.cardiac = {[physio_input.res_physlog,physio_input.details_physio]};
matlabbatch{2}.spm.tools.physio.log_files.respiration = {[physio_input.res_physlog,physio_input.details_physio]};
matlabbatch{2}.spm.tools.physio.log_files.scan_timing = {''};
matlabbatch{2}.spm.tools.physio.log_files.sampling_interval = 0.0020;
matlabbatch{2}.spm.tools.physio.log_files.relative_start_acquisition = 0;
matlabbatch{2}.spm.tools.physio.log_files.align_scan = 'last';
matlabbatch{2}.spm.tools.physio.scan_timing.sqpar.Nslices = 37;
matlabbatch{2}.spm.tools.physio.scan_timing.sqpar.NslicesPerBeat = 37;
matlabbatch{2}.spm.tools.physio.scan_timing.sqpar.TR = 2.5;
matlabbatch{2}.spm.tools.physio.scan_timing.sqpar.Ndummies = 5;
matlabbatch{2}.spm.tools.physio.scan_timing.sqpar.Nscans = physio_input.Nscans;
matlabbatch{2}.spm.tools.physio.scan_timing.sqpar.onset_slice = 18;
matlabbatch{2}.spm.tools.physio.scan_timing.sqpar.time_slice_to_slice = 0.0676;
matlabbatch{2}.spm.tools.physio.scan_timing.sqpar.Nprep = [];
matlabbatch{2}.spm.tools.physio.scan_timing.sync.gradient_log.grad_direction = 'y';
matlabbatch{2}.spm.tools.physio.scan_timing.sync.gradient_log.zero = 0.4;
matlabbatch{2}.spm.tools.physio.scan_timing.sync.gradient_log.slice = 0.45;
matlabbatch{2}.spm.tools.physio.scan_timing.sync.gradient_log.vol = [];
matlabbatch{2}.spm.tools.physio.scan_timing.sync.gradient_log.vol_spacing = [];
matlabbatch{2}.spm.tools.physio.preproc.cardiac.modality = 'ECG';
matlabbatch{2}.spm.tools.physio.preproc.cardiac.initial_cpulse_select.auto_matched.min = 0.4;
matlabbatch{2}.spm.tools.physio.preproc.cardiac.initial_cpulse_select.auto_matched.file = 'initial_cpulse_kRpeakfile.mat';
matlabbatch{2}.spm.tools.physio.preproc.cardiac.posthoc_cpulse_select.off = struct([]);
matlabbatch{2}.spm.tools.physio.model.output_multiple_regressors = physio_input.output_file;
matlabbatch{2}.spm.tools.physio.model.output_physio = 'physio.mat';
matlabbatch{2}.spm.tools.physio.model.orthogonalise = 'none';
matlabbatch{2}.spm.tools.physio.model.retroicor.yes.order.c = 3;
matlabbatch{2}.spm.tools.physio.model.retroicor.yes.order.r = 4;
matlabbatch{2}.spm.tools.physio.model.retroicor.yes.order.cr = 1;
matlabbatch{2}.spm.tools.physio.model.rvt.no = struct([]);
matlabbatch{2}.spm.tools.physio.model.hrv.no = struct([]);
matlabbatch{2}.spm.tools.physio.model.noise_rois.yes.fmri_files = physio_input.wfmri;
matlabbatch{2}.spm.tools.physio.model.noise_rois.yes.roi_files = cfg_dep('Normalise: Write: Normalised Images (Subj 1)', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('()',{1}, '.','files'));
matlabbatch{2}.spm.tools.physio.model.noise_rois.yes.thresholds = 0.9;
matlabbatch{2}.spm.tools.physio.model.noise_rois.yes.n_voxel_crop = 1;
matlabbatch{2}.spm.tools.physio.model.noise_rois.yes.n_components = 3;
matlabbatch{2}.spm.tools.physio.model.movement.yes.file_realignment_parameters = {physio_input.movement};
matlabbatch{2}.spm.tools.physio.model.movement.yes.order = 12;
matlabbatch{2}.spm.tools.physio.model.movement.yes.censoring_method = 'MAXVAL';
matlabbatch{2}.spm.tools.physio.model.movement.yes.censoring_threshold = 0.99;
matlabbatch{2}.spm.tools.physio.model.other.no = struct([]);
matlabbatch{2}.spm.tools.physio.verbose.level = 3;
matlabbatch{2}.spm.tools.physio.verbose.fig_output_file = 'PhysIO_output.fig';
matlabbatch{2}.spm.tools.physio.verbose.use_tabs = true;

% Execute actual_job
spm_jobman('run',matlabbatch);

