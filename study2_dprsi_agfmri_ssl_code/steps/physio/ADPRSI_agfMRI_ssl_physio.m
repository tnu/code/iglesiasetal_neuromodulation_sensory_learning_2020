function ADPRSI_agfMRI_ssl_physio( id )
% ADPRSI_agfMRI_ssl_physio: Runs the PhysIO toolbox for every participant

% PHYSIO Version 2019b, v7.2.1

%   IN:     id - the subject index

%   OUT:    -

% 01-11-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

% Set paths
workdir = ADPRSI_agfMRI_ssl_set_workdir;
options = ADPRSI_agfMRI_ssl_set_analysis_options;
[ details, paths ] = ADPRSI_agfMRI_ssl_subjects( id );

% Start diary
diary(paths.logfile_physio_ssl);

% Initialize
disp(['running PhysIO for subj: ', details.subjname,' (',num2str(id),')']);

% copy files from raw folder to results folder
ssl_files = dir([paths.physlog_ssl,'/*log']);
copyfile([paths.physlog_ssl,ssl_files(1,1).name],[paths.res_physlog_ssl,details.physio_ssl]);

%% PhysIO
% SSL
cd(paths.res_physlog_ssl);
SSL_physio.task = 'SSL';
SSL_physio.Nscans = 550;
SSL_physio.movement = ([paths.res_fMRIssl,'rp_fmri_ssl_00001.txt']);
SSL_physio.output_file = 'multiple_regressors_ssl.txt';
SSL_physio.res_physlog = paths.res_physlog_ssl;
SSL_physio.details_physio = details.physio_ssl;

% collect warped fMRI data:
fMRIssl_split = spm_select('List',paths.res_fMRIssl,'^wfmri_');
fMRIssl_split = cellstr(fMRIssl_split(:,:));
for i = 1:numel(fMRIssl_split)
    SSL_physio.wfmri(i,1) = fullfile(paths.res_fMRIssl,fMRIssl_split(i,1));
end

% Temporary fix for https://tnurepository.ethz.ch/physio/physio-public/issues/94
cmdline = spm_get_defaults('cmdline');
spm_get_defaults('cmdline', true);
ADPRSI_physio_PCA_batch(SSL_physio, paths)
spm_get_defaults('cmdline', cmdline);

end
