function ADPRSI_agfMRI_ssl_check_physio( id )
% ADPRSI_agfMRI_ssl_check_physio: 
% Checks and plots
% - no. of scans that have been marked with more than 0.99 mm (translation) 
%   or 0.99 degrees (rotation) of movement
% - plots movement regressors (translation and rotation)

%   IN:     id                  - the subject index

%   OUT:    -


% 04-11-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

% Set paths
workdir = ADPRSI_agfMRI_ssl_set_workdir;
options = ADPRSI_agfMRI_ssl_set_analysis_options;
[ details, paths ] = ADPRSI_agfMRI_ssl_subjects( id );

% Start diary
diary(paths.logfile_physio_ssl);

% Initialize
disp(['running check for PhysIO for subj: ', details.subjname,' (',num2str(id),')']);

%% Plot checks:
cd(paths.res_physlog_ssl);
physio_txt = load([paths.res_physlog_ssl,'/multiple_regressors_ssl.txt']);
figure('Color',[1 1 1]);
imagesc(physio_txt)
colormap gray; xlabel('regressor');ylabel('scan volume');
% determine additional censoring regressors
% RETROICOR = 18; Movement = 12 (with first derivative); 
% PCA (2 regions) = 8; therefore in total 38 nuisance regressors. 
% Additional regressors are "censoring" regressors:
physio_cens = numel(physio_txt(1,:))-38;
title({'Physiological regressor matrix for GLM', ...
'including input confound regressors: ' num2str(physio_cens)});

saveas (gcf, [details.subjname, '_check_physio'],'jpg');
copyfile ([details.subjname,'_check_physio.jpg'], [paths.quality_ssl_physio,details.subjname,'_check_physio.jpg']);

%% plot movement regressors
cd (paths.res_checkreg);
SSL_physio.movement = ([paths.res_fMRIssl,'rp_fmri_ssl_00001.txt']);
rp_ssl = load([SSL_physio.movement]);
figure;
subplot(2,1,1);plot(rp_ssl(:,1:3)); hold all;title('translation');
subplot(2,1,2);plot(rp_ssl(:,4:6)); hold all;title('rotation');
suptitle([details.subjname(1:5),' ',details.subjname(7:end)]);

saveas (gcf, [details.subjname, '_ssl_mov'],'pdf');
copyfile ([details.subjname,'_ssl_mov.pdf'], [paths.quality_ssl_prep,details.subjname,'_ssl_mov.pdf']);

end
