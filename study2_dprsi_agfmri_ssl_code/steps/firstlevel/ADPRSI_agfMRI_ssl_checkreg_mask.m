function ADPRSI_agfMRI_ssl_checkreg_mask( id )
% ADPRSI_agfMRI_ssl_checkreg_mask: checkreg mask.nii
% - overlay combined anatomical mask of dopaminergic (SN/VTA) and 
% cholinergic (PPT/LDT, basal forebrain) nuclei on subject-specific 
% mask.nii images (generated during ADPRSI_antfMRI_ssl_firstlevel_epsilon/
% ADPRSI_antfMRI_ssl_firstlevel_da steps).

%   IN:     id - the subject index

%   OUT:    -

% 01-11-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

% Set paths
workdir = ADPRSI_agfMRI_ssl_set_workdir;
options = ADPRSI_agfMRI_ssl_set_analysis_options;
[ details, paths ] = ADPRSI_agfMRI_ssl_subjects( id );

% Start diary
diary(paths.logfile_checkreg);

%% overlay anatomical mask to check for dropouts
% Initialize
disp(['running anatomical mask overlay on mask.nii: ', details.subjname,' (',num2str(id),')']);

cd(paths.res_first_ssl);
matlabbatch{1}.spm.util.checkreg.data = {
    [fullfile([paths.res_first_ssl, '/fixom_bin3_spm12/mask.nii'])]
    [fullfile([paths.res_first_ssl, '/fixom_bin5_spm12/mask.nii'])]
    };
spm_jobman('run',matlabbatch);

% display anatomical image on the other images
spm_orthviews('addcolouredimage', [1:2], [workdir.code,'\steps\mask\DAch_anat_red.nii'], [0 0 1]);
spm_orthviews('Redraw');
spm_orthviews('Reposition',[5 -20 -21]);
spm_orthviews('Xhairs','on');
spm_orthviews('Zoom',60);

matlabbatch2{1}.spm.util.print.fname = ([details.subjname,'_mask_overlay_first_ssl']);
matlabbatch2{1}.spm.util.print.fig.fighandle = NaN;
matlabbatch2{1}.spm.util.print.opts = 'pdf';
spm_jobman('run',matlabbatch2);

copyfile ([details.subjname,'_mask_overlay_first_ssl_001.pdf'], [paths.quality_ssl_first,details.subjname,'_mask_overlay_first_ssl.pdf']);


