function ADPRSI_agfMRI_ssl_nuisance_epsilon( id )
% ADPRSI_agfMRI_ssl_nuisance_epsilon: overlays the constrasts weighting
% either respiration, cardiac regressors, their interaction or movement on
% a structural image.

% Define in function ADPRSI_agfMRI_ssl_set_analysis_options.m which 
% structural image you would like to use as "options.nuisance.templ_overlay":
% case 'templ_wstruct'   : the subject-specific warped structural image, or
% case 'templ_meanstruct': mean structural image (note that if you would 
%                          like to use this option with your own data, you
%                          first need to run this function: 
%                          ADPRSI_agfMRI_ssl_imCalc_meanstruct.m)

%   IN:     id - the subject index

%   OUT:    -

%% preprocessing:
% - without slice timing
%% GLM
% - no orthogonalization
% - 3 parametric modulators:
%   epsilon2, epsilon3, precision-weighted choice prediction error

% 04-11-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

% Set paths
workdir = ADPRSI_agfMRI_ssl_set_workdir;
options = ADPRSI_agfMRI_ssl_set_analysis_options;
[ details, paths ] = ADPRSI_agfMRI_ssl_subjects( id );

% Start diary
diary(paths.logfile_first_ssl);

% Initialize
disp(['plot nuisance regressors for subj: ', details.subjname,' (',num2str(id),')']);
disp('**** GLM - bin3 - epsilon ****');

% specify overlay image
switch options.nuisance.templ_overlay
    case 'templ_meanstruct'
        fileMean = [paths.res_group_ssl_glm, 'mean_struct_agfMRI_ssl.img'];
        fileStruct = [fileMean;fileMean;fileMean;fileMean];
    case 'templ_wstruct'
        filewarped = [paths.res_struct, details.coregstruct];
        fileStruct=[filewarped;filewarped;filewarped;filewarped];
end

% load SPM.mat
dirGlm = ([paths.res_first_ssl, '/fixom_bin3_spm12/SPM.mat']);

for i = 17:21
    job.spm.stats.results.spmmat = {dirGlm};
    job.spm.stats.results.conspec.titlestr = '';
    job.spm.stats.results.conspec.contrasts = i;
    job.spm.stats.results.conspec.threshdesc = 'FWE';
    job.spm.stats.results.conspec.thresh = 0.05;
    job.spm.stats.results.conspec.extent = 0;
    job.spm.stats.results.conspec.conjunction = 1;
    job.spm.stats.results.conspec.mask.none = 1;
    job.spm.stats.results.units = 1;
    job.spm.stats.results.print = 0;
    job.spm.stats.results.write.none = 1;
    spm_jobman('run',{job});
    
    Con(i).SPM = evalin('base','SPM');
    Con(i).xSPM = evalin('base','xSPM');
    Con(i).hReg = evalin('base','hReg');
    Con(i).TabDat = evalin('base','TabDat');
end

% plot structural images
spm_check_registration(fileStruct);

% initialize image counter
m=0;
for i = 17:21
    m=m+1;
    spm_orthviews('AddBlobs',m,Con(i).xSPM.XYZ,Con(i).xSPM.Z,Con(i).xSPM.M); hold on;
    spm_orthviews('Redraw'); hold on;
    if m == 1
        Con_title = ('cardiac; respiration; interaction; movement');
        handle = title({Con_title; details.subjname}, 'FontSize', 12);
        set(handle,'Position',[20, 1500,0]);
    end
end

job2{1}.spm.util.print.fname = ([details.subjname,'_ssl_3reg_nuisance']);
job2{1}.spm.util.print.fig.fighandle = NaN;
job2{1}.spm.util.print.opts = 'pdf';
spm_jobman('run',job2);

copyfile ([details.subjname,'_ssl_3reg_nuisance_001.pdf'], [paths.quality_ssl_prep,details.subjname,'_ssl_3reg_nuisance.pdf']);
end
