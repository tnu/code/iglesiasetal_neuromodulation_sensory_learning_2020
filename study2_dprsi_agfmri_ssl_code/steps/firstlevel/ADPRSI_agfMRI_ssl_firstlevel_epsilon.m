function ADPRSI_agfMRI_ssl_firstlevel_epsilon( id )
% ADPRSI_antfMRI_ssl_firstlevel_epsilon: run first level analysis - GLM1.

%   IN:     id - the subject index

%   OUT:    -

%% preprocessing:
% - without slice timing
%% GLM
% - no orthogonalization
% - 3 parametric modulators:
%   epsilon2, epsilon3, precision-weighted choice prediction error

% 04-11-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

% Set paths
workdir = ADPRSI_agfMRI_ssl_set_workdir;
options = ADPRSI_agfMRI_ssl_set_analysis_options;
[ details, paths ] = ADPRSI_agfMRI_ssl_subjects( id );

% Start diary
diary(paths.logfile_first_ssl);

% Initialize
disp(['running first level analysis for subj: ', details.subjname,' (',num2str(id),')']);
disp('**** GLM - bin3 - epsilon ****');
spm_jobman('initcfg');
which spm

% Find scans
% ---------------------------------------------------------------------
% NOTE: when rerunning the GLM analysis, make sure to delete all
% old files within "fixom_bin3_spm12"
cd (paths.res_first_ssl);
unix(['rm -r ', 'fixom_bin3_spm12']);
mkdir fixom_bin3_spm12;
dirGlm = ([paths.res_first_ssl, '/fixom_bin3_spm12/']);

% collect preprocessed fMRI data:
fMRIssl_split = spm_select('List',paths.res_fMRIssl,'swfmri_');
fMRIssl_split = cellstr(fMRIssl_split(:,:));
for i = 1:numel(fMRIssl_split)
    fMRIssl_scans(i,1) = fullfile(paths.res_fMRIssl,fMRIssl_split(i,1));
end

% Model specification
% (a) Timing
matlabbatch{1}.spm.stats.fmri_spec.dir = {dirGlm};
matlabbatch{1}.spm.stats.fmri_spec.timing.units = 'secs';
matlabbatch{1}.spm.stats.fmri_spec.timing.RT = 2.5;
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t = 16;
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0 = 8;
%%
matlabbatch{1}.spm.stats.fmri_spec.sess.scans = fullfile(fMRIssl_scans);

% (b) Conditions
glm = load(fullfile([paths.res_first_ssl,'/GLM_bin_HGF_fixom_all.mat']));
glm
%
% face condition
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).name = 'face';
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).onset = glm.Face;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).duration = 0;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).tmod = 0;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(1).name = 'face_epsilon2';
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(1).param = glm.epsilon2_Face;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(1).poly = 1;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(2).name = 'face_epsilon3';
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(2).param = glm.epsilon3_Face;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(2).poly = 1;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(3).name = 'face_epsilon_ch';
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(3).param = glm.epsilon_ch_Face;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(3).poly = 1;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).orth = 0;

% house condition
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).name = 'house';
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).onset = glm.House;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).duration = 0;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).tmod = 0;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).pmod(1).name = 'house_epsilon2';
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).pmod(1).param = glm.epsilon2_House;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).pmod(1).poly = 1;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).pmod(2).name = 'house_epsilon3';
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).pmod(2).param = glm.epsilon3_House;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).pmod(2).poly = 1;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).pmod(3).name = 'house_epsilon_ch';
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).pmod(3).param = glm.epsilon_ch_House;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).pmod(3).poly = 1;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).orth = 0;
% irr condition
if  isempty(glm.irr_missed) && isempty(glm.irr_late)
    disp('no additional regressors');
    r = 0;
elseif isempty(glm.irr_missed) || isempty(glm.irr_late)
    if isempty(glm.irr_late)
        matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(3).name = 'irr_missed';
        matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(3).onset = glm.irr_missed;
        matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(3).duration = 0;
        matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(3).tmod = 0;
        matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(3).orth = 0;
        r = 2;
    else
        matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(3).name = 'irr_late';
        matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(3).onset = glm.irr_late;
        matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(3).duration = 0;
        matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(3).tmod = 0;
        matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(3).orth = 0;
        r = 2;
    end
else
    matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(3).name = 'irr_missed';
    matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(3).onset = glm.irr_missed;
    matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(3).duration = 0;
    matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(3).tmod = 0;
    matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(3).orth = 0;
    
    matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(4).name = 'irr_late';
    matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(4).onset = glm.irr_late;
    matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(4).duration = 0;
    matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(4).tmod = 0;
    matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(4).orth = 0;
    
    r = 4;
end

matlabbatch{1}.spm.stats.fmri_spec.sess.multi = {''};
matlabbatch{1}.spm.stats.fmri_spec.sess.regress = struct('name', {}, 'val', {});
% (c) Further regressors
matlabbatch{1}.spm.stats.fmri_spec.sess.multi_reg = {fullfile([paths.res_physlog_ssl,'/multiple_regressors_ssl.txt'])};
matlabbatch{1}.spm.stats.fmri_spec.sess.hpf = 128;
matlabbatch{1}.spm.stats.fmri_spec.fact = struct('name', {}, 'levels', {});
matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [1 0];
matlabbatch{1}.spm.stats.fmri_spec.volt = 1;
matlabbatch{1}.spm.stats.fmri_spec.global = 'None';
matlabbatch{1}.spm.stats.fmri_spec.mthresh = 0.8;
matlabbatch{1}.spm.stats.fmri_spec.mask = {''};
matlabbatch{1}.spm.stats.fmri_spec.cvi = 'AR(1)';

%-----------------------------------------------------------------------
% Model estimation
matlabbatch{2}.spm.stats.fmri_est.spmmat(1) = cfg_dep('fMRI model specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{2}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{2}.spm.stats.fmri_est.method.Classical = 1;
%-----------------------------------------------------------------------
matlabbatch{3}.spm.stats.con.spmmat(1) = cfg_dep('Model estimation: SPM.mat File', substruct('.','val', '{}',{2}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{3}.spm.stats.con.consess{1}.tcon.name = 'face';
matlabbatch{3}.spm.stats.con.consess{1}.tcon.convec = 1;
matlabbatch{3}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{2}.tcon.name = 'house';
matlabbatch{3}.spm.stats.con.consess{2}.tcon.convec = [zeros(1,8) 1];
matlabbatch{3}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{3}.tcon.name = 'epsilon2_face';
matlabbatch{3}.spm.stats.con.consess{3}.tcon.convec = [zeros(1,2) 1];
matlabbatch{3}.spm.stats.con.consess{3}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{4}.tcon.name = 'epsilon2_house';
matlabbatch{3}.spm.stats.con.consess{4}.tcon.convec = [zeros(1,10) 1];
matlabbatch{3}.spm.stats.con.consess{4}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{5}.tcon.name = 'epsilon3_face';
matlabbatch{3}.spm.stats.con.consess{5}.tcon.convec = [zeros(1,4) 1];
matlabbatch{3}.spm.stats.con.consess{5}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{6}.tcon.name = 'epsilon3_house';
matlabbatch{3}.spm.stats.con.consess{6}.tcon.convec = [zeros(1,12) 1];
matlabbatch{3}.spm.stats.con.consess{6}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{7}.tcon.name = 'SSL_pos';
matlabbatch{3}.spm.stats.con.consess{7}.tcon.convec = [1, zeros(1,7) 1];
matlabbatch{3}.spm.stats.con.consess{7}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{8}.tcon.name = 'SSL_neg';
matlabbatch{3}.spm.stats.con.consess{8}.tcon.convec = [-1, zeros(1,7) -1];
matlabbatch{3}.spm.stats.con.consess{8}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{9}.tcon.name = 'epsilon2_SSL_pos';
matlabbatch{3}.spm.stats.con.consess{9}.tcon.convec = [zeros(1,2), 1, zeros(1,7) 1];
matlabbatch{3}.spm.stats.con.consess{9}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{10}.tcon.name = 'epsilon2_SSL_neg';
matlabbatch{3}.spm.stats.con.consess{10}.tcon.convec = [zeros(1,2), -1, zeros(1,7) -1];
matlabbatch{3}.spm.stats.con.consess{10}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{11}.tcon.name = 'epsilon3_SSL_pos';
matlabbatch{3}.spm.stats.con.consess{11}.tcon.convec = [zeros(1,4), 1, zeros(1,7) 1];
matlabbatch{3}.spm.stats.con.consess{11}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{12}.tcon.name = 'epsilon3_SSL_neg';
matlabbatch{3}.spm.stats.con.consess{12}.tcon.convec = [zeros(1,4), -1, zeros(1,7) -1];
matlabbatch{3}.spm.stats.con.consess{12}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{13}.tcon.name = 'epsilon_ch_face';
matlabbatch{3}.spm.stats.con.consess{13}.tcon.convec = [zeros(1,6),1];
matlabbatch{3}.spm.stats.con.consess{13}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{14}.tcon.name = 'epsilon_ch_house';
matlabbatch{3}.spm.stats.con.consess{14}.tcon.convec = [zeros(1,14),1];
matlabbatch{3}.spm.stats.con.consess{14}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{15}.tcon.name = 'epsilon_ch_SSL_pos';
matlabbatch{3}.spm.stats.con.consess{15}.tcon.convec = [zeros(1,6), 1, zeros(1,7) 1];
matlabbatch{3}.spm.stats.con.consess{15}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{16}.tcon.name = 'epsilon_ch_SSL_pos';
matlabbatch{3}.spm.stats.con.consess{16}.tcon.convec = [zeros(1,6), -1, zeros(1,7) -1];
matlabbatch{3}.spm.stats.con.consess{16}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{17}.fcon.name = 'cardiac';
matlabbatch{3}.spm.stats.con.consess{17}.fcon.weights = [zeros(6,(16+r)) eye(6)];
matlabbatch{3}.spm.stats.con.consess{17}.fcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{18}.fcon.name = 'respiration';
matlabbatch{3}.spm.stats.con.consess{18}.fcon.weights = [zeros(8,(22+r)) eye(8)];
matlabbatch{3}.spm.stats.con.consess{18}.fcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{19}.fcon.name = 'interaction';
matlabbatch{3}.spm.stats.con.consess{19}.fcon.weights = [zeros(4,(30+r)) eye(4)];
matlabbatch{3}.spm.stats.con.consess{19}.fcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{20}.fcon.name = 'PCA';
matlabbatch{3}.spm.stats.con.consess{20}.fcon.weights = [zeros(8,(34+r)) eye(8)];
matlabbatch{3}.spm.stats.con.consess{20}.fcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{21}.fcon.name = 'movement';
matlabbatch{3}.spm.stats.con.consess{21}.fcon.weights = [zeros(6,(42+r)) eye(6)];
matlabbatch{3}.spm.stats.con.consess{21}.fcon.sessrep = 'none';

matlabbatch{3}.spm.stats.con.delete = 1;

%-----------------------------------------------------------------------
% Execute matlabbatch
spm_jobman('run',matlabbatch);

end
