function ADPRSI_agfMRI_ssl_glmInput( id )
% ADPRSI_agfMRI_ssl_glmInput: writes the structures GLM_HGF_fixom_all and 
% GLM_bin_HGF_fixom_all that contain the input data for the first level glm.

%   IN:     id - the subject index

%   OUT:    -

% 04-11-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

% Set paths
workdir = ADPRSI_agfMRI_ssl_set_workdir;
options = ADPRSI_agfMRI_ssl_set_analysis_options;
[ details, paths ] = ADPRSI_agfMRI_ssl_subjects( id );

% Start diary
diary(paths.logfile_glm_input_ssl);

% Initialize
disp(['running glm_input for subj: ', details.subjname,' (',num2str(id),')']);

%% find winning model
WinningModel_BMS = load((fullfile(paths.res_hgf_ssl_bms,'/agfMRI_BMS_hgfv1_0.mat')));
[WinningModel_BMS.M,WinningModel_BMS.I] = max(WinningModel_BMS.exp_r);

maskModel = {'/HGF_fixom_v1',  '/HGF_2l_v1', '/RW_v1'};
WinningModel = maskModel{WinningModel_BMS.I};
disp('****************');
disp(WinningModel);
disp('****************');


% determine how many events take place within a trial:
TR = 2.5;
bin = TR/16;
ssl.trial = 1.8; % cue presentation =0.3; time for decision = 1.2; target presentation = 0.3
ssl.events_c = ceil(ssl.trial/bin);
ssl.events_f = floor(ssl.trial/bin);

ssl.diff_c = abs(ssl.trial - ssl.events_c*bin);
ssl.diff_f = abs(ssl.trial - ssl.events_f*bin);

if ssl.diff_c < ssl.diff_f
    ssl.events = ssl.events_c;
else
    ssl.events = ssl.events_f;
end
disp(['number of events within a trial: ', num2str(ssl.events)]);

%% load data
AnaSSL = load([paths.res_behaviour_ssl,WinningModel,'/SSL.VS.mat']);
behav = load([paths.res_behaviour_ssl,details.behaviour_ssl]);

% absolute value of PE and epsilon2 is equivalent to tracking 2
% different paths: one for the calculation of the PE/epsilon2
% for face present (=1)/ face absent (=0)
% and one for house present (=1)/ house absent (=0).

% states from the HGF model:
VSssl.sa1 = AnaSSL.VS.est.traj.sa1;
VSssl.sa1hat = AnaSSL.VS.est.traj.sa1hat;
VSssl.sa2 = AnaSSL.VS.est.traj.sa2;
VSssl.sa2hat = AnaSSL.VS.est.traj.sa2hat;
VSssl.sa3 = AnaSSL.VS.est.traj.sa3;
VSssl.sa3hat = AnaSSL.VS.est.traj.sa3hat;
VSssl.mu1 = AnaSSL.VS.est.traj.mu1;
VSssl.mu1hat = AnaSSL.VS.est.traj.mu1hat;
VSssl.mu2 = AnaSSL.VS.est.traj.mu2;
VSssl.mu2hat = AnaSSL.VS.est.traj.mu2hat;
VSssl.mu3 = AnaSSL.VS.est.traj.mu3;
VSssl.mu3hat = AnaSSL.VS.est.traj.mu3hat;

% precision-weighted prediction error at the lower level:
VSssl.epsilon2 = VSssl.sa2.*AnaSSL.VS.est.traj.da1;
% coding of inputs (i.e. houses vs. faces) is arbitrary, therefore 
% we use the absolute epsilon2/da1:
VSssl.abs_epsilon2 = abs(VSssl.epsilon2);
% prediction error and absolute prediction error at the lower level:
VSssl.da1 = AnaSSL.VS.est.traj.da1;
VSssl.abs_da1 = abs(VSssl.da1);

% to compute the precision-weighting of the PE at the higher
% level:
% mu3 (t-1)
ssl.mu3 = [AnaSSL.VS.est.p_prc.mu3_0 AnaSSL.VS.est.traj.mu3'];
ssl.mu3(end) = [];

% sa2 (t-1)
ssl.sa2 = [AnaSSL.VS.est.p_prc.sa2_0 AnaSSL.VS.est.traj.sa2'];
ssl.sa2(end) = [];

ssl.v2 = exp(AnaSSL.VS.est.p_prc.ka.*ssl.mu3 + AnaSSL.VS.est.p_prc.om);

ssl.pi2hat = 1./(ssl.sa2 + ssl.v2);

% pw = precision-weighting at the higher level = pi2hat(t)/pi3hat(t)
% pi3hat = 1/sa3(t)
% therefore:
ssl.pw = ssl.pi2hat.*AnaSSL.VS.est.traj.sa3';
VSssl.pw3 = ssl.pw;

% precision-weighted prediction error at the higher level:
VSssl.epsilon3 = ssl.pw.*  AnaSSL.VS.est.traj.da2';
% prediction error at the higher level:
VSssl.da2 = AnaSSL.VS.est.traj.da2';


% lower-level mu1hat-dependent choice prediction error computation is 
% dependent on participant's choice (c) and coding of cue-outcome pairs:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% prediction:                            %
%           |high tone   | low tone      % high tone/face & low tone/house: 
% ------------------------------------   % prediction (c) = mu1hat
% c = Face  |mu1hat      | 1(1-mu1hat)   % high tone/house & low tone/face: 
% c = House |1(1-mu1hat) | mu1hat        % prediction (c) = 1-mu1hat
%========================================%
% prediction error:                      %
% correct outcome = Face:                %
%           |high tone   | low tone      %
% ------------------------------------   %
% c = Face  |(1-mu1hat)  | mu1hat        %
% c = House |-1(1-mu1hat)| -1*mu1hat     %
%========================================%
% correct outcome = House:               %
%           |high tone  | low tone       %
% ------------------------------------   %
% c = Face  |-1*mu1hat  | -1(1-mu1hat)   %
% c = House |mu1hat     | (1-mu1hat)     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% to compute choice PE get type cue (high tone = 1; low tone = 0), 
% type target (Face = 1; House = 0) and choice (Face = 1; House = 0):
VSssl.cue = behav.alldata(:,1);
VSssl.target = behav.alldata(:,2);
VSssl.choice = behav.alldata(:,4);

% lower-level (signed) choice prediction error:
for i = 1:length(VSssl.choice)
    if VSssl.target(i)==1 && VSssl.cue(i)==1
        if VSssl.choice(i) == 1
            VSssl.da_ch(i) = 1-AnaSSL.VS.est.traj.mu1hat(i);
        elseif VSssl.choice(i) == 0
            VSssl.da_ch(i) = -1*(1-AnaSSL.VS.est.traj.mu1hat(i));
        else
            VSssl.da_ch(i) = -1*(1-AnaSSL.VS.est.traj.mu1hat(i));
        end
    elseif VSssl.target(i)==1 && VSssl.cue(i)==0
        if VSssl.choice(i) == 1
            VSssl.da_ch(i) = AnaSSL.VS.est.traj.mu1hat(i);
        elseif VSssl.choice(i) == 0
            VSssl.da_ch(i) = -1*AnaSSL.VS.est.traj.mu1hat(i);
        else
            VSssl.da_ch(i) = -1*AnaSSL.VS.est.traj.mu1hat(i);
        end
    elseif VSssl.target(i)==0 && VSssl.cue(i)==1
        if VSssl.choice(i) == 1
            VSssl.da_ch(i) = -1*AnaSSL.VS.est.traj.mu1hat(i);
        elseif VSssl.choice(i) == 0
            VSssl.da_ch(i) = AnaSSL.VS.est.traj.mu1hat(i);
        else
            VSssl.da_ch(i) = -1*AnaSSL.VS.est.traj.mu1hat(i);
        end
    elseif VSssl.target(i)==0 && VSssl.cue(i)==0
        if VSssl.choice(i) == 1
            VSssl.da_ch(i) = -1*(1-AnaSSL.VS.est.traj.mu1hat(i));
        elseif VSssl.choice(i) == 0
            VSssl.da_ch(i) = 1-AnaSSL.VS.est.traj.mu1hat(i);
        else
            VSssl.da_ch(i) = -1*(1-AnaSSL.VS.est.traj.mu1hat(i));
        end
    end
end
% lower-level (signed) precision-weighted choice prediction error
VSssl.epsilon_ch = VSssl.sa2.*VSssl.da_ch';

% get timing of targets & cues:
VSssl.TimeTarget = behav.alldata(:,8);
VSssl.TimeCue = behav.alldata(:,7);
% get target type (Face = 1; House = 0)
VSssl.TargetInput = behav.alldata(:,2);

%% GLM input
% compute exact timing of targets and cues (substract time of scan start):
GLM_HGF_fixom_all.SSL_input.TimeTarget=(VSssl.TimeTarget-behav.param.scanstart*(ones(320,1)))/1000;
GLM_HGF_fixom_all.SSL_input.TimeCue=(VSssl.TimeCue-behav.param.scanstart*(ones(320,1)))/1000;

% combine missed and early responses into same regressor
missed_early = unique([AnaSSL.VS.irr AnaSSL.VS.irr_early]);
if isempty(missed_early)
    GLM_HGF_fixom_all.SSL_input.irr = [];
else
    GLM_HGF_fixom_all.SSL_input.irr = GLM_HGF_fixom_all.SSL_input.TimeCue(missed_early);
end

% model late responses with a separate regressor
if isempty(AnaSSL.VS.irr_late)
    GLM_HGF_fixom_all.SSL_input.irr_late = [];
else
    GLM_HGF_fixom_all.SSL_input.irr_late = GLM_HGF_fixom_all.SSL_input.TimeCue(AnaSSL.VS.irr_late);
end

% reshape the following regressors:
VSssl.epsilon3=VSssl.epsilon3';
VSssl.da2=VSssl.da2';
VSssl.da_ch=VSssl.da_ch';
VSssl.pw3=VSssl.pw3';

% for completeness combine all regressors into one matrix:
GLM_HGF_fixom_all.SSL_input.data = [VSssl.TargetInput GLM_HGF_fixom_all.SSL_input.TimeCue VSssl.abs_epsilon2 VSssl.abs_da1 VSssl.sa1hat VSssl.sa1 ... %6
    VSssl.sa2hat VSssl.sa2 VSssl.sa3hat VSssl.sa3 VSssl.epsilon3 VSssl.da2 VSssl.mu1hat VSssl.mu1 VSssl.mu2hat VSssl.mu2 VSssl.mu3hat VSssl.mu3 ... %18
    VSssl.da_ch VSssl.epsilon_ch VSssl.pw3 GLM_HGF_fixom_all.SSL_input.TimeTarget]; %22

GLM_HGF_fixom_all.SSL_input.CueFace= GLM_HGF_fixom_all.SSL_input.data(GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,2);             % time of face cue
GLM_HGF_fixom_all.SSL_input.CueHouse= GLM_HGF_fixom_all.SSL_input.data(~GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,2);           % time of house cue

GLM_HGF_fixom_all.SSL_input.epsilon2_Face= GLM_HGF_fixom_all.SSL_input.data(GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,3);       % Face condition: epsilon2
GLM_HGF_fixom_all.SSL_input.epsilon2_House= GLM_HGF_fixom_all.SSL_input.data(~GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,3);     % House condition: epsilon2

GLM_HGF_fixom_all.SSL_input.da1_Face= GLM_HGF_fixom_all.SSL_input.data(GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,4);            % Face condition: da1
GLM_HGF_fixom_all.SSL_input.da1_House= GLM_HGF_fixom_all.SSL_input.data(~GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,4);          % House condition: da1

GLM_HGF_fixom_all.SSL_input.sa1hat_Face= GLM_HGF_fixom_all.SSL_input.data(GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,5);         % Face condition: sa1hat     
GLM_HGF_fixom_all.SSL_input.sa1hat_House= GLM_HGF_fixom_all.SSL_input.data(~GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,5);       % House condition: sa1hat

GLM_HGF_fixom_all.SSL_input.sa1_Face= GLM_HGF_fixom_all.SSL_input.data(GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,6);            % Face condition: sa1
GLM_HGF_fixom_all.SSL_input.sa1_House= GLM_HGF_fixom_all.SSL_input.data(~GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,6);          % House condition: sa1

GLM_HGF_fixom_all.SSL_input.sa2hat_Face= GLM_HGF_fixom_all.SSL_input.data(GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,7);         % Face condition: sa2hat
GLM_HGF_fixom_all.SSL_input.sa2hat_House= GLM_HGF_fixom_all.SSL_input.data(~GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,7);       % House condition: sa2hat

GLM_HGF_fixom_all.SSL_input.sa2_Face= GLM_HGF_fixom_all.SSL_input.data(GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,8);            % Face condition: sa2
GLM_HGF_fixom_all.SSL_input.sa2_House= GLM_HGF_fixom_all.SSL_input.data(~GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,8);          % House condition: sa2

GLM_HGF_fixom_all.SSL_input.sa3hat_Face= GLM_HGF_fixom_all.SSL_input.data(GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,9);         % Face condition: sa3hat
GLM_HGF_fixom_all.SSL_input.sa3hat_House= GLM_HGF_fixom_all.SSL_input.data(~GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,9);       % House condition: sa3hat

GLM_HGF_fixom_all.SSL_input.sa3_Face= GLM_HGF_fixom_all.SSL_input.data(GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,10);           % Face condition: sa3
GLM_HGF_fixom_all.SSL_input.sa3_House= GLM_HGF_fixom_all.SSL_input.data(~GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,10);         % House condition: sa3

GLM_HGF_fixom_all.SSL_input.epsilon3_Face= GLM_HGF_fixom_all.SSL_input.data(GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,11);      % Face condition: epsilon3
GLM_HGF_fixom_all.SSL_input.epsilon3_House= GLM_HGF_fixom_all.SSL_input.data(~GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,11);    % House condition: epsilon3

GLM_HGF_fixom_all.SSL_input.da2_Face= GLM_HGF_fixom_all.SSL_input.data(GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,12);           % Face condition: da2
GLM_HGF_fixom_all.SSL_input.da2_House= GLM_HGF_fixom_all.SSL_input.data(~GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,12);         % House condition: da2

GLM_HGF_fixom_all.SSL_input.mu1hat_Face= GLM_HGF_fixom_all.SSL_input.data(GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,13);        % Face condition: mu1hat
GLM_HGF_fixom_all.SSL_input.mu1hat_House= GLM_HGF_fixom_all.SSL_input.data(~GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,13);      % House condition: mu1hat

GLM_HGF_fixom_all.SSL_input.mu1_Face= GLM_HGF_fixom_all.SSL_input.data(GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,14);           % Face condition: mu1
GLM_HGF_fixom_all.SSL_input.mu1_House= GLM_HGF_fixom_all.SSL_input.data(~GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,14);         % House condition: mu1

GLM_HGF_fixom_all.SSL_input.mu2hat_Face= GLM_HGF_fixom_all.SSL_input.data(GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,15);        % Face condition: mu2hat
GLM_HGF_fixom_all.SSL_input.mu2hat_House= GLM_HGF_fixom_all.SSL_input.data(~GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,15);      % House condition: mu2hat

GLM_HGF_fixom_all.SSL_input.mu2_Face= GLM_HGF_fixom_all.SSL_input.data(GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,16);           % Face condition: mu2
GLM_HGF_fixom_all.SSL_input.mu2_House= GLM_HGF_fixom_all.SSL_input.data(~GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,16);         % House condition: mu2

GLM_HGF_fixom_all.SSL_input.mu3hat_Face= GLM_HGF_fixom_all.SSL_input.data(GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,17);        % Face condition: mu3hat
GLM_HGF_fixom_all.SSL_input.mu3hat_House= GLM_HGF_fixom_all.SSL_input.data(~GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,17);      % House condition: mu3hat

GLM_HGF_fixom_all.SSL_input.mu3_Face= GLM_HGF_fixom_all.SSL_input.data(GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,18);           % Face condition: mu3
GLM_HGF_fixom_all.SSL_input.mu3_House= GLM_HGF_fixom_all.SSL_input.data(~GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,18);         % House condition: mu3

GLM_HGF_fixom_all.SSL_input.da_ch_Face= GLM_HGF_fixom_all.SSL_input.data(GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,19);         % Face condition: da_ch (lower-level choice PE)
GLM_HGF_fixom_all.SSL_input.da_ch_House= GLM_HGF_fixom_all.SSL_input.data(~GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,19);       % House condition: da_ch 

GLM_HGF_fixom_all.SSL_input.epsilon_ch_Face= GLM_HGF_fixom_all.SSL_input.data(GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,20);    % Face condition: epsilon_ch 
GLM_HGF_fixom_all.SSL_input.epsilon_ch_House= GLM_HGF_fixom_all.SSL_input.data(~GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,20);  % House condition: epsilon_ch

GLM_HGF_fixom_all.SSL_input.pw3_Face = GLM_HGF_fixom_all.SSL_input.data(GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,21);          % Face condition: pw3 (precision-weight at the higher level)
GLM_HGF_fixom_all.SSL_input.pw3_House= GLM_HGF_fixom_all.SSL_input.data(~GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,21);         % House condition: pw3

GLM_HGF_fixom_all.SSL_input.TargetFace= GLM_HGF_fixom_all.SSL_input.data(GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,22);         % time of face target
GLM_HGF_fixom_all.SSL_input.TargetHouse= GLM_HGF_fixom_all.SSL_input.data(~GLM_HGF_fixom_all.SSL_input.data(:,1) & 1,22);       % time of house target

cd (paths.res_first_ssl);
save ('GLM_HGF_fixom_all.mat','-struct','GLM_HGF_fixom_all','SSL_input');

% we model the whole trial using time bins; with a TR of 2.5 and a trial
% length of 1.8 seconds, we get 12 events per trial (see calculation at the 
% beginning of this function):

% reload regressors:
GLM_bin_HGF_fixom_all = load ([paths.res_first_ssl,'/GLM_HGF_fixom_all.mat']);

% resize regressors such that every trial consists of 12 events:
if isempty(GLM_bin_HGF_fixom_all.SSL_input.irr)
    irr_missed = [];
else
    for n = 1:length(GLM_bin_HGF_fixom_all.SSL_input.irr) % ssl.events = 12, the target presentation starts after 1.5 sec, i.e. at the 9.6th event => 10. event
        irr_missed(n,:)=[GLM_bin_HGF_fixom_all.SSL_input.irr(n),GLM_bin_HGF_fixom_all.SSL_input.irr(n)+bin,GLM_bin_HGF_fixom_all.SSL_input.irr(n)+2*bin,GLM_bin_HGF_fixom_all.SSL_input.irr(n)+3*bin,GLM_bin_HGF_fixom_all.SSL_input.irr(n)+4*bin ...
            GLM_bin_HGF_fixom_all.SSL_input.irr(n)+5*bin,GLM_bin_HGF_fixom_all.SSL_input.irr(n)+6*bin,GLM_bin_HGF_fixom_all.SSL_input.irr(n)+7*bin,GLM_bin_HGF_fixom_all.SSL_input.irr(n)+8*bin,GLM_bin_HGF_fixom_all.SSL_input.irr(n)+9*bin, ...
            GLM_bin_HGF_fixom_all.SSL_input.irr(n)+10*bin,GLM_bin_HGF_fixom_all.SSL_input.irr(n)+11*bin];
    end
end

if isempty(GLM_bin_HGF_fixom_all.SSL_input.irr_late)
    irr_late = [];
else
    for n = 1:length(GLM_bin_HGF_fixom_all.SSL_input.irr_late) % ssl.events = 12, the target presentation starts after 1.5 sec, i.e. at the 9.6th event => 10. event
        irr_late(n,:)=[GLM_bin_HGF_fixom_all.SSL_input.irr_late(n),GLM_bin_HGF_fixom_all.SSL_input.irr_late(n)+bin,GLM_bin_HGF_fixom_all.SSL_input.irr_late(n)+2*bin,GLM_bin_HGF_fixom_all.SSL_input.irr_late(n)+3*bin,GLM_bin_HGF_fixom_all.SSL_input.irr_late(n)+4*bin ...
            GLM_bin_HGF_fixom_all.SSL_input.irr_late(n)+5*bin,GLM_bin_HGF_fixom_all.SSL_input.irr_late(n)+6*bin,GLM_bin_HGF_fixom_all.SSL_input.irr_late(n)+7*bin,GLM_bin_HGF_fixom_all.SSL_input.irr_late(n)+8*bin,GLM_bin_HGF_fixom_all.SSL_input.irr_late(n)+9*bin, ...
            GLM_bin_HGF_fixom_all.SSL_input.irr_late(n)+10*bin,GLM_bin_HGF_fixom_all.SSL_input.irr_late(n)+11*bin];
    end
end

for n = 1:length(GLM_bin_HGF_fixom_all.SSL_input.CueFace) % ssl.events = 12, the target presentation starts after 1.5 sec, i.e. at the 9.6th event => 10. event
    Face(n,:)=[GLM_bin_HGF_fixom_all.SSL_input.CueFace(n)];
    for i = 2:12
        Face(n,i) = Face(n,i-1)+bin;
    end
    
    % regressors are updated at outcome presentation (i.e. 10th event):
    pw2_Face(n,:)=[zeros(1,9), repmat(GLM_bin_HGF_fixom_all.SSL_input.sa2_Face(n),1,3)];
    
    da1_Face(n,:)=[zeros(1,9),repmat(GLM_bin_HGF_fixom_all.SSL_input.da1_Face(n),1,3)];
    
    epsilon2_Face(n,:)=[zeros(1,9), repmat(GLM_bin_HGF_fixom_all.SSL_input.epsilon2_Face(n),1,3)];
    
    pw3_Face(n,:)=[zeros(1,9),repmat(GLM_bin_HGF_fixom_all.SSL_input.pw3_Face(n),1,3)];
    
    da2_Face(n,:)=[zeros(1,9),repmat(GLM_bin_HGF_fixom_all.SSL_input.da2_Face(n),1,3)];
    
    epsilon3_Face(n,:)=[zeros(1,9), repmat(GLM_bin_HGF_fixom_all.SSL_input.epsilon3_Face(n),1,3)];
    
    da_ch_Face(n,:)=[zeros(1,9),repmat(GLM_bin_HGF_fixom_all.SSL_input.da_ch_Face(n),1,3)];
    
    epsilon_ch_Face(n,:)=[zeros(1,9),repmat(GLM_bin_HGF_fixom_all.SSL_input.epsilon_ch_Face(n),1,3)];
    
end

for n = 1:length(GLM_bin_HGF_fixom_all.SSL_input.CueHouse) % ssl.events = 12, the target presentation starts after 1.5 sec, i.e. at the 9.6th event => 10. event
    House(n,:)=[GLM_bin_HGF_fixom_all.SSL_input.CueHouse(n)];
    for i = 2:12
        House(n,i) = House(n,i-1)+bin;
    end

    % regressors are updated at outcome presentation (i.e. 10th event):
    pw2_House(n,:)=[zeros(1,9), repmat(GLM_bin_HGF_fixom_all.SSL_input.sa2_House(n),1,3)];
    
    da1_House(n,:)=[zeros(1,9),repmat(GLM_bin_HGF_fixom_all.SSL_input.da1_House(n),1,3)];
    
    epsilon2_House(n,:)=[zeros(1,9), repmat(GLM_bin_HGF_fixom_all.SSL_input.epsilon2_House(n),1,3)];
    
    pw3_House(n,:)=[zeros(1,9),repmat(GLM_bin_HGF_fixom_all.SSL_input.pw3_House(n),1,3)];
    
    da2_House(n,:)=[zeros(1,9),repmat(GLM_bin_HGF_fixom_all.SSL_input.da2_House(n),1,3)];
   
    epsilon3_House(n,:)=[zeros(1,9), repmat(GLM_bin_HGF_fixom_all.SSL_input.epsilon3_House(n),1,3)];
    
    da_ch_House(n,:)=[zeros(1,9),repmat(GLM_bin_HGF_fixom_all.SSL_input.da_ch_House(n),1,3)];
    
    epsilon_ch_House(n,:)=[zeros(1,9),repmat(GLM_bin_HGF_fixom_all.SSL_input.epsilon_ch_House(n),1,3)];
    
end

% reshape regressors:
GLM_bin_HGF_fixom_all.irr_missed = reshape(irr_missed.',1,[])';
GLM_bin_HGF_fixom_all.irr_late = reshape(irr_late.',1,[])';

GLM_bin_HGF_fixom_all.Face = reshape(Face.',1,[])';
GLM_bin_HGF_fixom_all.House = reshape(House.',1,[])';

GLM_bin_HGF_fixom_all.pw2_Face = reshape(pw2_Face.',1,[])';
GLM_bin_HGF_fixom_all.pw2_House = reshape(pw2_House.',1,[])';

GLM_bin_HGF_fixom_all.da1_Face = reshape(da1_Face.',1,[])';
GLM_bin_HGF_fixom_all.da1_House = reshape(da1_House.',1,[])';

GLM_bin_HGF_fixom_all.epsilon2_Face = reshape(epsilon2_Face.',1,[])';
GLM_bin_HGF_fixom_all.epsilon2_House = reshape(epsilon2_House.',1,[])';

GLM_bin_HGF_fixom_all.pw3_Face = reshape(pw3_Face.',1,[])';
GLM_bin_HGF_fixom_all.pw3_House = reshape(pw3_House.',1,[])';

GLM_bin_HGF_fixom_all.da2_Face = reshape(da2_Face.',1,[])';
GLM_bin_HGF_fixom_all.da2_House = reshape(da2_House.',1,[])';

GLM_bin_HGF_fixom_all.epsilon3_Face = reshape(epsilon3_Face.',1,[])';
GLM_bin_HGF_fixom_all.epsilon3_House = reshape(epsilon3_House.',1,[])';

GLM_bin_HGF_fixom_all.da_ch_Face = reshape(da_ch_Face.',1,[])';
GLM_bin_HGF_fixom_all.da_ch_House = reshape(da_ch_House.',1,[])';

GLM_bin_HGF_fixom_all.epsilon_ch_Face = reshape(epsilon_ch_Face.',1,[])';
GLM_bin_HGF_fixom_all.epsilon_ch_House = reshape(epsilon_ch_House.',1,[])';

cd (paths.res_first_ssl);
save ('GLM_bin_HGF_fixom_all.mat','-struct','GLM_bin_HGF_fixom_all');

%% display no. of irregular trials, no. of face and house regressors:
disp(['missed and early responses :']);
disp([size(irr_missed)]);
disp(['irr_late :']);
disp([size(irr_late)]);
disp(['Face :']);
disp([size(GLM_bin_HGF_fixom_all.Face)]);
disp(['House :']);
disp([size(GLM_bin_HGF_fixom_all.House)]);


end

