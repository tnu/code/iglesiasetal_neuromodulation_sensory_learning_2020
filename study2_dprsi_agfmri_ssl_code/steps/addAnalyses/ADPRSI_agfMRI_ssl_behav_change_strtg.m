function ADPRSI_agfMRI_ssl_behav_change_strtg
% ADPRSI_agfMRI_ssl_behav_change_strtg: summarises and visualises 
% how many participants got each trial correct.

%   IN:     -

%   OUT:    -

% 24-08-2020; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

% Set paths
workdir = ADPRSI_agfMRI_ssl_set_workdir;
options = ADPRSI_agfMRI_ssl_set_analysis_options;
[ details, paths ] = ADPRSI_agfMRI_ssl_subjects( 1 );

% Start diary
diary(paths.logfile_hgf_ssl);

% Initialize
disp(['preparing struct to visualise correct responses per trial']);

ant_ssl = load([paths.res_group_ssl_descr,'/ADPRSI_agfMRI_ssl.mat']);
behav=load([paths.res_group_ssl_behaviour,'/HGF_fixom_v1/ParSSL.mat']);
out=load([paths.res_group_ssl_glm,'/sslbin/fixom_bin3_spm12_fact/agfMRI_ssl_group.mat']);
check_behav = load([paths.quality_ssl_behav,'/Check_agfMRI_ssl_behaviour.mat']);

for id = 1:numel(options.subjectIDs)
    [ details, paths ] = ADPRSI_agfMRI_ssl_subjects( id );
    disp(['running summary behavioural data for subj: ', details.subjname,' (',num2str(id),')']);
    disp ('**************************************************************');
    % load behavioural data:
    behav = load([paths.res_behaviour_ssl,details.behaviour_ssl]);
    % load HGF data with information about irregular trials:
    hgf1 = load([paths.res_behaviour_ssl,'/HGF_fixom_v1/SSL.VS.mat']);
    
    choice = behav.alldata(:,5);
    choice(hgf1.VS.irr_all) = NaN;
    
    ADPRSI_agfMRI_ssl_choices.subj(:,id) = choice;
    ADPRSI_agfMRI_ssl_choices.subj_da1(:,id) = abs(hgf1.VS.est.traj.da1);
end

% remove excluded participants:
drug = ant_ssl.group;
drug(out.out_final) = [];
ADPRSI_agfMRI_ssl_choices.subj(:,out.out_final) = [];
ADPRSI_agfMRI_ssl_choices.subj_da1(:,out.out_final) = [];

% categorise drug conditions:
choices_pla = ADPRSI_agfMRI_ssl_choices.subj(:,drug == 0);
choices_lev = ADPRSI_agfMRI_ssl_choices.subj(:,drug == 1);
choices_gal = ADPRSI_agfMRI_ssl_choices.subj(:,drug == 2);

% sum across all trials; all participants:
ADPRSI_agfMRI_ssl_choices.sum_all(:,1)=sum(ADPRSI_agfMRI_ssl_choices.subj(:,:),2,'omitnan');
% group-wise
ADPRSI_agfMRI_ssl_choices.sum_pla=sum(choices_pla(:,:),2,'omitnan');
ADPRSI_agfMRI_ssl_choices.sum_lev=sum(choices_lev(:,:),2,'omitnan');
ADPRSI_agfMRI_ssl_choices.sum_gal=sum(choices_gal(:,:),2,'omitnan');

% %CR of all participants:
ADPRSI_agfMRI_ssl_choices.prc_all = (ADPRSI_agfMRI_ssl_choices.sum_all/numel(drug));
% group-wise
ADPRSI_agfMRI_ssl_choices.prc_pla = (ADPRSI_agfMRI_ssl_choices.sum_pla/numel(drug));
ADPRSI_agfMRI_ssl_choices.prc_lev = (ADPRSI_agfMRI_ssl_choices.sum_lev/numel(drug));
ADPRSI_agfMRI_ssl_choices.prc_gal = (ADPRSI_agfMRI_ssl_choices.sum_gal/numel(drug));

% code %CR based on probability block:
for i = 1:numel(behav.alldata(:,13))
    if behav.alldata(i,13) > 0.5
        ADPRSI_agfMRI_ssl_choices.prc_all_coded(i) = ADPRSI_agfMRI_ssl_choices.prc_all(i);
    else
        ADPRSI_agfMRI_ssl_choices.prc_all_coded(i) = 1-ADPRSI_agfMRI_ssl_choices.prc_all(i);
    end
end

% mean delta1:
ADPRSI_agfMRI_ssl_choices.mean_da1 = mean(ADPRSI_agfMRI_ssl_choices.subj_da1,2);

[~,~]= mkdir([paths.res_group_ssl_behaviour,'/sum_correct_choices']);
cd([paths.res_group_ssl_behaviour,'/sum_correct_choices'])
save('ADPRSI_agfMRI_ssl_choices.mat', '-struct','ADPRSI_agfMRI_ssl_choices');

% plot figure:
h1 = figure('Name','trial-wise correct responses','Color',[1 1 1]);
s1=subplot(3,1,1);
plot(behav.alldata(:,13),'Color','black','LineWidth',1); hold on;
plot(1:320, hgf1.VS.CongrIn, '.', 'Color', [0 0.6 0]); % cue-outcome congruency
ylim([-0.1 1.1]);
xlim([0 320]);
hold on;
x = [1:320];
y = behav.alldata(:,13);
plot(ADPRSI_agfMRI_ssl_choices.prc_all,'Color',[0.00,0.25,0.74],'LineWidth',0.5);
hold on;
title({'percentage of participants giving a correct response (blue);', 'changes in cue strength (black);  input u (green)'});

s2=subplot(3,1,2);
plot(behav.alldata(:,13),'Color','black','LineWidth',1); hold on;
plot(1:320, hgf1.VS.CongrIn, '.', 'Color', [0 0.6 0]); % cue-outcome congruency
ylim([-0.1 1.1]);
xlim([0 320]);
hold on;
x = [1:320];
y = behav.alldata(:,13);
plot(ADPRSI_agfMRI_ssl_choices.prc_all_coded,'Color',[0.00,0.25,0.74],'LineWidth',0.5);
hold on;
title({'percentage of participants giving a correct response (blue) - inverted for 0.1 and 0.3 blocks;', 'changes in cue strength (black); input u (green)'});

s3=subplot(3,1,3);
% plot(behav.alldata(:,13),'Color','black','LineWidth',1); hold on;
plot(1:320, hgf1.VS.CongrIn, '.', 'Color', [0 0.6 0]); % cue-outcome congruency
ylim([-0.1 1.1]);
xlim([0 320]);
hold on;
x = [1:320];
y = behav.alldata(:,13);
plot(ADPRSI_agfMRI_ssl_choices.mean_da1,'Color',[0.64,0.08,0.18],'LineWidth',1);
plot(1-ADPRSI_agfMRI_ssl_choices.prc_all,'Color',[0.00,0.25,0.74],'LineWidth',0.5);
hold on;
title({'inverted trace of percentage of participants giving a correct response (blue) vs. average \delta_1 (red)','input u (green)'});
print -djpeg ADPRSI_agfMRI_ssl_choices_prc;
saveas(h1,'ADPRSI_agfMRI_ssl_choices_prc','fig');
clear h1;

ADPRSI_agfMRI_ssl_choices.struct = [behav.alldata(:,13) behav.alldata(:,12) ADPRSI_agfMRI_ssl_choices.sum_all(:,1) ...
ADPRSI_agfMRI_ssl_choices.sum_pla(:,1) ADPRSI_agfMRI_ssl_choices.sum_lev(:,1) ADPRSI_agfMRI_ssl_choices.sum_gal(:,1)];
save('ADPRSI_agfMRI_ssl_choices.mat', '-struct','ADPRSI_agfMRI_ssl_choices');

end

