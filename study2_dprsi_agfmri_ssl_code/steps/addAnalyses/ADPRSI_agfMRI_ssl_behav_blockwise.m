function ADPRSI_agfMRI_ssl_behav_blockwise
% ADPRSI_agfMRI_ssl_behav_blockwise: writes the structure
% ADPRSI_agfMRI_ssl_bw with behavioural data summarised blockwise.

%   IN:     -

%   OUT:    -

% 27-07-2020; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

% Set paths
workdir = ADPRSI_agfMRI_ssl_set_workdir;
options = ADPRSI_agfMRI_ssl_set_analysis_options;
[ details, paths ] = ADPRSI_agfMRI_ssl_subjects( 1 );

% Start diary
diary(paths.logfile_hgf_ssl);

% Initialize
disp(['preparing struct for blockwise descriptive statistics']);

descr = load([paths.res_group_ssl_descr,'/ADPRSI_agfMRI_ssl.mat']);
behav=load([paths.res_group_ssl_behaviour,'/HGF_fixom_v1/ParSSL.mat']);
out=load([paths.res_group_ssl_glm,'/sslbin/fixom_bin3_spm12_fact/agfMRI_ssl_group.mat']);
check_behav = load([paths.quality_ssl_behav,'/Check_agfMRI_ssl_behaviour.mat']);

for id = 1:numel(options.subjectIDs)
    [ details, paths ] = ADPRSI_agfMRI_ssl_subjects( id );
    disp(['running summary behavioural data for subj: ', details.subjname,' (',num2str(id),')']);
    disp ('**************************************************************');
    % load behavioural data:
    behav = load([paths.res_behaviour_ssl,details.behaviour_ssl]);
    % load HGF data with information about irregular trials:
    hgf1 = load([paths.res_behaviour_ssl,'/HGF_fixom_v1/SSL.VS.mat']);
    
    % write out number of correct trials and RT per block
    CurrentBlockIndex = 0;
    for nr = 1:length(behav.param.block_length)
        CurrentBlockIndex = CurrentBlockIndex+1;
        Corrects_block = behav.block(CurrentBlockIndex).exp_data(:,5);
        irr = [];
        irr_late = [];
        irr_early = [];
        for l = 1:length(behav.block(CurrentBlockIndex).exp_data(:,3))
            if behav.block(CurrentBlockIndex).exp_data(l,4) == -99
                irr = [irr, l];
            elseif behav.block(CurrentBlockIndex).exp_data(l,3) > 1600 % Duration cue presentation: 300; Duration interstimulus interval: 1200; plus 100ms margin, 
                                                                       % as the reaction to the outcome presentation would not be as fast.
                irr_late = [irr_late, l];
            elseif behav.block(CurrentBlockIndex).exp_data(l,3) < 0
                if behav.block(CurrentBlockIndex).exp_data(l,4) ~=-99
                    irr_early = [irr_early, l];
                end
            end
        end
        irr_all = [irr irr_late irr_early]; %all irregular trials within block
        Corrects_block(irr_all) = [];
        
        ParSSL.nr_corr.trials = size(find(Corrects_block==1));
        ParSSL.nr_corr.block(CurrentBlockIndex,1) = ParSSL.nr_corr.trials(1,1);
        irr_neg_block = [];
        for l = 1:length(behav.block(CurrentBlockIndex).exp_data(:,3))
            if behav.block(CurrentBlockIndex).exp_data(l,3) < -1 || behav.block(CurrentBlockIndex).exp_data(l,3) > 1600
                irr_neg_block = [irr_neg_block, l];
            end
        end
        rtblock = behav.block(CurrentBlockIndex).exp_data(:,3);
        rtblock(irr_neg_block) = [];
        rtmean = mean(rtblock);
        ParSSL.stat.blockrtmean(CurrentBlockIndex,1) = rtmean(1,1);
        ParSSL.block_nr(CurrentBlockIndex,1) = CurrentBlockIndex;
    end
    % a=[40;32;24;40;32;24;32;24;40;32]; % number of trials per block
    % b=[0.9;0.9;0.5;0.7;0.7;0.9;0.7;0.7;0.5;0.9]; % probability for each block
    % results in the following maximal number of corrects possible
    % per block
    ParSSL.prob_corr = [36;29;12;28;22;22;22;17;20;29];
    ParSSL.nr_trials = [40;32;24;40;32;24;32;24;40;32];
    ParSSL.block_percent_corr_adj= ParSSL.nr_corr.block(:,1)./ParSSL.prob_corr(:,1);
    ParSSL.block_percent_corr= ParSSL.nr_corr.block(:,1)./ParSSL.nr_trials(:,1);
    ParSSL.sum_nr_corr(id,1) = sum(ParSSL.nr_corr.block(:,1));
    
    % Correct responses (not adjusted)
    CR = behav.alldata(:,5);
    if ~(check_behav.behav.Missed.irr_total(id,1))==0
        CR(hgf1.VS.irr_all) = [];
    end
    ParSSL.all_CR.sum(id,1) = sum(CR);
    ParSSL.all_CR.percent(id,1) = sum(CR)/320;
    
    A_SSL_VS.block_percent_corr_adj(id,:) = ParSSL.block_percent_corr_adj(:,1);
    A_SSL_VS.block_percent_corr(id,:) = ParSSL.block_percent_corr(:,1);
    A_SSL_VS.all_percent_corr(id,1) = ParSSL.all_CR.percent(id,1);
    A_SSL_VS.blockrtmean(id,:) =  ParSSL.stat.blockrtmean(:,1);
end

ADPRSI_agfMRI_ssl_bw.labels = [descr.subj, descr.drug, descr.snp.comt];
ADPRSI_agfMRI_ssl_bw.labels_string = [{'SubjName'}, {'SubstanceString'}, {'COMTString'}]';
ADPRSI_agfMRI_ssl_bw.data = [descr.age, descr.weight, descr.group, ...
    A_SSL_VS.block_percent_corr_adj(:,1), A_SSL_VS.block_percent_corr_adj(:,2), A_SSL_VS.block_percent_corr_adj(:,3), A_SSL_VS.block_percent_corr_adj(:,4), A_SSL_VS.block_percent_corr_adj(:,5), ...
    A_SSL_VS.block_percent_corr_adj(:,6), A_SSL_VS.block_percent_corr_adj(:,7), A_SSL_VS.block_percent_corr_adj(:,8), A_SSL_VS.block_percent_corr_adj(:,9), A_SSL_VS.block_percent_corr_adj(:,10), ...
    A_SSL_VS.block_percent_corr(:,1), A_SSL_VS.block_percent_corr(:,2), A_SSL_VS.block_percent_corr(:,3), A_SSL_VS.block_percent_corr(:,4), A_SSL_VS.block_percent_corr(:,5), ...
    A_SSL_VS.block_percent_corr(:,6), A_SSL_VS.block_percent_corr(:,7), A_SSL_VS.block_percent_corr(:,8), A_SSL_VS.block_percent_corr(:,9), A_SSL_VS.block_percent_corr(:,10), ...
    A_SSL_VS.blockrtmean(:,1), A_SSL_VS.blockrtmean(:,2), A_SSL_VS.blockrtmean(:,3), A_SSL_VS.blockrtmean(:,4), A_SSL_VS.blockrtmean(:,5), ...
    A_SSL_VS.blockrtmean(:,6), A_SSL_VS.blockrtmean(:,7), A_SSL_VS.blockrtmean(:,8), A_SSL_VS.blockrtmean(:,9), A_SSL_VS.blockrtmean(:,10), ...
    descr.behav.KSS, descr.snp.comt_code, 1./check_behav.behav.RT.mean, A_SSL_VS.all_percent_corr];
ADPRSI_agfMRI_ssl_bw.data_string = [{'Age'}, {'Weight'}, {'Substance'}, ...
    {'adjCR_1'}, {'adjCR_2'}, {'adjCR_3'}, {'adjCR_4'}, {'adjCR_5'}, {'adjCR_6'}, {'adjCR_7'}, {'adjCR_8'}, {'adjCR_9'}, {'adjCR_10'},...
    {'CR_1'}, {'CR_2'}, {'CR_3'}, {'CR_4'}, {'CR_5'}, {'CR_6'}, {'CR_7'}, {'CR_8'}, {'CR_9'}, {'CR_10'},...
    {'RT_1'}, {'RT_2'}, {'RT_3'}, {'RT_4'}, {'RT_5'}, {'RT_6'}, {'RT_7'}, {'RT_8'}, {'RT_9'}, {'RT_10'},...
    {'KSS'}, {'COMTrs4680'}, {'InverseRT'}, {'all_CR'}]';

% delete entries for excluded subjects:
ADPRSI_agfMRI_ssl_bw.labels(out.out_final,:)=[];
ADPRSI_agfMRI_ssl_bw.data(out.out_final,:)=[];

[~,~]= mkdir([paths.res_group_ssl_behaviour,'/SPSS']);
cd([paths.res_group_ssl_behaviour,'/SPSS'])
save('ADPRSI_agfMRI_ssl_bw.mat', '-struct','ADPRSI_agfMRI_ssl_bw');
end

