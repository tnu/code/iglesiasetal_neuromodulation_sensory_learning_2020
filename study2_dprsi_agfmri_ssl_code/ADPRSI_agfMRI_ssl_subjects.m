function [ details, paths ] = ADPRSI_agfMRI_ssl_subjects( id )
% ADPRSI_agfMRI_ssl_subjects function that sets all filenames and paths

%   IN:     id          - the subject index

%   OUT:    details     - a struct that holds all filenames and paths for
%                       the subject with subject number id
%           paths       - a struct that holds all paths and config files
%                       that are not subject-specific

% 01-11-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

%-- check input -----------------------------------------------------------
if isstruct(id)
    options = id;
    id = 'dummy';
elseif ischar(id) && nargin < 2
    options = ADPRSI_agfMRI_ssl_set_analysis_options;
elseif isnumeric(id) && nargin < 2
    options = ADPRSI_agfMRI_ssl_set_analysis_options;
end

options.part = 'agfMRI';

%% paths to directories --------------------------------------------------%
details.subjfolder          = options.subjectIDs{id};

paths.scandata              = fullfile(options.rawdir, details.subjfolder , '/scandata/');
paths.fMRIssl               = fullfile(options.rawdir, details.subjfolder , '/scandata/fmri/ssl/');
paths.struct                = fullfile(options.rawdir, details.subjfolder , '/scandata/struct/');
paths.fMRIssl_backup        = fullfile(options.rawdir, details.subjfolder , '/scandata/fmri/ssl/backup/');
paths.struct_backup         = fullfile(options.rawdir, details.subjfolder , '/scandata/struct/backup/');

paths.physlog               = fullfile(options.rawdir, details.subjfolder , '/physlog/');
paths.physlog_ssl           = fullfile(options.rawdir, details.subjfolder , '/physlog/ssl/');

paths.behaviour             = fullfile(options.rawdir, details.subjfolder , '/behavior/');
paths.behaviour_ssl         = fullfile(options.rawdir, details.subjfolder , '/behavior/ssl/');

paths.res_fMRIssl           = fullfile(options.resultsdir, '/results_fMRI_ssl/', details.subjfolder , '/scandata/fmri/ssl/');
paths.res_struct            = fullfile(options.resultsdir, '/results_fMRI_ssl/', details.subjfolder , '/scandata/struct/');

paths.res_fMRIssl_backup    = fullfile(options.resultsdir, '/results_fMRI_ssl/', details.subjfolder , '/scandata/fmri/ssl/backup/');
paths.res_struct_backup     = fullfile(options.resultsdir, '/results_fMRI_ssl/', details.subjfolder , '/scandata/struct/backup/');
paths.res_checkreg          = fullfile(options.resultsdir, '/results_fMRI_ssl/', details.subjfolder , '/scandata/checkreg/');

paths.res_physlog_ssl       = fullfile(options.resultsdir, '/results_phys_ssl/', details.subjfolder , '/physlog/ssl/');

paths.res_behaviour_ssl     = fullfile(options.resultsdir, '/results_behav_ssl/', details.subjfolder , '/behavior/ssl/');

paths.res_first_ssl         = fullfile(options.resultsdir, '/results_first_ssl/', details.subjfolder , '/glm/ssl/');

%group directories -------------------------------------------------------%
paths.res_group_ssl         = fullfile(options.group_ssl);

paths.quality_ssl           = fullfile(paths.res_group_ssl, '/quality');
paths.quality_ssl_prep      = fullfile(paths.res_group_ssl, '/quality/prep/');
paths.quality_ssl_behav     = fullfile(paths.res_group_ssl, '/quality/behav/');
paths.quality_ssl_physio    = fullfile(paths.res_group_ssl, '/quality/physio/');
paths.quality_ssl_first     = fullfile(paths.res_group_ssl, '/quality/first/');

paths.group_ssl_descr           = fullfile(options.rawdir_group, '/descr/');
paths.res_group_ssl_descr       = fullfile(paths.res_group_ssl, '/descr/');

paths.res_group_ssl_behaviour   = fullfile(paths.res_group_ssl, '/behav/');

paths.res_hgf_ssl_bms           = fullfile(paths.res_group_ssl, '/behav/bms/');

paths.group_ssl_glm             = fullfile(options.rawdir_group, '/glm/');
paths.res_group_ssl_glm         = fullfile(paths.res_group_ssl, '/glm/');

%%file names -------------------------------------------------------------%
details.subjname        = options.subjectIDs{id};
disp(details.subjname);
disp('******************');

details.rawfMRIssl      = 'fmri_ssl.nii';
details.rawstruct       = 'struct.nii';
details.splitfMRIssl    = 'fmri_ssl_00';
details.coregfMRIssl    = 'wfmri_ssl.nii';
details.coregstruct     = 'wmstruct.nii';
details.smoothfMRIssl   = 'swfmri_ssl.nii';
details.smoothstruct    = 'swstruct.nii';

details.physio_ssl      = 'physio_ssl.log';

details.behaviour_ssl   = ([details.subjname,'.mat']);

%% logging
paths.logdir                = fullfile(options.resultsdir, '/logfile/');

paths.logdir_prep           = fullfile(options.resultsdir, '/results_fMRI_ssl/logfile/');
paths.logdir_physio_ssl     = fullfile(options.resultsdir, '/results_phys_ssl/logfile/');

paths.logdir_hgf_ssl        = fullfile(options.resultsdir, '/results_behav_ssl/logfile/');
paths.logdir_first_ssl      = fullfile(options.resultsdir, '/results_first_ssl/logfile/first/');
paths.logdir_glm_input_ssl  = fullfile(options.resultsdir, '/results_first_ssl/logfile/glm_input/');

paths.logdir_group_ssl      = fullfile(options.resultsdir, '/results_group_ssl/logfile/');

% logging "prepare data"
paths.logfile_prepare       = fullfile(options.resultsdir, '/logfile/', [details.subjfolder, options.part, 'preparedata.log']);

% logging "preprocess data"
paths.logfile_prep          = fullfile(options.resultsdir, '/results_fMRI_ssl/logfile/', [details.subjfolder, options.part, 'preprocessdata.log']);
paths.logfile_checkreg      = fullfile(options.resultsdir, '/results_fMRI_ssl/logfile/', [details.subjfolder, options.part, 'checkreg.log']);
paths.logfile_physio_ssl    = fullfile(options.resultsdir, '/results_phys_ssl/logfile/', [details.subjfolder, options.part, 'physio.log']);

% logging "hgf and behaviour"
paths.logfile_hgf_ssl               = fullfile(options.resultsdir, '/results_behav_ssl/logfile/', [details.subjfolder, options.part, 'hgf.log']);
paths.logfile_check_behaviour_ssl   = fullfile(options.resultsdir, '/results_behav_ssl/logfile/', [details.subjfolder, options.part, 'check_behaviour.log']);
paths.logfile_behav_summary_ssl     = fullfile(options.resultsdir, '/results_behav_ssl/logfile/', [details.subjfolder, options.part, 'ssl_behav_summary.log']);
paths.logfile_bms_ssl               = fullfile(options.resultsdir, '/results_behav_ssl/logfile/', [details.subjfolder, options.part, 'bms.log']);

% logging "first level analysis"
paths.logfile_first_ssl     = fullfile(options.resultsdir, '/results_first_ssl/logfile/first/', [details.subjfolder, options.part, 'first.log']);
paths.logfile_glm_input_ssl = fullfile(options.resultsdir, '/results_first_ssl/logfile/glm_input/', [details.subjfolder, options.part, 'glm_input.log']);

% logging "second level analysis"
paths.logfile_group_ssl     = fullfile(options.resultsdir, '/results_group_ssl/logfile/', [details.subjfolder, options.part, 'group.log']);

%% create folders --------------------------------------------------------% 
if ~exist(paths.res_group_ssl, 'dir')
    mkdir(paths.res_group_ssl );
end

if ~exist(paths.quality_ssl_prep, 'dir')
    mkdir(paths.quality_ssl_prep );
end
if ~exist(paths.quality_ssl, 'dir')
    mkdir(paths.quality_ssl );
end
if ~exist(paths.quality_ssl_behav, 'dir')
    mkdir(paths.quality_ssl_behav );
end
if ~exist(paths.quality_ssl_physio, 'dir')
    mkdir(paths.quality_ssl_physio );
end
if ~exist(paths.quality_ssl_first, 'dir')
    mkdir(paths.quality_ssl_first );
end

if ~exist(paths.scandata, 'dir')
    mkdir(paths.scandata );
end
if ~exist(paths.fMRIssl, 'dir')
    mkdir(paths.fMRIssl);
end
if ~exist(paths.struct, 'dir')
    mkdir(paths.struct);
end
if ~exist(paths.fMRIssl_backup, 'dir')
    mkdir(paths.fMRIssl_backup);
end
if ~exist(paths.struct_backup, 'dir')
    mkdir(paths.struct_backup);
end


if ~exist(paths.physlog, 'dir')
    mkdir(paths.physlog );
end
if ~exist(paths.physlog_ssl, 'dir')
    mkdir(paths.physlog_ssl );
end


if ~exist(paths.behaviour, 'dir')
    mkdir(paths.behaviour );
end
if ~exist(paths.behaviour_ssl, 'dir')
    mkdir(paths.behaviour_ssl );
end

if ~exist(paths.res_fMRIssl, 'dir')
    mkdir(paths.res_fMRIssl );
end
if ~exist(paths.res_struct, 'dir')
    mkdir(paths.res_struct );
end
if ~exist(paths.res_fMRIssl_backup, 'dir')
    mkdir(paths.res_fMRIssl_backup );
end
if ~exist(paths.res_struct_backup, 'dir')
    mkdir(paths.res_struct_backup );
end
if ~exist(paths.res_checkreg, 'dir')
    mkdir(paths.res_checkreg );
end


if ~exist(paths.res_physlog_ssl, 'dir')
    mkdir(paths.res_physlog_ssl );
end


if ~exist(paths.res_behaviour_ssl, 'dir')
    mkdir(paths.res_behaviour_ssl );
end


if ~exist(paths.res_first_ssl, 'dir')
    mkdir(paths.res_first_ssl );
end


if ~exist(paths.group_ssl_descr, 'dir')
    mkdir(paths.group_ssl_descr );
end
if ~exist(paths.res_group_ssl_descr, 'dir')
    mkdir(paths.res_group_ssl_descr );
end

if ~exist(paths.group_ssl_glm, 'dir')
    mkdir(paths.group_ssl_glm );
end
if ~exist(paths.res_group_ssl_glm, 'dir')
    mkdir(paths.res_group_ssl_glm );
end

if ~exist(paths.res_group_ssl_behaviour, 'dir')
    mkdir(paths.res_group_ssl_behaviour );
end
if ~exist(paths.res_hgf_ssl_bms, 'dir')
    mkdir(paths.res_hgf_ssl_bms );
end


if ~exist(paths.logdir, 'dir')
    mkdir(paths.logdir );
end
if ~exist(paths.logdir_prep, 'dir')
    mkdir(paths.logdir_prep );
end
if ~exist(paths.logdir_physio_ssl, 'dir')
    mkdir(paths.logdir_physio_ssl );
end
if ~exist(paths.logdir_hgf_ssl, 'dir')
    mkdir(paths.logdir_hgf_ssl );
end
if ~exist(paths.logdir_first_ssl, 'dir')
    mkdir(paths.logdir_first_ssl );
end
if ~exist(paths.logdir_glm_input_ssl, 'dir')
    mkdir(paths.logdir_glm_input_ssl );
end
if ~exist(paths.logdir_group_ssl, 'dir')
    mkdir(paths.logdir_group_ssl );
end

end
