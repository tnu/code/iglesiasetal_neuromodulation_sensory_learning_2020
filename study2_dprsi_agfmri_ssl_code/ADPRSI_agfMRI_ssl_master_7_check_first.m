function ADPRSI_agfMRI_ssl_master_7_check_first
% ADPRSI_agfMRI_ssl_master_7_check_first function to run analysis steps: 
% 1. correlation between model parameters and between states
% 2. plot da2 trajectories using different values of theta
% 3. run HGF simulations, i.e. parameter recovery

%   IN:     -

%   OUT:    -

% 04-11-2019; Sandra Iglesias
% -------------------------------------------------------------------------
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public Licence 
% (GPL), version 3. You can redistribute it and/or modify it under the 
% terms of the GPL (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
% =========================================================================

%% Set paths
ADPRSI_agfMRI_ssl_initialise_environment();

%% plot correlations between HGF and GLM regressors
ADPRSI_agfMRI_ssl_compute_corr_hgf_trajectories
%% plot da2 trajectories using different values of theta
ADPRSI_agfMRI_ssl_hgfv1_plot_traj_diffthetas

%% run HGF simulations
ADPRSI_agfMRI_ssl_hgfv1_sim_post
end
